/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';


var User = require('../api/user/user.model'),
  Product= require('../api/product/product.model'),
  Conversations= require('../api/conversations/conversations.model'),
  id = null,
  fs = require('fs'),
  util = require('util'),
  obj = null;


User.on('index', function () {
  console.log('index');
  User.create({
    name : 'Pedro',
    surname : 'Bustamante',
    password : 'mypass',
    type : 0,
    email : 'bustamantepj@gmail.com'
  },function(err,user){
    console.log('USER',user);
    if(undefined !== user){
      id = user._id;
      console.log("ID",id);
      Product.find({}).remove(function() {
        Product.create({
          name: 'Curso de AngularJS',
          description: 'Curso básico de AngularJs de 80 horas',
          location: {type: 'Point', coordinates: [40.4456511, -3.6319230000000005]},
          category: 0,
          owner :id,
          price: 100
        }, {
          name: 'Curso de HTML5',
          description: 'Curso HTML5 con CSS de 120 horas',
          location: {type: 'Point', coordinates: [42.46565, -3.671923]},
          category: 0,
          owner :id,
          price: 100
        }, {
          name: 'Curso de NodeJS',
          description: 'Curso básico de NodeJS de 80 horas',
          location: {type: 'Point', coordinates: [40.46565, -3.631923]},
          category: 0,
          owner :id,
          price: 100
        }, {
          name: 'Curso de MongoDB',
          description: 'Curso básico de MongoDB de 80 horas',
          location: {type: 'Point', coordinates: [40.47565, -3.641923]},
          category: 0,
          owner :id,
          price: 100
        }, {
          name: 'Curso de Destacado',
          description: 'Curso Destacado',
          location: {type: 'Point', coordinates: [40.47065, -3.641023]},
          productType : 1,
          category: 0,
          owner :id,
          price: 100
        });

      });

      // fs.readFile('categorias.json', 'utf8', function (err, data) {
      //   if (err) throw err;
      //   obj = JSON.parse(data);
      //   var length_obj = obj.length;
      //
      //   for (var i=0; i< length_obj; i++) {
      //     Category.create(
      //       {
      //         name_es : obj[i]["denominacion"],
      //         name_en : obj[i]["denominacion"],
      //         seccion : obj[i]["seccion"],
      //         division : obj[i]["division"],
      //         agrupacion : obj[i]["agrupacion"],
      //         grupo : obj[i]["grupo"],
      //         epigrafe : obj[i]["epigrafe"]
      //       }
      //     )
      //   }
      // });

    }

  });
});
