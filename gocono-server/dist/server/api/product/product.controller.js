
/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /products              ->  getAllProducts
 * POST    /products              ->  createProduct
 * GET     /products/:id          ->  getProduct
 * PUT     /products/:id          ->  updateProduct
 * DELETE  /products/:id          ->  deleteProduct
 */

'use strict';

var _ = require('lodash'),
  Product = require('./product.model'),
  User = require('../user/user.controller'),
  UserModel = require('../user/user.model'),
  mongoose = require('mongoose')
 ,  ObjectId = mongoose.Types.ObjectId;



//mongoose.createConnection('localhost', 'maat');

/**
 * createProduct: Method to create an product
 * @param name  String with the product's name //mandatory
 * @param category : Number with the product's category //mandatory
 * @param price : Number with the product's price
 * @param location : Point with the product's location //mandatory
 */


exports.createProduct = function (req, res){
  if(undefined !== req.query){
    var productModel = new Product();
    //Guardamos el producto en base de datos
    var productData = JSON.parse(JSON.stringify(req.query));
    Product.create(productData,function (err,product) {
      if (!err) {
        //Si todo va bien enviamos de vuelta el producto
        res.status(201).json(product);
      } else {
        console.log(err)
        return handleError(res, err);
      }
    });
  }else{
    return handleError(res, 'Product Required');
  }
};



/**
 * getProduct: Method to get an Product by id
 * @param product_id  ID of the product we want
 */
exports.getProduct = function(req, res, next) {
  Product.findById(new ObjectId(req.params.product_id), function(err, product) {
    if (err) {
      return handleError(res, err);
    } else {
      if (product) {
        res.status(200).json(product);
      } else {
          return res.status(404).send('Product: ' + req.params.product_id + ' not found');
      }
    }
  });
};


/**
 * getAllProducts: Method to get an All Products
 * @param location : Point with the product's location
 */
exports.getAllProducts = function(req, res, next) {
  var regex_product = req.query.product;
  console.log("peticion de servicios")
  console.log(req.query)
  if (req.query.owner) {
    User.apiRoutes(req, res);
    console.log(req.decoded.sub)
    Product.find({owner: req.decoded.sub},{}, function(err,products) {
      if (err) {
        return handleError(res, err);
      } else {
        return res.status(200).json(products);
      }
    });
  } else {
    if(undefined !== req.query.lat) {
      var searchObject = {$or:[
          {"name":{$regex : regex_product,$options : 'i'}},
          {"description":{$regex : regex_product,$options : 'i'}},
          {"category":{$regex : regex_product,$options : 'i'}}
        ]};
      if (req.query.price) {
        searchObject.price =  { $lt: parseInt(req.query.price) };
      }
      if (req.query.offers == 'true') {
        searchObject.offers =  { $exists: true };
      }
      if (req.query.startsNumber) {
        searchObject.average_rating =  { $gt: parseInt(req.query.startsNumber) - 1 };
      } 
      if (req.query.dayHourFrom) {
        searchObject.dayHourFrom =  { $gt: new Date (req.query.dayHourFrom) };
      }
      if (req.query.dayHourTo) {
        searchObject.dayHourTo =  { $lt: new Date (req.query.dayHourTo) };
      }
      if (req.query.discount) {
        searchObject.descuento =  { $gt: parseInt(req.query.discount) };
      }
      searchObject.location = {
        $near: [parseFloat(req.query.lat),parseFloat(req.query.long)],
        $maxDistance: parseFloat(req.query.distance)/101
      };
      if (req.query.contactsOnly == 'true') {
        if (User.apiRoutes(req, res)) {
          console.log(req.decoded)
        }
        UserModel.findOne({ _id: req.decoded.sub },{}, function(err, response) {
          if (err) {
            return handleError(res, 500, err);
          }
          if (response.contactos) {
            searchObject.owner =  { $in: response.contactos };
            console.log(searchObject)
            Product.find(searchObject,{}, function(err,products) {
            if (err) {
                return handleError(res, err);
              } else {
                return res.status(200).json(products);
              }
            });
          } else {
            return res.status(401).send('Este usuario no tiene contactos');
          }
        })
        //FALTA añadir buscar solo entre mis contatos en searchObject
      } else {
        console.log(searchObject)
        Product.find(searchObject,{}, function(err,products) {
          if (err) {
            return handleError(res, err);
          } else {
            return res.status(200).json(products);
          }
        });
      }
    }else if(undefined !== req.query.id) {
      console.log("getProduct1",new ObjectId(req.query.id));

      Product.findById(new ObjectId(req.query.id))
        .populate('owner')
        .exec(function (err, product) {
          if (err) {
            return handleError(res, err);
          } else {
            if (product) {
              console.log('The creator is %s', product.owner.name);
              res.status(200).json(product);
            } else {
              return res.status(404).send('Product: ' + req.query.id + ' not found');
            }
          }
        });
    };
  };

};

/**
 * updateProduct: Method to update an Product by id
 * @param product_id  ID of the product we want
 * @param name  String with the product's name //mandatory
 * @param category : Number with the product's category //mandatory
 * @param price : Number with the product's price
 * @param location : Point with the product's location //mandatory
 * @param reactive : String flag to active product
 */
exports.updateProduct = function(req, res, next) {
  req.params._id = new ObjectId(req.params.product_id);
  if(req.params.reactive){
    req.params.active  = true;
  }
  var updatedProductModel = new Product(req.params);
  Product.findByIdAndUpdate(req.params._id, updatedProductModel.toObject(), function(err, product) {
    if (err) {
      return handleError(res, err);
    } else {
      if (product) {
        res.status(200).json(product);
      } else {
        return res.status(404).send('Product: ' + req.params.product_id + 'Not Found');
      }
    }
  });
};


/**
 * deleteProduct: Method to make an logical delete of an Product
 * @param product_id  ID of the product we want
 */
exports.deleteProduct = function(req, res, next) {
  Product.findByIdAndUpdate(new Object(req.params.product_id), {$set: {active: false} },function(err, product) {
    if (err) {
      return handleError(res, err);
    } else {
      res.status(204).send('Product: ' + req.params.product_id + ' deleted successfully');
    }
  });
};




function handleError(res, err) {
  return res.status(500).send(err);
}
