'use strict'

// Dependencies
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/* Mongoose Schemas for product */
var ProductSchema = new Schema({

  name : { type: String, required: true },
  category : { type: String, required: true},// a saber
  active : { type: Boolean, default: true },
  owner : {  type: String },
  price : { type: Number, default: 0 },
  productType : { type: Number, default: 0 }, //0 normal, 1 destacado
  description : {type: String, required: true},
  location: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d',     // create the geospatial index
    required: true
  },
  offers: { type: String},
  images: { type: String},
  average_rating : { type: Number, default: 0 },//estas son las estrellas de valoración del servicio 
  dayHourFrom: { type: Date},//esto es el dia y la hora a la que esta disponible el servicio (inicio)
  dayHourTo: { type: Date},//esto es el dia y la hora a la que esta disponible el servicio (fin)
  descuento: { type: Number},//este es el descuento
  created: { type: Date, default: Date.now },
  videoUrl: {type: String},
  duracionDescuento: {type: Date}

}, {collection: 'product'});

ProductSchema.index({ location: '2dsphere' });
//exports
module.exports = mongoose.model('Product', ProductSchema);

