/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /users              ->  getAllUsers
 * POST    /users              ->  createUser
 * GET     /users/:id          ->  getUser
 * PUT     /users/:id          ->  updateUser
 * DELETE  /users/:id          ->  deleteUser
 */

'use strict';
var express = require('express');
var app = express();
var _ = require('lodash'),
  User = require('./user.model'),
  jwt = require('jwt-simple'),
  moment = require('moment'),
  bcrypt = require('bcrypt-nodejs'),
  Product = require('../product/product.model'),
  SALT_WORK_FACTOR = 10;

 // ,  ObjectId = mongoose.Types.ObjectId;
app.set('jwtTokenSecret', 'polepo1234');
var createToken = function(user) {  
  var payload = {
    sub: user._id,
    iat: moment().unix(),
    exp: moment().add(14, "days").unix(),
  };
  return jwt.encode(payload, app.get('jwtTokenSecret'));
};
// route middleware to verify a token
exports.apiRoutes = function(req, res) {
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  // decode token
  console.log(token)
  if (token != null && token != undefined && token != "null") {
    var decoded = jwt.decode(token, app.get('jwtTokenSecret'));
    if (!decoded) {
      console.log("error")
      return res.json({ success: false, message: 'Failed to authenticate token.' });    
    } else {
      // if everything is good, save to request for use in other routes
      req.decoded = decoded;
      return true
    }

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
};

exports.getUserPhoto = function(req, res) {
  User.findOne({ _id: req.query.id },{}, function(err, userData) {
    if (err) {
      return handleError(res, 500, err);
    } else {
      if (userData) {
        res.status(200).json(userData);
      } else {
        return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
      }
    }
  });
};

exports.getUserAddContact = function(req, res) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token != null && token != undefined && token != "null") {
    exports.apiRoutes(req, res)
    User.findOne({ _id: req.decoded.sub },{}, function(err, userData) {
      if (err) {
        return handleError(res, 500, err);
      } else {
        if (userData) {
          console.log(userData.contactos)
          if (userData.contactos) {
            userData.contactos = userData.contactos + ',' + req.query.ownerId;
          } else {
            userData.contactos = req.query.ownerId;
          }
          User.findByIdAndUpdate(req.decoded.sub,userData, function(err, user) {
            if (err) {
                return handleError(res, 500, err);
            } else {
              if (user) {
                res.status(200).json(user);
              } else {
                return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
              }
            }
          });
        } else {
          return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
        }
      }
    });
  }
}

exports.getUserContacts = function(req, res) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token != null && token != undefined && token != "null") {
    exports.apiRoutes(req, res)
    User.findOne({ _id: req.decoded.sub },{}, function(err, userData) {
      if (err) {
        return handleError(res, 500, err);
      } else {
        if (userData.contactos) {
          var contactosSplit = userData.contactos.split(",");
          var responseUser = [];
          var contadorName = 0;
          var contadorProduct = 0;
          var contadorName2 = -1;
          var contadorProduct2 = -1;
          var namesVector = [];
          var servicesVector = [];
          for (var contacto in contactosSplit) {
                contadorName2 = contadorName2 + 1 ;
            User.findOne({ _id: contactosSplit[contadorName2] },{}, function(err, userData) {
              if (err) {
                return handleError(res, 500, err);
              } else {
                namesVector[contadorName] = userData.nick;
                contadorName = contadorName + 1;
                contadorProduct2 = contadorProduct2 + 1 ;
                Product.find({owner: contactosSplit[contadorProduct2]},{}, function(err,products) {
                  if (err) {
                    return handleError(res, err);
                  } else {
                    servicesVector[contadorProduct] = products;
                    contadorProduct = contadorProduct + 1;
                    if (contadorProduct == contactosSplit.length) {
                      for (var i = 0; i < contadorProduct; i++) {
                        if (servicesVector[i]) {
                          if (servicesVector[i].length > 1) {
                            for (var j = 0; j < servicesVector[i].length; j++) {
                              servicesVector[i][j] = servicesVector[i][j].category;
                            }
                          } else {
                            if (servicesVector[i][0]) {
                              servicesVector[i] = [servicesVector[i][0].category];
                            }
                          }
                        }
                      }
                      for (var  i = 0; i < contadorName; i++) {
                        responseUser[i] = {};
                        responseUser[i].name = namesVector[i];
                        responseUser[i].contacts = servicesVector[i];
                      }

                      if (responseUser) {
                        res.status(200).json(responseUser);
                      } else {
                        return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
                      }
                    }
                  }
                });
              }
            })
          }
        } else {
          res.status(200).json();
        }
      }
    })
  }
}

// Get list of users
exports.getUser = function(req, res) {
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token != null && token != undefined && token != "null") {
    exports.apiRoutes(req, res)
    User.findOne({ _id: req.decoded.sub },{}, function(err, userData) {
      if (err) {
        return handleError(res, 500, err);
      } else {
        if (userData) {
          res.status(200).json(userData);
        } else {
          return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
        }
      }
    })
  } else {
  // fetch user and test password verification

    if(undefined !== req.query.email){
      User.findOne({ email: req.query.email }, function(err, user) {
        if (err) {
          return handleError(res, 500, err);
        }
        if (!user) {
          return handleError(res, 404, 'Faild User');
        }
        // test a matching password
        user.comparePassword(req.query.pass, function(err, isMatch) {
          if (err) {
            return handleError(res, 500, err);
          }
          if(isMatch){
            
            var userObj = JSON.parse(JSON.stringify(user.toObject()));
            delete userObj['password'];
            var expires = moment().add(7,'days').valueOf();
            res.status(200).json({
                token: createToken(user),
                expires: expires,
                data: userObj
            });
          } else {

            return handleError(res, 404, 'Faild User' );
          }
        });
      });
    }
  }
};


/**
 * createUser: Method to create an User by id
 * @param name  String with the user's name //mandatory
 * @param surname : String with the user's surname //mandatory
 * @param email : String with the user's email //mandatory
 */
exports.createUser = function (req, res){
  var new_user = JSON.parse(JSON.stringify(req.query));
  console.log(new_user)
  if(undefined !== new_user.user){
    //Guardamos el usuario en base de datos
    console.log(JSON.parse(new_user.user));
    User.create(JSON.parse(new_user.user),function (err,user) {
      if (!err) {
        //Si todo va bien enviamos de vuelta el usuario
        var userObj = JSON.parse(JSON.stringify(user.toObject()));
          delete userObj['password'];
          var expires = moment().add(7,'days').valueOf();
          res.status(201).json({
              token: createToken(user),
              expires: expires,
              data: userObj
        });
      } else {
        if(/email/.test(err.message)){
          return handleError(res, 409,"Email ya registrado");
        }else{
          console.log(err)
          return handleError(res, 409, err.message);
        }
      }
    });

  }else{
    return handleError(res, 'User Required');
  }
};


/**
 * updateUser: Method to update an User by id
 * @param user_id  ID of the user we want
 * @param name  String with the user's name //mandatory
 * @param surname : String with the user's surname //mandatory
 * @param email : String with the user's email //mandatory
 * @param reactive : String flag to active user
 */
exports.updateUser = function(req, res, next) {
  var updated_user = JSON.parse(JSON.parse(JSON.stringify(req.query)).user);
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if (token != null && token != undefined && token != "null") {
    exports.apiRoutes(req, res)
    updated_user._id = req.decoded.sub;
  } else {
    return handleError(res, 'User Required');
  }
  if(req.query.reactive){
    req.query.active  = true;
  }
  console.log(updated_user)
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {

    console.log("USERSCHEMA salt",salt);
    if (err) return err;

    // hash the password using our new salt
    if (updated_user.password){
      bcrypt.hash(updated_user.password, salt, null, function(err, hash) {
        if (err) return err;

        // override the cleartext password with the hashed one
        updated_user.password = hash;
        console.log("USERSCHEMA pass",updated_user.password);
        console.log(updated_user)
        User.findByIdAndUpdate(updated_user._id,updated_user, function(err, user) {
          if (err) {
              return handleError(res, 500, err);
          } else {
            if (user) {
              res.status(200).json(user);
            } else {
              return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
            }
          }
        });
      });
    } else {
      User.findByIdAndUpdate(updated_user._id,updated_user, function(err, user) {
        if (err) {
            return handleError(res, 500, err);
        } else {
          if (user) {
            res.status(200).json(user);
          } else {
            return handleError(res, 404, 'User: ' + req.query.user_id + 'Not Found');
          }
        }
      });
    }
    
  });
};



/**
 * deleteUser: Method to make an logical delete of an User
 * @param user_id  ID of the user we want
 */
exports.deleteUser = function(req, res, next) {
  User.findByIdAndUpdate(new Object(req.query.user_id), {$set: {active: false} },function(err, user) {
    if (err) {
      return handleError(res, err);
    } else {
      res.status(204).send('User: ' + req.query.user_id + ' deleted successfully');
    }
  });
};


function handleError(res, status, err) {
  return res.status(status).send(err);
}

