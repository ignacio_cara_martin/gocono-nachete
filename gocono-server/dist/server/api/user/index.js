'use strict';

var express = require('express');
var controller = require('./user.controller');

var router = express.Router();

router.get('/', controller.getUser);
router.get('/contacts', controller.getUserContacts);
router.get('/photo', controller.getUserPhoto);
router.get('/addContact', controller.getUserAddContact);
router.get('/:id', controller.getUser);
router.post('/', controller.createUser);
router.put('/', controller.updateUser);
router.patch('/:id', controller.updateUser);
router.delete('/:id', controller.deleteUser);

module.exports = router;
