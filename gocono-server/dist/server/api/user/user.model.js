'use strict';

// Dependencies
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcrypt-nodejs'),
  SALT_WORK_FACTOR = 10;

/* Mongoose Schemas for user */
var UserSchema = new Schema({
  name : { type: String, required: true },
  surname : { type: String, required: true},
  email : { type: String, required: true, index: {unique: true} },
  password: { type: String, required: true },
  nick: { type: String, index: {unique: true} },
  telefono: { type: Number },
  contactos: { type: String },
  web: { type: String},
  fax: { type: String},
  facebook: { type: String},
  linkedin: { type: String},
  idpropio: { type: String},
  dateIdPropio: { type: Date},
  avatar: { type: String, default:"http://redes.que.es/wp-content/uploads/2013/10/k-ola-ase.jpg"},
  video: { type: String},
  type : { type: Number, default: 1 },//0:admin, 1:user, 2: company
  active : { type: Boolean, default: true },
  created: { type: Date, default: Date.now },
  firstTime: { type: String},
  location: {
    type: [Number],  // [<longitude>, <latitude>]
    index: '2d'    // create the geospatial index
  }

}, {collection: 'user'});

UserSchema.pre('save', function(next) {
  var user = this;
  console.log("USERSCHEMA",user.name);
  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {

    console.log("USERSCHEMA salt",salt);
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, null, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      console.log("USERSCHEMA pass",user.password);

      next();
    });
  });
});

UserSchema.on('save', function(next) {
  console.log("USERSCHEMA ONSAVE");
});


UserSchema.post('save', function() {
  console.log('post save');
});

UserSchema.pre('findOne', function() {
  console.log('pre findOne');
});

UserSchema.post('findOne', function() {
  console.log('post findOne');
});



UserSchema.methods.comparePassword = function(candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if (err) {
      return callback(err);
    }

    console.log("USERSCHEMA isMatch",isMatch);
    callback(null, isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);
