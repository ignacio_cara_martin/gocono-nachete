'use strict';


var fs = require('fs'),
  dir = __dirname + '/../../../public/angular/upload/tmp',
  async = require('async');

exports.uploadFile = function(req, res) {
  // We are able to access req.files.file thanks to
  // the multiparty middleware
  var file = req.files.files,
    ret_err = null;


  async.each(
    // Pass items to iterate over
    file,
    // Pass iterator function that is called for each item
    function(file, callback) {
      fs.readFile(file.path, function(err, content) {
        if (!err) {
          fs.writeFile(dir+'/'+file.name, content, function (err) {
            if(err){
              ret_err = err;
            }
          });
        }

        // Calling callback makes it go to the next item.
        callback(err);
      });
    },
    // Final callback after each item has been iterated over.
    function(err) {
      if(err){
        handleError(res,err);
      }else{
        res.status(200).send('ok');
      }
    }
  );
};

exports.removeFile = function(req, res) {
  // TODO
};





function handleError(res, err) {
  return res.status(500).send(err);
}

