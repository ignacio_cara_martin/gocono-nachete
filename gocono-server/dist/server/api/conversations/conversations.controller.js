
/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /products              ->  getAllProducts
 * POST    /products              ->  createProduct
 * GET     /products/:id          ->  getProduct
 * PUT     /products/:id          ->  updateProduct
 * DELETE  /products/:id          ->  deleteProduct
 */

'use strict';

var _ = require('lodash'),
  Conversations = require('./conversations.model'),
  User = require('../user/user.controller'),
  UserModel = require('../user/user.model'),
  mongoose = require('mongoose')
 ,  ObjectId = mongoose.Types.ObjectId;



//mongoose.createConnection('localhost', 'maat');

/**
 * createProduct: Method to create an product
 * @param name  String with the product's name //mandatory
 * @param category : Number with the product's category //mandatory
 * @param price : Number with the product's price
 * @param location : Point with the product's location //mandatory
 */


exports.getAllConversations = function (req, res){

  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  var data = [];
  var noIntroducir = false;
  if (token != null && token != undefined && token != "null") {
    User.apiRoutes(req, res)
    console.log('a')
    console.log(req.decoded.sub)
    Conversations.find({ user1: req.decoded.sub },{}, function(err, mensajes) {
    	console.log(mensajes)
      if (err) {
        return handleError(res, 500, err);
      } else {
        for (var i = 0; i < mensajes.length; i++) {
      		noIntroducir = false;
      		for (var j = 0; j < data.length; j++) {
      			var t = 'a' + data[j]._id;
      			var l = 'a' + mensajes[i]._id;
      			if (t== l) {
      				noIntroducir = true;
      			}
      		} 
      		if (!noIntroducir) {
      			data.push(mensajes[i]);
      		}
      	}
      }
	    Conversations.find({ user2: req.decoded.sub },{}, function(err, mensajes) {
	    	console.log(mensajes)
	      if (err) {
	        return handleError(res, 500, err);
	      } else {
	      	for (var i = 0; i < mensajes.length; i++) {
	      		noIntroducir = false;
	      		for (var j = 0; j < data.length; j++) {
	      			var t = 'a' + data[j]._id;
	      			var l = 'a' + mensajes[i]._id;
	      			if (t == l) {
	      				noIntroducir = true;
	      			}
	      		} 
	      		if (!noIntroducir) {
	      			data.push(mensajes[i]);
	      		}
	      	}
	      }
	      console.log(data)
	      res.status(201).json(data);
	    });
    });
  }
};

exports.createConversations = function (req, res){
  if(undefined !== req.query){
    var conversationsModel = new Conversations();
    //Guardamos el producto en base de datos
    var productData = JSON.parse(JSON.stringify(req.query));
    Conversations.create(productData,function (err,conversations) {
      if (!err) {
        //Si todo va bien enviamos de vuelta el producto
        res.status(201).json(conversations);
      } else {
        console.log(err)
        return handleError(res, err);
      }
    });
  }else{
    return handleError(res, 'Conversations Required');
  }
};

exports.updateConversations = function(req, res, next) {
  var updatedProductModel = { mensaje: req.query.mensaje};
  console.log(updatedProductModel)
  Conversations.findByIdAndUpdate(req.query.id, updatedProductModel, function(err, conversation) {
  	console.log(conversation)
    if (err) {
      return handleError(res, err);
    } else {
      if (conversation) {
        res.status(200).json(conversation);
      } else {
        return res.status(404).send('Product: ' + req.query.id + 'Not Found');
      }
    }
  });
};