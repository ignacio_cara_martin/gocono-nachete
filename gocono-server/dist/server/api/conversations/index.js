'use strict';

var express = require('express');
var controller = require('./conversations.controller');

var router = express.Router();

router.get('/', controller.getAllConversations);
router.post('/', controller.createConversations);
router.put('/', controller.updateConversations);

module.exports = router;
