(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./src/js/themes/angular/app.js":[function(require,module,exports){
// Essentials
require('essential/js/main');

// Layout
require('layout/js/main');

// Sidebar
require('sidebar/js/main');

// Owl Carousel
require('media/js/carousel/main');

// CORE
require('./main');
},{"./main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/main.js","essential/js/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/main.js","layout/js/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/main.js","media/js/carousel/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/main.js","sidebar/js/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/main.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-carousel.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkCarousel = function () {

        if (! this.length) return;

        this.carousel();

        this.find('[data-slide]').click(function (e) {
            e.preventDefault();
        });

    };

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-collapse.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkCollapse = function () {

        if (! this.length) return;

        var target = this.attr('href') || this.attr('target');
        if (! target) return;

        this.click(function(e){
            e.preventDefault();
        });

        $(target).collapse({toggle: false});

    };

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-modal.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkModal = function () {

        if (! this.length) return;

        var target = this.attr('href') || this.attr('target');
        if (! target) return;

        this.click(function (e) {
            e.preventDefault();
        });

        $(target).modal({show: false});

    };

    /**
     * Modal creator for the demo page.
     * Allows to explore different modal types.
     * For demo purposes only.
     */

    // Process the modal via Handlebars templates
    var modal = function (options) {
        var source = $("#" + options.template).html();
        var template = Handlebars.compile(source);
        return template(options);
    };

    var randomId = function () {
        /** @return String */
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    $.fn.tkModalDemo = function () {

        if (! this.length) return;

        var targetId = this.attr('href') || this.attr('target'),
            target = $(targetId);

        if (! targetId) {
            targetId = randomId();
            this.attr('data-target', '#' + targetId);
        }

        targetId.replace('#', '');

        if (! target.length) {
            target = $(modal({
                id: targetId,
                template: this.data('template') || 'tk-modal-demo',
                modalOptions: this.data('modalOptions') || '',
                dialogOptions: this.data('dialogOptions') || '',
                contentOptions: this.data('contentOptions') || ''
            }));
            $('body').append(target);
            target.modal({show: false});
        }

        this.click(function (e) {
            e.preventDefault();
            target.modal('toggle');
        });

    };

    $('[data-toggle="tk-modal-demo"]').each(function () {
        $(this).tkModalDemo();
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-switch.js":[function(require,module,exports){
(function ($) {
    "use strict";

    $('[data-toggle="switch-checkbox"]').each(function () {

        $(this).bootstrapSwitch({
            offColor: 'danger'
        });

    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_check-all.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkCheckAll = function(){

        if (! this.length) return;

        this.on('click', function () {
            $($(this).data('target')).find(':checkbox').prop('checked', this.checked);
        });

    };

    // Check All Checkboxes
    $('[data-toggle="check-all"]').tkCheckAll();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_cover.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * Conserve aspect ratio of the orignal region. Useful when shrinking/enlarging
     * images to fit into a certain area.
     *
     * @param {Number} srcWidth Source area width
     * @param {Number} srcHeight Source area height
     * @param {Number} maxWidth Fittable area maximum available width
     * @param {Number} maxHeight Fittable area maximum available height
     * @return {Object} { width, heigth }
     */
    var aspectRatioFit = function (srcWidth, srcHeight, maxWidth, maxHeight) {

        var wRatio = maxWidth / srcWidth,
            hRatio = maxHeight / srcHeight,
            width = srcWidth,
            height = srcHeight;

        if (srcWidth / maxWidth < srcHeight / maxHeight) {
            width = maxWidth;
            height = srcHeight * wRatio;
        } else {
            width = srcWidth * hRatio;
            height = maxHeight;
        }

        return {width: width, height: height};
    };

    $.fn.tkCover = function () {

        if (! this.length) return;

        this.filter(':visible').not('[class*="height"]').each(function () {
            var t = $(this),
                i = t.find('img:first');

            if (i.length) {
                $.loadImage(i.attr('src')).done(function (img) {
                    t.height(i.height());
                    $('.overlay-full', t).innerHeight(i.height());
                    $(document).trigger('domChanged');
                });
            }
            else {
                i = t.find('.img:first');
                t.height(i.height());
                $('.overlay-full', t).innerHeight(i.height());
                $(document).trigger('domChanged');
            }
        });

        this.filter(':visible').filter('[class*="height"]').each(function () {
            var t = $(this),
                img = t.find('img') || t.find('.img');

            img.each(function () {
                var i = $(this);
                if (i.data('autoSize') === false) {
                    return true;
                }
                if (i.is('img')) {
                    $.loadImage(i.attr('src')).done(function (img) {
                        i.removeAttr('style');
                        i.css(aspectRatioFit(i.width(), i.height(), t.width(), t.height()));
                    });
                }
                else {
                    i.removeAttr('style');
                    i.css(aspectRatioFit(i.width(), i.height(), t.width(), t.height()));
                }
            });
        });

    };

    function height() {

        $('.cover.overlay').each(function () {
            $(this).tkCover();
        });

    }

    $(document).ready(height);
    $(window).on('load', height);

    var t;
    $(window).on("debouncedresize", function () {
        clearTimeout(t);
        t = setTimeout(height, 200);
    });

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_datepicker.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkDatePicker = function () {

        if (! this.length) return;

        if (typeof $.fn.datepicker != 'undefined') {

            this.datepicker();

        }

    };

    $('.datepicker').tkDatePicker();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_daterangepicker.js":[function(require,module,exports){
(function ($) {
    "use strict";

    $.fn.tkDaterangepickerReport = function () {
        var e = this;
        this.daterangepicker(
            {
                ranges: {
                    'Today': [ moment(), moment() ],
                    'Yesterday': [ moment().subtract('days', 1), moment().subtract('days', 1) ],
                    'Last 7 Days': [ moment().subtract('days', 6), moment() ],
                    'Last 30 Days': [ moment().subtract('days', 29), moment() ],
                    'This Month': [ moment().startOf('month'), moment().endOf('month') ],
                    'Last Month': [ moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month') ]
                },
                startDate: moment().subtract('days', 29),
                endDate: moment()
            },
            function (start, end) {
                var output = start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY');
                e.find('span').html(output);
            }
        );
    };

    $.fn.tkDaterangepickerReservation = function () {
        this.daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        });
    };

    $('.daterangepicker-report').tkDaterangepickerReport();

    $('.daterangepicker-reservation').tkDaterangepickerReservation();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_expandable.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     * @todo: Angular directive.
     */
    $.fn.tkExpandable = function () {

        if (! this.length) return;

        this.find('.expandable-content').append('<div class="expandable-indicator"><i></i></div>');

    };

    $('.expandable').each(function () {
        $(this).tkExpandable();
    });

    $('body').on('click', '.expandable-indicator', function(){
        $(this).closest('.expandable').toggleClass('expandable-open');
    });

    $('body').on('click', '.expandable-trigger:not(.expandable-open)', function(){
        $(this).addClass('expandable-open');
    });

}(jQuery));
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_iframe.js":[function(require,module,exports){
(function () {
    "use strict";

    // if we're inside an iframe, reload without iframe
    if (window.location != window.parent.location)
        top.location.href = document.location.href;

})();

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_minicolors.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     * @todo: Angular directive.
     */
    $.fn.tkMiniColors = function () {

        if (! this.length) return;

        if (typeof $.fn.minicolors != 'undefined') {

            this.minicolors({
                control: this.attr('data-control') || 'hue',
                defaultValue: this.attr('data-defaultValue') || '',
                inline: this.attr('data-inline') === 'true',
                letterCase: this.attr('data-letterCase') || 'lowercase',
                opacity: this.attr('data-opacity'),
                position: this.attr('data-position') || 'bottom left',
                change: function (hex, opacity) {
                    if (! hex) return;
                    if (opacity) hex += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(hex);
                    }
                },
                theme: 'bootstrap'
            });

        }

    };

    $('.minicolors').each(function () {

        $(this).tkMiniColors();

    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_nestable.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     * @todo: Angular directive.
     */
    $.fn.tkNestable = function () {

        if (! this.length) return;

        if (typeof $.fn.nestable != 'undefined') {

            this.nestable({
                rootClass: 'nestable',
                listNodeName: 'ul',
                listClass: 'nestable-list',
                itemClass: 'nestable-item',
                dragClass: 'nestable-drag',
                handleClass: 'nestable-handle',
                collapsedClass: 'nestable-collapsed',
                placeClass: 'nestable-placeholder',
                emptyClass: 'nestable-empty'
            });

        }

    };

    $('.nestable').tkNestable();

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_panel-collapse.js":[function(require,module,exports){
(function ($) {
    "use strict";

    var randomId = function() {
        /** @return String */
        var S4 = function() {
            return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkPanelCollapse = function () {

        if (! this.length) return;

        var body = $('.panel-body', this),
            id = body.attr('id') || randomId(),
            collapse = $('<div/>');

        collapse
            .attr('id', id)
            .addClass('collapse' + (this.data('open') ? ' in' : ''))
            .append(body.clone());

        body.remove();

        $(this).append(collapse);

        $('.panel-collapse-trigger', this)
            .attr('data-toggle', 'collapse' )
            .attr('data-target', '#' + id)
            .collapse({ trigger: false });

    };

    $('[data-toggle="panel-collapse"]').each(function(){
        $(this).tkPanelCollapse();
    });

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_progress-bars.js":[function(require,module,exports){
(function ($) {

    // Progress Bar Animation
    $('.progress-bar').each(function () {
        $(this).width($(this).attr('aria-valuenow') + '%');
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_select2.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSelect2 = function () {

        if (! this.length) return;

        if (typeof $.fn.select2 != 'undefined') {

            var t = this,
                options = {
                    allowClear: t.data('allowClear')
                };

            if (t.is('button')) return true;
            if (t.is('input[type="button"]')) return true;

            if (t.is('[data-toggle="select2-tags"]')) {
                options.tags = t.data('tags').split(',');
            }

            t.select2(options);

        }

    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSelect2Enable = function () {

        if (! this.length) return;

        if (typeof $.fn.select2 != 'undefined') {

            this.click(function () {
                $($(this).data('target')).select2("enable");
            });

        }

    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSelect2Disable = function () {

        if (! this.length) return;

        if (typeof $.fn.select2 != 'undefined') {

            this.click(function () {
                $(this.data('target')).select2("disable");
            });

        }

    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSelect2Flags = function () {

        if (! this.length) return;

        if (typeof $.fn.select2 != 'undefined') {

            // templating
            var format = function (state) {
                if (! state.id) return state.text;
                return "<img class='flag' src='http://select2.github.io/select2/images/flags/" + state.id.toLowerCase() + ".png'/>" + state.text;
            };

            this.select2({
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

        }

    };

    $('[data-toggle*="select2"]').each(function() {

        $(this).tkSelect2();

    });

    $('[data-toggle="select2-enable"]').tkSelect2Enable();

    $('[data-toggle="select2-disable"]').tkSelect2Disable();

    $("#select2_7").tkSelect2Flags();

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_selectpicker.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSelectPicker = function () {

        if (! this.length) return;

        if (typeof $.fn.selectpicker != 'undefined') {

            this.selectpicker({
                width: this.data('width') || '100%'
            });

        }

    };

    $(function () {

        $('.selectpicker').each(function () {
           $(this).tkSelectPicker();
        });

    });

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_show-hover.js":[function(require,module,exports){
(function ($) {

    var showHover = function () {
        $('[data-show-hover]').hide().each(function () {
            var self = $(this),
                parent = $(this).data('showHover');

            self.closest(parent).on('mouseover', function (e) {
                e.stopPropagation();
                self.show();
            }).on('mouseout', function () {
                self.hide();
            });
        });
    };

    showHover();

    window.showHover = showHover;

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_skin.js":[function(require,module,exports){
module.exports = (function () {
    var skin = $.cookie('skin');

    if (typeof skin == 'undefined') {
        skin = 'default';
    }
    return skin;
});
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_slider.js":[function(require,module,exports){
(function ($) {
    "use strict";

    var bars = function(el){
        $('.slider-handle', el).html('<i class="fa fa-bars fa-rotate-90"></i>');
    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSlider = function () {

        if (! this.length) return;

        if (typeof $.fn.slider != 'undefined') {

            this.slider();

            bars(this);

        }

    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSliderFormatter = function () {

        if (! this.length) return;

        if (typeof $.fn.slider != 'undefined') {

            this.slider({
                formatter: function (value) {
                    return 'Current value: ' + value;
                }
            });

            bars(this);

        }

    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSliderUpdate = function () {

        if (! this.length) return;

        if (typeof $.fn.slider != 'undefined') {

            this.on("slide", function (slideEvt) {
                $(this.attr('data-on-slide')).text(slideEvt.value);
            });

            bars(this);

        }

    };

    $('[data-slider="default"]').tkSlider();

    $('[data-slider="formatter"]').tkSliderFormatter();

    $('[data-on-slide]').tkSliderUpdate();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_summernote.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSummernote = function () {

        if (! this.length) return;

        if (typeof $.fn.summernote != 'undefined') {

            this.summernote({
                height: 300
            });

        }

    };

    $(function () {

        $('.summernote').each(function () {
           $(this).tkSummernote();
        });

    });

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tables.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkDataTable = function(){

        if (! this.length) return;

        if (typeof $.fn.dataTable != 'undefined') {

            this.dataTable();

        }

    };

    $('[data-toggle="data-table"]').tkDataTable();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tabs.js":[function(require,module,exports){
(function ($) {

    var skin = require('./_skin')();

    $('.tabbable .nav-tabs').each(function(){
        var tabs = $(this).niceScroll({
            cursorborder: 0,
            cursorcolor: config.skins[ skin ][ 'primary-color' ],
            horizrailenabled: true,
            oneaxismousemode: true
        });

        var _super = tabs.getContentSize;
        tabs.getContentSize = function() {
            var page = _super.call(tabs);
            page.h = tabs.win.height();
            return page;
        };
    });

    $('[data-scrollable]').getNiceScroll().resize();

    $('.tabbable .nav-tabs a').on('shown.bs.tab', function(e){
        var tab = $(this).closest('.tabbable');
        var target = $(e.target),
            targetPane = target.attr('href') || target.data('target');

        // refresh tabs with horizontal scroll
        tab.find('.nav-tabs').getNiceScroll().resize();

        // refresh [data-scrollable] within the activated tab pane
        $(targetPane).find('[data-scrollable]').getNiceScroll().resize();
    });

}(jQuery));
},{"./_skin":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_skin.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tooltip.js":[function(require,module,exports){
(function ($) {
    "use strict";

    // Tooltip
    $("body").tooltip({selector: '[data-toggle="tooltip"]', container: "body"});

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_touchspin.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkTouchSpin = function () {

        if (! this.length) return;

        if (typeof $.fn.TouchSpin != 'undefined') {

            this.TouchSpin();

        }

    };

    $('[data-toggle="touch-spin"]').tkTouchSpin();

}(jQuery));
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tree.js":[function(require,module,exports){
(function ($) {

    var tree_glyph_options = {
        map: {
            checkbox: "fa fa-square-o",
            checkboxSelected: "fa fa-check-square",
            checkboxUnknown: "fa fa-check-square fa-muted",
            error: "fa fa-exclamation-triangle",
            expanderClosed: "fa fa-caret-right",
            expanderLazy: "fa fa-angle-right",
            expanderOpen: "fa fa-caret-down",
            doc: "fa fa-file-o",
            noExpander: "",
            docOpen: "fa fa-file",
            loading: "fa fa-refresh fa-spin",
            folder: "fa fa-folder",
            folderOpen: "fa fa-folder-open"
        }
    },
    tree_dnd_options = {
        autoExpandMS: 400,
            focusOnClick: true,
            preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
            preventRecursiveMoves: true, // Prevent dropping nodes on own descendants
            dragStart: function(node, data) {
            /** This function MUST be defined to enable dragging for the tree.
             *  Return false to cancel dragging of node.
             */
            return true;
        },
        dragEnter: function(node, data) {
            /** data.otherNode may be null for non-fancytree droppables.
             *  Return false to disallow dropping on node. In this case
             *  dragOver and dragLeave are not called.
             *  Return 'over', 'before, or 'after' to force a hitMode.
             *  Return ['before', 'after'] to restrict available hitModes.
             *  Any other return value will calc the hitMode from the cursor position.
             */
            // Prevent dropping a parent below another parent (only sort
            // nodes under the same parent)
            /*
            if(node.parent !== data.otherNode.parent){
                return false;
            }
            // Don't allow dropping *over* a node (would create a child)
            return ["before", "after"];
            */
            return true;
        },
        dragDrop: function(node, data) {
            /** This function MUST be defined to enable dropping of items on
             *  the tree.
             */
            data.otherNode.moveTo(node, data.hitMode);
        }
    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkFancyTree = function(){

        if (! this.length) return;

        if (typeof $.fn.fancytree == 'undefined') return;

        var extensions = [ "glyph" ];
        if (typeof this.attr('data-tree-dnd') !== "undefined") {
            extensions.push( "dnd" );
        }
        this.fancytree({
            extensions: extensions,
            glyph: tree_glyph_options,
            dnd: tree_dnd_options,
            clickFolderMode: 3,
            checkbox: typeof this.attr('data-tree-checkbox') !== "undefined" || false,
            selectMode: typeof this.attr('data-tree-select') !== "undefined" ? parseInt(this.attr('data-tree-select')) : 2
        });

    };

    // using default options
    $('[data-toggle="tree"]').each(function () {
        $(this).tkFancyTree();
    });

}(jQuery));
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_wizard.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkWizard = function () {

        if (! this.length) return;

        if (typeof $.fn.slick == 'undefined') return;

        var t = this,
            container = t.closest('.wizard-container');

        t.slick({
            dots: false,
            arrows: false,
            slidesToShow: 1,
            rtl: this.data('rtl'),
            slide: 'fieldset',
            onAfterChange: function (wiz, index) {
                $(document).trigger('after.wizard.step', {
                    wiz: wiz,
                    target: index,
                    container: container,
                    element: t
                });
            }
        });

        container.find('.wiz-next').click(function (e) {
            e.preventDefault();
            t.slickNext();
        });

        container.find('.wiz-prev').click(function (e) {
            e.preventDefault();
            t.slickPrev();
        });

        container.find('.wiz-step').click(function (e) {
            e.preventDefault();
            t.slickGoTo($(this).data('target'));
        });

        $(document).on('show.bs.modal', function () {
            t.closest('.modal-body').hide();
        });

        $(document).on('shown.bs.modal', function () {
            t.closest('.modal-body').show();
            t.slickSetOption('dots', false, true);
        });

    };

    $('[data-toggle="wizard"]').each(function () {
        $(this).tkWizard();
    });

    /**
     * By leveraging events we can hook into the wizard to add functionality.
     * This example updates the progress bar after the wizard step changes.
     */
    $(document).on('after.wizard.step', function (event, data) {

        if (data.container.is('#wizard-demo-1')) {

            var target = data.container.find('.wiz-progress li:eq(' + data.target + ')');

            data.container.find('.wiz-progress li').removeClass('active complete');

            target.addClass('active');

            target.prevAll().addClass('complete');

        }

    });

}(jQuery));
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_carousel.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('carousel', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkCarousel();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_check-all.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'check-all') {
                        el.tkCheckAll();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_collapse.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'collapse') {
                        el.tkCollapse();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_cover.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('cover', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function () {
                        el.tkCover();
                    }, 200);
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_datepicker.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('datepicker', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkDatePicker();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_daterangepicker.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('daterangepickerReport', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkDaterangepickerReport();
                }
            };
        } ])
        .directive('daterangepickerReservation', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkDaterangepickerReservation();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_expandable.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('expandable', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkExpandable();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_minicolors.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('minicolors', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkMiniColors();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_modal.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'modal') {
                        el.tkModal();
                    }
                    if (attrs.toggle == 'tk-modal-demo') {
                        el.tkModalDemo();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_nestable.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('nestable', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkNestable();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_panel-collapse.js":[function(require,module,exports){
(function () {
    "use strict";

    var randomId = function () {
        /** @return String */
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    angular.module('app')
        .directive('toggle', [ '$compile', function ($compile) {
            return {
                restrict: 'A',
                priority: 100,
                compile: function (el, attrs) {

                    if (attrs.toggle !== 'panel-collapse') return;

                    var body = angular.element('.panel-body', el),
                        id = body.attr('id') || randomId(),
                        collapse = angular.element('<div/>');

                    collapse
                        .attr('id', id)
                        .addClass('collapse' + (el.data('open') ? ' in' : ''))
                        .append(body.clone());

                    body.remove();

                    el.append(collapse);

                    $('.panel-collapse-trigger', el)
                        .attr('data-toggle', 'collapse')
                        .attr('data-target', '#' + id)
                        .collapse({trigger: false})
                        .removeAttr('style');

                    return function (scope, el, attrs) {
                    };

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_select2.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'select2' || attrs.toggle == 'select2-tags') {
                        el.tkSelect2();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_selectpicker.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('selectpicker', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkSelectPicker();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_slider.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('slider', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.slider == 'default') {
                        el.tkSlider();
                    }

                    if (attrs.slider == 'formatter') {
                        el.tkSliderFormatter();
                    }

                }
            };
        } ]);

    angular.module('app')
        .directive('onSlide', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    el.tkSliderUpdate();

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_summernote.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('summernote', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkSummernote();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tables.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'data-table') {
                        el.tkDataTable();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tabs.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'tab') {
                        el.click(function(e){
                            e.preventDefault();
                        });
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_touchspin.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'touch-spin') {
                        el.tkTouchSpin();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tree.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'tree') {
                        el.tkFancyTree();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_wizard.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'wizard') {
                        el.tkWizard();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/main.js":[function(require,module,exports){
require('./_panel-collapse');
require('./_carousel');
require('./_check-all');
require('./_collapse');
require('./_cover');
require('./_datepicker');
require('./_daterangepicker');
require('./_expandable');
require('./_minicolors');
require('./_modal');
require('./_nestable');
require('./_select2');
require('./_selectpicker');
require('./_slider');
require('./_summernote');
require('./_touchspin');
require('./_tables');
require('./_tabs');
require('./_tree');
require('./_wizard');
},{"./_carousel":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_carousel.js","./_check-all":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_check-all.js","./_collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_collapse.js","./_cover":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_cover.js","./_datepicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_datepicker.js","./_daterangepicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_daterangepicker.js","./_expandable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_expandable.js","./_minicolors":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_minicolors.js","./_modal":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_modal.js","./_nestable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_nestable.js","./_panel-collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_panel-collapse.js","./_select2":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_select2.js","./_selectpicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_selectpicker.js","./_slider":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_slider.js","./_summernote":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_summernote.js","./_tables":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tables.js","./_tabs":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tabs.js","./_touchspin":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_touchspin.js","./_tree":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tree.js","./_wizard":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_wizard.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/main.js":[function(require,module,exports){
require('./_tabs');
require('./_tree');
require('./_show-hover');
require('./_daterangepicker');
require('./_expandable');
require('./_nestable');
require('./_cover');
require('./_tooltip');
require('./_tables');
require('./_check-all');
require('./_progress-bars');
require('./_iframe');
require('./_bootstrap-collapse');
require('./_bootstrap-carousel');
require('./_bootstrap-modal');
require('./_panel-collapse');

// Forms
require('./_touchspin');
require('./_select2');
require('./_slider');
require('./_selectpicker');
require('./_datepicker');
require('./_minicolors');
require('./_bootstrap-switch');
require('./_wizard');
require('./_summernote');
},{"./_bootstrap-carousel":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-carousel.js","./_bootstrap-collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-collapse.js","./_bootstrap-modal":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-modal.js","./_bootstrap-switch":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_bootstrap-switch.js","./_check-all":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_check-all.js","./_cover":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_cover.js","./_datepicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_datepicker.js","./_daterangepicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_daterangepicker.js","./_expandable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_expandable.js","./_iframe":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_iframe.js","./_minicolors":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_minicolors.js","./_nestable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_nestable.js","./_panel-collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_panel-collapse.js","./_progress-bars":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_progress-bars.js","./_select2":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_select2.js","./_selectpicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_selectpicker.js","./_show-hover":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_show-hover.js","./_slider":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_slider.js","./_summernote":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_summernote.js","./_tables":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tables.js","./_tabs":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tabs.js","./_tooltip":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tooltip.js","./_touchspin":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_touchspin.js","./_tree":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_tree.js","./_wizard":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_wizard.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_async.js":[function(require,module,exports){
function contentLoaded(win, fn) {

    var done = false, top = true,

        doc = win.document,
        root = doc.documentElement,
        modern = doc.addEventListener,

        add = modern ? 'addEventListener' : 'attachEvent',
        rem = modern ? 'removeEventListener' : 'detachEvent',
        pre = modern ? '' : 'on',

        init = function (e) {
            if (e.type == 'readystatechange' && doc.readyState != 'complete') return;
            (e.type == 'load' ? win : doc)[ rem ](pre + e.type, init, false);
            if (! done && (done = true)) fn.call(win, e.type || e);
        },

        poll = function () {
            try {
                root.doScroll('left');
            } catch (e) {
                setTimeout(poll, 50);
                return;
            }
            init('poll');
        };

    if (doc.readyState == 'complete') fn.call(win, 'lazy');
    else {
        if (! modern && root.doScroll) {
            try {
                top = ! win.frameElement;
            } catch (e) {
            }
            if (top) poll();
        }
        doc[ add ](pre + 'DOMContentLoaded', init, false);
        doc[ add ](pre + 'readystatechange', init, false);
        win[ add ](pre + 'load', init, false);
    }
}

module.exports = function(urls, callback) {

    var asyncLoader = function (urls, callback) {

        urls.foreach(function (i, file) {
            loadCss(file);
        });

        // checking for a callback function
        if (typeof callback == 'function') {
            // calling the callback
            contentLoaded(window, callback);
        }
    };

    var loadCss = function (url) {
        var link = document.createElement('link');
        link.type = 'text/css';
        link.rel = 'stylesheet';
        link.href = url;
        document.getElementsByTagName('head')[ 0 ].appendChild(link);
    };

    // simple foreach implementation
    Array.prototype.foreach = function (callback) {
        for (var i = 0; i < this.length; i ++) {
            callback(i, this[ i ]);
        }
    };

    asyncLoader(urls, callback);

};
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_breakpoints.js":[function(require,module,exports){
(function ($) {

    $(window).setBreakpoints({
        distinct: true,
        breakpoints: [ 320, 480, 768, 1024 ]
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_gridalicious.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkGridalicious = function () {

        if (! this.length) return;

        this.gridalicious({
            gutter: this.data('gutter') || 15,
            width: this.data('width') || 370,
            selector: '> div',
            animationOptions: {
                complete: function () {
                    $(window).trigger('resize');
                }
            }
        });

    };

    $('[data-toggle*="gridalicious"]').each(function () {
        $(this).tkGridalicious();
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_isotope.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkIsotope = function () {

        if (! this.length) return;

        this.isotope({
            layoutMode: this.data('layoutMode') || "packery",
            itemSelector: '.item'
        });

        /*
        this.isotope('on', 'layoutComplete', function(){
            $(window).trigger('resize');
        });
        */

    };

    $(function(){

        setTimeout(function () {
            $('[data-toggle="isotope"]').each(function () {
                $(this).tkIsotope();
            });
        }, 300);

        $(document).on('domChanged', function(){
            $('[data-toggle="isotope"]').each(function(){
                $(this).isotope();
            });
        });

    });

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_parallax.js":[function(require,module,exports){
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license
(function () {
    var lastTime = 0;
    var vendors = [ 'ms', 'moz', 'webkit', 'o' ];
    for (var x = 0; x < vendors.length && ! window.requestAnimationFrame; ++ x) {
        window.requestAnimationFrame = window[ vendors[ x ] + 'RequestAnimationFrame' ];
        window.cancelAnimationFrame = window[ vendors[ x ] + 'CancelAnimationFrame' ] || window[ vendors[ x ] + 'CancelRequestAnimationFrame' ];
    }

    if (! window.requestAnimationFrame)
        window.requestAnimationFrame = function (callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function () {
                    callback(currTime + timeToCall);
                },
                timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (! window.cancelAnimationFrame)
        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
}());

(function ($, window) {
    "use strict";

    $.fn.tkParallax = function () {

        if (Modernizr.touch) return;

        var getOptions = function (e) {
            return {
                speed: e.data('speed') || 4,
                translate: e.data('speed') || true,
                translateWhen: e.data('translateWhen') || 'inViewportTop',
                autoOffset: e.data('autoOffset'),
                offset: e.data('offset') || 0,
                opacity: e.data('opacity')
            };
        };

        var $window = $(window),
            $windowContent = $('.st-content-inner'),
            $element = this;

        var ticking = false,
            $scrollable = null,
            lastScrollTop = 0;

        var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

        var requestTick = function (e) {
            if (! ticking) {
                $scrollable = $(e.currentTarget);
                // although Safari has support for requestAnimationFrame,
                // the animation in this case is choppy so we'll just run it directly
                if (isSafari) {
                    animate();
                } else {
                    window.requestAnimationFrame(animate);
                    ticking = true;
                }
            }
        };

        // Translates an element on the Y axis using translate3d to ensure
        // that the rendering is done by the GPU
        var translateY = function (elm, value) {
            var translate = 'translate3d(0px,' + value + 'px, 0px)';
            elm.style[ '-webkit-transform' ] = translate;
            elm.style[ '-moz-transform' ] = translate;
            elm.style[ '-ms-transform' ] = translate;
            elm.style[ '-o-transform' ] = translate;
            elm.style.transform = translate;
        };

        var layers = $element.find('.parallax-layer');

        var init = function () {
            layers.each(function () {

                var layer = $(this),
                    layerOptions = getOptions(layer),
                    height = $element.outerHeight(true);

                if (layerOptions.translate) {
                    if (layer.is('img') && layerOptions.autoOffset) {
                        $.loadImage(layer.attr('src')).done(function () {
                            layer.removeAttr('style');
                            var layerHeight = layer.height();
                            var offset = layerHeight * 0.33;
                            if ((offset + height) > layerHeight) {
                                offset = layerHeight - height;
                            }
                            offset = offset * - 1;
                            layer.attr('data-offset', offset);
                            translateY(layer.get(0), offset);
                        });
                    }
                }

            });
        };

        init();
        $(window).on("debouncedresize", init);

        var animate = function () {
            var scrollTop = parseInt($scrollable.scrollTop());
            var scrollableTop = $scrollable.is($window) ? 0 : $scrollable.offset().top;
            var height = $element.outerHeight(true);
            var bodyPadding = {
                top: parseInt($(document.body).css('padding-top')),
                bottom: parseInt($(document.body).css('padding-bottom'))
            };
            var windowHeight = $scrollable.innerHeight();
            var windowBottom = scrollTop + windowHeight - (bodyPadding.bottom + bodyPadding.top);
            var top = $element.offset().top - scrollableTop - bodyPadding.top;
            var bottom = top + height;
            var topAbs = Math.abs(top);
            var pos = top / windowHeight * 100;
            var opacityKey = height * 0.5;
            var when = {};

            /*
             * ONLY when the scrollable element IS NOT the window
             */

            // when the element is anywhere in viewport
            when.inViewport = (bottom > 0) && (top < windowHeight);

            // when the top of the viewport is crossing the element
            when.inViewportTop = (bottom > 0) && (top < 0);

            // when the bottom of the viewport is crossing the element
            when.inViewportBottom = (bottom > 0) && (top < windowHeight) && (bottom > windowHeight);

            /*
             * ONLY when the scrollable element IS the window
             */

            if ($scrollable.is($window)) {

                // when the window is scrollable and the element is completely in the viewport
                when.inWindowViewportFull = (top >= scrollTop) && (bottom <= windowBottom);

                when.inWindowViewport2 = (top >= scrollTop) && (top <= windowBottom);

                when.inWindowViewport3 = (bottom >= scrollTop) && (bottom <= windowBottom);

                when.inWindowViewport4 = (bottom >= scrollTop) && (bottom >= windowHeight) && (height > windowHeight);

                // when the window is scrollable and the top of the viewport is crossing the element
                when.inWindowViewportTop = ! when.inWindowViewport2 && (when.inWindowViewport3 || when.inWindowViewport4);

                // when the window is scrollable and the bottom of the viewport is crossing the element
                when.inWindowViewportBottom = when.inWindowViewport2 && ! when.inWindowViewport3;

                // when the window is scrollable and the element is anywhere in viewport
                when.inWindowViewport = when.inWindowViewportTop || when.inWindowViewportBottom || when.inWindowViewportFull;

                when.inViewport = when.inWindowViewport;
                when.inViewportTop = when.inWindowViewportTop;
                when.inViewportBottom = when.inWindowViewportBottom;

                pos = (top - scrollTop) / windowHeight * 100;
            }

            if (when.inViewportTop && when.inViewportBottom) {
                when.inViewportBottom = false;
            }

            if (! isNaN(scrollTop)) {
                layers.each(function () {

                    var layer = $(this);
                    var layerOptions = getOptions(layer);

                    var ty = (windowHeight + height) - bottom;

                    if ($scrollable.is($window)) {
                        ty = windowBottom - top;
                    }

                    if (layerOptions.translate) {

                        var layerPos = (- 1 * pos * layerOptions.speed) + layerOptions.offset;
                        var layerHeight = layer.height();

                        if (when.inViewport && ! when.inViewportTop && ! when.inViewportBottom) {
                            if (layer.is('img') && layerHeight > height) {
                                if ((Math.abs(layerPos) + height) > layerHeight) {
                                    layerPos = (layerHeight - height) * - 1;
                                }
                            }
                            if (! layer.is('img')) {
                                layerPos = 0;
                            }
                        }

                        if (when.inViewportTop && ((layer.is('img') && layerHeight == height) || ! layer.is('img') )) {
                            layerPos = Math.abs(layerPos);
                        }

                        if (when.inViewportBottom && ! layer.is('img')) {
                            layerPos = height - ty;

                            // scrolling up
                            if (scrollTop < lastScrollTop) {
                                layerPos = layerPos * - 1;
                            }
                        }

                        if (when.inViewport) {
                            layerPos = (layerPos).toFixed(5);
                            if (layerHeight > $window.height() && scrollTop <= 0) {
                                layerPos = 0;
                            }
                            translateY(layer.get(0), layerPos);
                        }

                    }

                    if (layerOptions.opacity) {

                        // fade in
                        if (when.inViewportBottom) {

                            var y, yP;

                            if ($scrollable.is($window)) {

                                y = ty;
                                yP = (y / height).toFixed(5);

                                if (y > opacityKey) {
                                    layer.css({opacity: yP});
                                }
                                else {
                                    layer.css({opacity: 0});
                                }
                            }
                            else {
                                if (bottom < (windowHeight + opacityKey)) {

                                    y = (windowHeight + opacityKey) - bottom;
                                    yP = (y / opacityKey).toFixed(5);

                                    layer.css({opacity: yP});
                                } else {
                                    layer.css({opacity: 0});
                                }
                            }
                        }

                        // fade out
                        else if (when.inViewportTop) {
                            var topOrigin = $scrollable.is($window) ? scrollTop - top : topAbs;
                            if (topOrigin > opacityKey) {
                                layer.css({
                                    'opacity': (1 - (topOrigin / height)).toFixed(5)
                                });
                            } else {
                                layer.css({'opacity': 1});
                            }
                        }

                        // reset
                        else {
                            layer.css({'opacity': 1});
                        }

                        if (when.inViewportBottom && scrollTop <= 0) {
                            layer.css({'opacity': 1});
                        }

                    }

                });

                lastScrollTop = scrollTop;
            }

            ticking = false;
        };

        if ($windowContent.length) {
            $windowContent.scroll(requestTick);
        } else {
            $window.scroll(requestTick);
        }

    };

    $('.parallax').each(function () {
        $(this).tkParallax();
    });

})(jQuery, window);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_scrollable.js":[function(require,module,exports){
(function ($) {
    "use strict";

    var skin = require('./_skin')();

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkScrollable = function (options) {

        if (! this.length) return;

        var settings = $.extend({
            horizontal: false
        }, options);

        var nice = this.niceScroll({
            cursorborder: 0,
            cursorcolor: config.skins[ skin ][ 'primary-color' ],
            horizrailenabled: settings.horizontal
        });

        if (! settings.horizontal) return;

        var _super = nice.getContentSize;

        nice.getContentSize = function () {
            var page = _super.call(nice);
            page.h = nice.win.height();
            return page;
        };

    };

    $('[data-scrollable]').tkScrollable();

    $('[data-scrollable-h]').each(function () {

       $(this).tkScrollable({ horizontal: true });

    });

    var t;
    $(window).on('debouncedresize', function () {
        clearTimeout(t);
        t = setTimeout(function () {
            $('[data-scrollable], [data-scrollable-h]').getNiceScroll().resize();
        }, 100);
    });

}(jQuery));
},{"./_skin":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_skin.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_sidebar-pc.js":[function(require,module,exports){
(function ($) {
    "use strict";

    $.fn.tkSidebarSizePcDemo = function(){

        var t, spc_demo = this;

        if (! spc_demo.length) return;

        $(document)
            .on('sidebar.show', function(){
                $('#pc-open').prop('disabled', true);
            })
            .on('sidebar.hidden', function(){
                $('#pc-open').prop('disabled', false);
            });

        spc_demo.on('submit', function (e) {
            e.preventDefault();
            var s = $('.sidebar'), ve = $('#pc-value'), v = ve.val();
            ve.blur();
            if (! v.length || v < 25) {
                v = 25;
                ve.val(v);
            }
            s[ 0 ].className = s[ 0 ].className.replace(/sidebar-size-([\d]+)pc/ig, 'sidebar-size-' + v + 'pc');
            sidebar.open('sidebar-menu');
            clearTimeout(t);
            t = setTimeout(function () {
                sidebar.close('sidebar-menu');
            }, 5000);
        });

    };

    $('[data-toggle="sidebar-size-pc-demo"]').tkSidebarSizePcDemo();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_skin.js":[function(require,module,exports){
module.exports=require("/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_skin.js")
},{"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_skin.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/_skin.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_skins.js":[function(require,module,exports){
var asyncLoader = require('./_async');

(function ($) {

    var changeSkin = function () {
        var skin = $.cookie("skin"),
            file = $.cookie("skin-file");
        if (typeof skin != 'undefined') {
            asyncLoader([ 'css/' + file + '.min.css' ], function () {
                $('[data-skin]').removeProp('disabled').parent().removeClass('loading');
            });
        }
    };

    $('[data-skin]').on('click', function () {

        if ($(this).prop('disabled')) return;

        $('[data-skin]').prop('disabled', true);

        $(this).parent().addClass('loading');

        $.cookie("skin", $(this).data('skin'));

        $.cookie("skin-file", $(this).data('file'));

        changeSkin();

    });

    var skin = $.cookie("skin");

    if (typeof skin != 'undefined' && skin != 'default') {
        changeSkin();
    }

})(jQuery);
},{"./_async":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_async.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_gridalicious.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ '$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'gridalicious') {
                        $timeout(function(){
                            el.tkGridalicious();
                        }, 100);
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_isotope.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ '$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'isotope') {
                        $timeout(function(){
                            el.tkIsotope();
                        }, 100);
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_parallax.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('parallax', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkParallax();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_scrollable.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('scrollable', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el) {
                    el.tkScrollable();
                }
            };
        } ])
        .directive('scrollableH', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el) {
                    el.tkScrollable({ horizontal: true });
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_sidebar-pc.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'sidebar-size-pc-demo') {
                        el.tkSidebarSizePcDemo();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/main.js":[function(require,module,exports){
require('./_scrollable');
require('./_isotope');
require('./_parallax');
require('./_gridalicious');
require('./_sidebar-pc');
},{"./_gridalicious":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_gridalicious.js","./_isotope":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_isotope.js","./_parallax":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_parallax.js","./_scrollable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_scrollable.js","./_sidebar-pc":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_sidebar-pc.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/main.js":[function(require,module,exports){
require('./_breakpoints.js');
require('./_gridalicious.js');
require('./_scrollable.js');
require('./_skins');
require('./_isotope');
require('./_parallax');

// Sidebar Percentage Sizes Demo
require('./_sidebar-pc');
},{"./_breakpoints.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_breakpoints.js","./_gridalicious.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_gridalicious.js","./_isotope":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_isotope.js","./_parallax":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_parallax.js","./_scrollable.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_scrollable.js","./_sidebar-pc":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_sidebar-pc.js","./_skins":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/_skins.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_owl.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('owlBasic', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkOwlDefault();
                    }, 200);
                }
            };
        } ])
        .directive('owlMixed', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkOwlMixed();
                    }, 200);
                }
            };
        } ])
        .directive('owlPreview', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkOwlPreview();
                    }, 200);
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_slick.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('slickBasic', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkSlickDefault();
                    }, 200);
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/main.js":[function(require,module,exports){
require('./_owl');
require('./_slick');
},{"./_owl":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_owl.js","./_slick":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_slick.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/main.js":[function(require,module,exports){
require('./owl/main');
require('./slick/_default');
},{"./owl/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/main.js","./slick/_default":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/slick/_default.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/_default.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkOwlDefault = function () {

        if (! this.length) return;

        var c = this;
        c.owlCarousel({
            dots: true,
            items: c.data('items') || 4,
            responsive: {
                1200: {
                    items: c.data('itemsLg') || 4
                },
                992: {
                    items: c.data('itemsMg') || 3
                },
                768: {
                    items: c.data('itemsSm') || 3
                },
                480: {
                    items: c.data('itemsXs') || 2
                },
                0: {
                    items: 1
                }
            },
            rtl: this.data('rtl'),
            afterUpdate: function () {
                $(window).trigger('resize');
            }
        });

    };

    $(".owl-basic").each(function () {
        $(this).tkOwlDefault();
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/_mixed.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkOwlMixed = function () {

        if (! this.length) return;

        this.owlCarousel({
            items: 2,
            nav: true,
            dots: false,
            rtl: this.data('rtl'),
            navText: [ '<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>' ],
            responsive: {
                1200: {
                    items: 2
                },
                0: {
                    items: 1
                }
            }
        });

    };

    $(".owl-mixed").tkOwlMixed();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/_preview.js":[function(require,module,exports){
(function ($) {
    "use strict";

    var syncPosition = function (e, target) {
        if (e.namespace && e.property.name === 'items') {
            target.trigger('to.owl.carousel', [e.item.index, 300, true]);
        }
    };

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkOwlPreview = function () {

        if (! this.length) return;

        var target = $(this.data('sync')),
            preview = this,
            rtl = this.data('rtl');

        if (! target.length) return;

        this.owlCarousel({
            items: 1,
            slideSpeed: 1000,
            dots: false,
            responsiveRefreshRate: 200,
            rtl: rtl,
            nav: true,
            navigationText: [ '<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>' ]
        });

        this.on('change.owl.carousel', function(e){
            syncPosition(e, target);
        });

        target.owlCarousel({
            items: 5,
            responsive: {
                1200: {
                    items: 7
                },
                768: {
                    items: 6
                },
                480: {
                    items: 3
                },
                0: {
                    items: 2
                }
            },
            dots: false,
            nav: true,
            responsiveRefreshRate: 100,
            rtl: rtl,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        target.on('change.owl.carousel', function(e){
            syncPosition(e, preview);
        });

        target.find('.owl-item').click(function (e) {
            e.preventDefault();
            var item = $(this).data("owl-item");
            preview.trigger("to.owl.carousel", [item.index, 300, true]);
        });

    };

    $(".owl-preview").tkOwlPreview();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/main.js":[function(require,module,exports){
require('./_default');
require('./_mixed');
require('./_preview');
},{"./_default":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/_default.js","./_mixed":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/_mixed.js","./_preview":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/owl/_preview.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/carousel/slick/_default.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSlickDefault = function () {

        if (! this.length) return;

        if (typeof $.fn.slick == 'undefined') return;

        var c = this;
        
        c.slick({
            dots: true,
            slidesToShow: c.data('items') || 3,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: c.data('itemsLg') || 4
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: c.data('itemsMd') || 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: c.data('itemsSm') || 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: c.data('itemsXs') || 2
                    }
                },
                {
                    breakpoint: 0,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ],
            rtl: this.data('rtl'),
            onSetPosition: function () {
                $(window).trigger('resize');
            }
        });

        $(document).on('sidebar.shown', function(){
            c.slickSetOption('dots', true, true);
        });

    };

    $(".slick-basic").each(function () {
        $(this).tkSlickDefault();
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_breakpoints.js":[function(require,module,exports){
(function ($) {
    "use strict";

    var restore = function () {
            $("html").addClass('show-sidebar');
            $('.sidebar.sidebar-visible-desktop').not(':visible').each(function () {
                var options = sidebar.options($(this));
                sidebar.open($(this).attr('id'), options);
            });
        },
        hide = function () {
            $("html").removeClass('show-sidebar');
            $('.sidebar:visible').each(function () {
                sidebar.close($(this).attr('id'));
            });
        };

    $(window).bind('enterBreakpoint768', function () {
        if (! $('.sidebar').length) return;
        if ($('.hide-sidebar').length) return;
        restore();
    });

    $(window).bind('enterBreakpoint1024', function () {
        if (! $('.sidebar').length) return;
        if ($('.hide-sidebar').length) return;
        restore();
    });

    $(window).bind('enterBreakpoint480', function () {
        if (! $('.sidebar').length) return;
        hide();
    });

    if ($(window).width() <= 480) {
        if (! $('.sidebar').length) return;
        hide();
    }

})(jQuery);

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_collapsible.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSidebarCollapse = function () {

        if (! this.length) return;

        var sidebar = this;

        sidebar.find('.sidebar-menu > li > a').off('mouseenter');
        sidebar.find('.sidebar-menu > li.dropdown > a').off('mouseenter');
        sidebar.find('.sidebar-menu > li > a').off('mouseenter');
        sidebar.find('.sidebar-menu > li > a').off('click');
        sidebar.off('mouseleave');
        sidebar.find('.dropdown').off('mouseover');
        sidebar.find('.dropdown').off('mouseout');

        $('body').off('mouseout', '#dropdown-temp .dropdown');

        sidebar.find('ul.collapse')
            .off('shown.bs.collapse')
            .off('show.bs.collapse')
            .off('hide.bs.collapse')
            .off('hidden.bs.collapse');

        sidebar.find('#dropdown-temp').remove();

        sidebar.find('.hasSubmenu').removeClass('dropdown')
            .find('> ul').addClass('collapse').removeClass('dropdown-menu submenu-hide submenu-show')
            .end()
            .find('> a').attr('data-toggle', 'collapse').on('click', function(e){
                e.preventDefault();
            });

        sidebar.find('.collapse').on('shown.bs.collapse', function () {
            sidebar.find('[data-scrollable]').getNiceScroll().resize();
        });

        // Collapse
        sidebar.find('.collapse').on('show.bs.collapse', function (e) {
            e.stopPropagation();
            var parents = $(this).parents('ul:first').find('> li.open > ul');
            if (parents.length) {
                parents.collapse('hide').closest('.hasSubmenu').removeClass('open');
            }
            $(this).closest('.hasSubmenu').addClass('open');
        });

        sidebar.find('.collapse').on('hidden.bs.collapse', function (e) {
            e.stopPropagation();
            $(this).closest('.hasSubmenu').removeClass('open');
        });

        sidebar.find('.collapse').collapse({ toggle: false });

    };

    $('.sidebar[data-type="collapse"]').each(function(){
        $(this).tkSidebarCollapse();
    });

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_dropdown.js":[function(require,module,exports){
(function ($) {
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSidebarDropdown = function () {

        if (! this.length) return;

        var sidebar = this;

        sidebar.find('.collapse')
            .off('shown.bs.collapse')
            .off('show.bs.collapse')
            .off('hidden.bs.collapse');

        var nice = sidebar.find('[data-scrollable]').getNiceScroll()[ 0 ];

        nice.scrollstart(function () {
            if (! sidebar.is('[data-type="dropdown"]')) return;
            sidebar.addClass('scrolling');
            sidebar.find('#dropdown-temp > ul > li').empty();
            sidebar.find('#dropdown-temp').hide();
            sidebar.find('.open').removeClass('open');
        });

        nice.scrollend(function () {
            if (! sidebar.is('[data-type="dropdown"]')) return;
            $.data(this, 'lastScrollTop', nice.getScrollTop());
            sidebar.removeClass('scrolling');
        });

        sidebar.find('.hasSubmenu').addClass('dropdown').removeClass('open')
            .find('> ul').addClass('dropdown-menu').removeClass('collapse in').removeAttr('style')
            .end()
            .find('> a').removeClass('collapsed')
            .removeAttr('data-toggle');

        sidebar.find('.sidebar-menu > li.dropdown > a').on('mouseenter', function () {

            var c = sidebar.find('#dropdown-temp');

            sidebar.find('.open').removeClass('open');
            c.hide();

            if (! $(this).parent('.dropdown').is('.open') && ! sidebar.is('.scrolling')) {
                var p = $(this).parent('.dropdown'),
                    t = p.find('> .dropdown-menu').clone().removeClass('submenu-hide');

                if (! c.length) {
                    c = $('<div/>').attr('id', 'dropdown-temp').appendTo(sidebar);
                    c.html('<ul><li></li></ul>');
                }

                c.show();
                c.find('.dropdown-menu').remove();
                c = c.find('> ul > li').css({overflow: 'visible'}).addClass('dropdown open');

                p.addClass('open');
                t.appendTo(c).css({
                    top: p.offset().top - c.offset().top,
                    left: '100%'
                }).show();

                if (sidebar.is('.right')) {
                    t.css({
                        left: 'auto',
                        right: '100%'
                    });
                }
            }
        });

        sidebar.find('.sidebar-menu > li > a').on('mouseenter', function () {

            if (! $(this).parent().is('.dropdown')) {
                var sidebar = $(this).closest('.sidebar');
                sidebar.find('.open').removeClass('open');
                sidebar.find('#dropdown-temp').hide();
            }

        });

        sidebar.find('.sidebar-menu > li > a').on('click', function (e) {
            if ($(this).parent().is('.dropdown')) {
                e.preventDefault();
                e.stopPropagation();
            }
        });

        sidebar.on('mouseleave', function () {
            $(this).find('#dropdown-temp').hide();
            $(this).find('.open').removeClass('open');
        });

        sidebar.find('.dropdown').on('mouseover', function () {
            $(this).addClass('open').children('ul').removeClass('submenu-hide').addClass('submenu-show');
        }).on('mouseout', function () {
            $(this).children('ul').removeClass('.submenu-show').addClass('submenu-hide');
        });

        $('body').on('mouseout', '#dropdown-temp .dropdown', function () {
            $('.sidebar-menu .open', $(this).closest('.sidebar')).removeClass('.open');
        });

    };

    var transform_dd = function(){

        $('.sidebar[data-type="dropdown"]').each(function(){
            $(this).tkSidebarDropdown();
        });

    };

    var transform_collapse = function(){

        $('.sidebar[data-type="collapse"]').each(function(){
            $(this).tkSidebarCollapse();
        });

    };

    transform_dd();

    $(window).bind('enterBreakpoint480', function () {
        if (! $('.sidebar[data-type="dropdown"]').length) return;
        $('.sidebar[data-type="dropdown"]').attr('data-type', 'collapse').attr('data-transformed', true);
        transform_collapse();
    });

    function make_dd() {
        if (! $('.sidebar[data-type="collapse"][data-transformed]').length) return;
        $('.sidebar[data-type="collapse"][data-transformed]').attr('data-type', 'dropdown').attr('data-transformed', true);
        transform_dd();
    }

    $(window).bind('enterBreakpoint768', make_dd);

    $(window).bind('enterBreakpoint1024', make_dd);

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_options.js":[function(require,module,exports){
module.exports = function (sidebar) {
    return {
        "transform-button": sidebar.data('transformButton') === true,
        "transform-button-icon": sidebar.data('transformButtonIcon') || 'fa-ellipsis-h'
    };
};
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_sidebar-menu.js":[function(require,module,exports){
(function ($) {

    var sidebars = $('.sidebar');

    sidebars.each(function () {

        var sidebar = $(this);
        var options = require('./_options')(sidebar);

        if (options[ 'transform-button' ]) {
            var button = $('<button type="button"></button>');

            button
                .attr('data-toggle', 'sidebar-transform')
                .addClass('btn btn-default')
                .html('<i class="fa ' + options[ 'transform-button-icon' ] + '"></i>');

            sidebar.find('.sidebar-menu').append(button);
        }
    });

}(jQuery));
},{"./_options":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_options.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_sidebar-toggle.js":[function(require,module,exports){
(function ($) {
    "use strict";

    $('#subnav').collapse({'toggle': false});

    function mobilecheck() {
        var check = false;
        (function (a) {
            if (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
                check = true;
        })(navigator.userAgent || navigator.vendor || window.opera);
        return check;
    }

    (function () {

        var defaults = {
                effect: 'st-effect-1',
                duration: 550,
                overlay: false
            },

            containerSelector = '.st-container',

            eventtype = mobilecheck() ? 'touchstart' : 'click',

            getLayoutClasses = function (sidebar, direction) {

                var layoutClasses = sidebar.data('layoutClasses');

                if (! layoutClasses) {
                    var toggleLayout = sidebar.data('toggleLayout');
                    if (typeof toggleLayout == 'string') {
                        layoutClasses = toggleLayout.split(",").join(" ");
                        sidebar.data('layoutClasses', layoutClasses);
                        return layoutClasses;
                    }

                    var match = new RegExp('sidebar-' + direction + '(\\S+)', 'ig');
                    layoutClasses = $('html').get(0).className.match(match);
                    if (layoutClasses !== null && layoutClasses.length) {
                        layoutClasses = layoutClasses.join(" ");
                        sidebar.data('layoutClasses', layoutClasses);
                    }
                }

                return layoutClasses;

            },

            getSidebarDataOptions = function(sidebar){

                return {
                    effect: sidebar.data('effect'),
                    overlay: sidebar.data('overlay')
                };

            },

            animating = function () {

                if ($('body').hasClass('animating')) return true;
                $('body').addClass('animating');

                setTimeout(function () {
                    $('body').removeClass('animating');
                }, defaults.duration);

                return false;

            },

            reset = function (id, options) {

                var container = $(containerSelector);

                var target = typeof id !== 'undefined' ? '#' + id : container.data('stMenuTarget'),
                    sidebar = $(target);

                if (! sidebar.length) return false;
                if (! sidebar.is(':visible')) return false;
                if (sidebar.hasClass('sidebar-closed')) return false;

                var effect = typeof options !== 'undefined' && options.effect ? options.effect : container.data('stMenuEffect'),
                    direction = sidebar.is('.left') ? 'l' : 'r',
                    size = sidebar.get(0).className.match(/sidebar-size-(\S+)/).pop(),
                    htmlClass = 'st-effect-' + direction + size,
                    toggleLayout = sidebar.data('toggleLayout'),
                    layoutClasses = getLayoutClasses(sidebar, direction),
                    eventData = {
                        sidebar: sidebar,
                        target: target
                    };

                $(document).trigger('sidebar.hide', eventData);

                $('[data-toggle="sidebar-menu"][href="' + target + '"]')
                    .removeClass('active')
                    .closest('li')
                    .removeClass('active');

                $('html').addClass(htmlClass);
                sidebar.addClass(effect);
                container.addClass(effect);

                container.removeClass('st-menu-open st-pusher-overlay');

                setTimeout(function () {
                    $('html').removeClass(htmlClass);
                    if (toggleLayout) $('html').removeClass(layoutClasses);
                    sidebar.removeClass(effect);
                    container.get(0).className = 'st-container'; // clear
                    sidebar.addClass('sidebar-closed').hide();
                    $(document).trigger('sidebar.hidden', eventData);
                }, defaults.duration);

            },

            open = function (target, options) {

                var container = $(containerSelector);

                var sidebar = $(target);
                if (! sidebar.length) return false;

                // on mobile, allow only one sidebar to be open at the same time
                if ($(window).width() < 768 && container.hasClass('st-menu-open')) {
                    return reset();
                }

                $('[data-toggle="sidebar-menu"][href="' + target + '"]')
                    .addClass('active')
                    .closest('li')
                    .addClass('active');

                var effect = options.effect,
                    overlay = options.overlay;

                var direction = sidebar.is('.left') ? 'l' : 'r',
                    size = sidebar.get(0).className.match(/sidebar-size-(\S+)/).pop(),
                    htmlClass = 'st-effect-' + direction + size,
                    toggleLayout = sidebar.data('toggleLayout'),
                    layoutClasses = getLayoutClasses(sidebar, direction),
                    eventData = {
                        sidebar: sidebar,
                        target: target
                    };

                $(document).trigger('sidebar.show', eventData);

                $('html').addClass(htmlClass);
                sidebar.show().removeClass('sidebar-closed');

                container.data('stMenuEffect', effect);
                container.data('stMenuTarget', target);

                sidebar.addClass(effect);
                container.addClass(effect);
                if (overlay) container.addClass('st-pusher-overlay');

                setTimeout(function () {
                    container.addClass('st-menu-open');
                    sidebar.find('[data-scrollable]').getNiceScroll().resize();
                    $(window).trigger('resize');
                }, 25);

                setTimeout(function () {
                    if (toggleLayout) $('html').addClass(layoutClasses);
                    $(document).trigger('sidebar.shown', eventData);
                }, defaults.duration);

            },

            toggle = function (e) {

                e.stopPropagation();
                e.preventDefault();

                var a = animating();
                if (a) return false;

                var button = $(this),
                    target = button.attr('href'),
                    sidebar;

                if (target.length > 3) {
                    sidebar = $(target);
                    if (! sidebar.length) return false;
                }

                if (target.length < 3) {
                    var currentActiveElement = $('[data-toggle="sidebar-menu"]').not(this).closest('li').length ? $('[data-toggle="sidebar-menu"]').not(this).closest('li') : $('[data-toggle="sidebar-menu"]').not(this);
                    var activeElement = $(this).closest('li').length ? $(this).closest('li') : $(this);

                    currentActiveElement.removeClass('active');
                    activeElement.addClass('active');

                    if ($('html').hasClass('show-sidebar')) activeElement.removeClass('active');

                    $('html').removeClass('show-sidebar');

                    if (activeElement.hasClass('active')) $('html').addClass('show-sidebar');
                    return;
                }

                var dataOptions = getSidebarDataOptions(sidebar),
                    buttonOptions = {};

                if (button.data('effect')) buttonOptions.effect = button.data('effect');
                if (button.data('overlay')) buttonOptions.overlay = button.data('overlay');

                var options = $.extend({}, defaults, dataOptions, buttonOptions);

                if (! sidebar.hasClass('sidebar-closed') && sidebar.is(':visible')) {
                    reset(sidebar.attr('id'), options);
                    return;
                }

                open(target, options);

            };

        $('body').on(eventtype, '[data-toggle="sidebar-menu"]', toggle);

        $(document).on('keydown', null, 'esc', function () {

            var container = $(containerSelector);

            if (container.hasClass('st-menu-open')) {
                reset();
                return false;
            }

        });

        /**
         * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
         */
        $.fn.tkSidebarToggleBar = function () {

            if (! this.length) return;

            var sidebar = this;

            /* Sidebar Toggle Bar */
            if (sidebar.data('toggleBar')) {
                var bar = $('<a></a>');
                bar.attr('href', '#' + sidebar.attr('id'))
                    .attr('data-toggle', 'sidebar-menu')
                    .addClass('sidebar-toggle-bar');

                sidebar.append(bar);
            }

        };

        $('.sidebar').each(function(){
            $(this).tkSidebarToggleBar();
        });

        window.sidebar = {

            open: function (id, options) {

                var a = animating();
                if (a) return false;

                options = $.extend({}, defaults, options);

                return open('#' + id, options);

            },

            close: function (id, options) {

                options = $.extend({}, defaults, options);

                return reset(id, options);

            },

            options: getSidebarDataOptions

        };

    })();

})(jQuery);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-collapse.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('type', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (! el.is('.sidebar')) return;
                    if (attrs.type !== 'collapse') return;

                    el.tkSidebarCollapse();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-dropdown.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('type', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (! el.is('.sidebar')) return;
                    if (attrs.type !== 'dropdown') return;

                    el.tkSidebarDropdown();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-toggle-bar.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggleBar', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggleBar) {
                        el.tkSidebarToggleBar();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/main.js":[function(require,module,exports){
require('./_sidebar-dropdown');
require('./_sidebar-collapse');
require('./_sidebar-toggle-bar');
},{"./_sidebar-collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-collapse.js","./_sidebar-dropdown":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-dropdown.js","./_sidebar-toggle-bar":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-toggle-bar.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/main.js":[function(require,module,exports){
require('./_breakpoints');
require('./_sidebar-menu');
require('./_collapsible');
require('./_dropdown');
require('./_sidebar-toggle');

(function($){
    "use strict";

    /**
     * jQuery plugin wrapper for compatibility with Angular UI.Utils: jQuery Passthrough
     */
    $.fn.tkSidebar = function (options) {

        if (! this.length) return;

        var settings = $.extend({
            menuType: false,
            toggleBar: false
        }, options);

        var sidebar = this;

        if (settings.menuType == "collapse") {
            sidebar.tkSidebarCollapse();
        }

        if (settings.menuType == "dropdown") {
            sidebar.tkSidebarDropdown();
        }

        if (settings.toggleBar === true) {
            sidebar.tkSidebarToggleBar();
        }

    };

})(jQuery);
},{"./_breakpoints":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_breakpoints.js","./_collapsible":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_collapsible.js","./_dropdown":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_dropdown.js","./_sidebar-menu":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_sidebar-menu.js","./_sidebar-toggle":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/_sidebar-toggle.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/app.js":[function(require,module,exports){
(function(){
    'use strict';

    angular.module('app', [
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'uiGmapgoogle-maps',
        'ngFileUpload',
        'ui.bootstrap.datetimepicker',
        'pascalprecht.translate'
    ]);

    var app = angular.module('app')
        .config(
        [ '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ])
        .config(
        ['uiGmapGoogleMapApiProvider', function(GoogleMapApiProviders) {
            GoogleMapApiProviders.configure({
                v: '2.3.3',
                libraries: 'weather,geometry,visualization',
                key: 'AIzaSyCz2DCKjoWMnlrf0V23RGidagkYrwTce58'
            });
            }
        ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/config.router.js":[function(require,module,exports){
(function(){
    'use strict';

    angular.module('app')
        .run([ '$rootScope', '$state', '$stateParams',
            function ($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ])
        .config(
        [ '$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider
                    .otherwise('/search');

                $stateProvider
                    //edited view
                    .state('discover', {
                        abstract: true,
                        url: '/discover',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('/search', {
                        url: '/search',
                        templateUrl: 'containers/search.view.html',
                        controller: 'SearchCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'containers/userLogin.view.html',
                        controller: 'LoginCtrl'
                    })
                    .state('register', {
                        url: '/register',
                        templateUrl: 'containers/userRegister.view.html',
                        controller: 'RegisterUserCtrl'
                    })
                    .state('discover.listing', {
                        url: '/listing',
                        templateUrl: 'containers/listing.view.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l-sum-13';
                        }]
                    })
                    .state('discover.map-listing-list', {
                        url: '/map-listing-list',
                        templateUrl: 'containers/search-results.view.html',
                        controller: 'MapCtrl'
                    })
                    .state('registerService', {
                        url: '/registerService',
                        templateUrl: 'containers/register-service.view.html',
                        controller: 'registerService'
                    })
                    .state('userProfile', {
                        url: '/userProfile',
                        templateUrl: 'containers/user-profile.view.html',
                        controller: 'UpdateUserCtrl'
                    })
                    .state('viewService', {
                        url: '/viewService',
                        templateUrl: 'containers/view-services-registered.view.html',
                        controller: 'viewServiceCtrl'
                    })
                    .state('viewContacts', {
                        url: '/viewContacts',
                        templateUrl: 'containers/view-contacts.view.html',
                        controller: 'viewContactsCtrl'
                    })
                    .state('viewOffersForYou', {
                        url: '/viewOffersForYou',
                        templateUrl: 'containers/view-offers-for-you.view.html',
                        controller: 'viewOffersForYouCtrl'
                    })
                    .state('viewAllOffers', {
                        url: '/viewAllOffers',
                        templateUrl: 'containers/view-all-offers.view.html',
                        controller: 'viewAllOffersCtrl'
                    })
                    .state('viewConversations', {
                        url: '/viewConversations',
                        templateUrl: 'containers/view-conversations.view.html',
                        controller: 'viewConversationsCtrl'
                    })
                    



                    //No edited yet
                    .state('discover.map-full', {
                        url: '/map-full',
                        templateUrl: 'discover/map-full.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs';
                        }]
                    })
                    .state('discover.map-listing-grid', {
                        url: '/map-listing-grid',
                        templateUrl: 'discover/map-listing-grid.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-48pc-lg sidebar-r-40pc';
                        }]
                    })
                    .state('discover.listing-grid', {
                        url: '/listing-grid',
                        templateUrl: 'discover/listing-grid.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l-sum-13';
                        }]
                    })
                    .state('discover.listing-map', {
                        url: '/listing-map',
                        templateUrl: 'discover/listing-map.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l-sum-13';
                        }]
                    });

                $stateProvider
                    .state('property', {
                        abstract: true,
                        url: '/property',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('property.map', {
                        url: '/map',
                        templateUrl: 'property/map.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-48pc-lg sidebar-r-40pc';
                        }]
                    })
                    .state('property.property', {
                        url: '/property',
                        templateUrl: 'property/property.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-25pc-lg sidebar-r-30pc';
                        }]
                    })
                    .state('property.edit', {
                        url: '/edit',
                        templateUrl: 'property/edit.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-48pc-lg sidebar-r-40pc';
                        }]
                    });

                $stateProvider
                    .state('map-features', {
                        abstract: true,
                        url: '/map-features',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('map-features.themes', {
                        url: '/themes',
                        templateUrl: 'map-features/themes.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs';
                        }]
                    })
                    .state('map-features.filters', {
                        url: '/filters',
                        templateUrl: 'map-features/filters.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1';
                        }]
                    })
                    .state('map-features.markers', {
                        url: '/markers',
                        templateUrl: 'map-features/markers.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l2';
                        }]
                    });

                $stateProvider
                    .state('front', {
                        abstract: true,
                        url: '/front',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('front.home-map', {
                        url: '/home-map',
                        templateUrl: 'front/home-map.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.home-slider', {
                        url: '/home-slider',
                        templateUrl: 'front/home-slider.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.listing', {
                        url: '/listing',
                        templateUrl: 'front/listing.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.listing-grid', {
                        url: '/listing-grid',
                        templateUrl: 'front/listing-grid.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.property', {
                        url: '/property',
                        templateUrl: 'front/property.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    });
            }
        ]
    );

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/main.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .controller('AppCtrl', [ '$scope', '$state',
            function ($scope, $state) {

                $scope.app = {
                    settings: {
                        htmlClass: ''
                    }
                };

                $scope.$state = $state;

            } ]);

})();

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/contacts.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewContactsCtrl', function ($scope, $http, usersService, productsService, $location) {
		$scope.user = usersService.getUser();
	    $scope.$watch(function(){return usersService.getUser()}, function() {
	        $scope.user = usersService.getUser();
	        if ($scope.user) {
		    	$scope.init();
	        }
	    });
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
	    $scope.init = function () {
		    usersService.getUserContacts().then( function(response){
		    	if (response.err){
	              	$scope.user_error = response.err;
	            } else {
	          		$scope.services = response;
	          		$scope.servicesContacts = [];
	          		var entered = false;
	          		var noEnteredService = true;
			        for (var i = 0; i < $scope.services.length; i ++) {
			        	for (var item in $scope.services[i].contacts) {
			        		if ($scope.servicesContacts[0]) {
			        			noEnteredService = true;
					        	for (var j = 0; j < $scope.servicesContacts.length; j ++) {
					        		if ($scope.servicesContacts[j].name == $scope.services[i].contacts[item]) {
					        			noEnteredService = false;
					        			entered = false;
					        			for (var t = 0; t < $scope.servicesContacts[j].contacts.length; t++) {
					        				if ($scope.servicesContacts[j].contacts[t] == $scope.services[i].name) {
					        					entered = true;
					        				}
					        			}
					        			if (!entered) {
					        				$scope.servicesContacts[j].contacts.push($scope.services[i].name);
					        			}					        			
					        		}
					        	}
					        	if (noEnteredService) {
					        			$scope.servicesContacts.push({"name" :$scope.services[i].contacts[item], "contacts": [$scope.services[i].name]});
					        		}
					        } else {
					        	$scope.servicesContacts[0] = {"name" :$scope.services[i].contacts[item], "contacts": [$scope.services[i].name]};
					        }
				    	}
			        }
	          	}
	        });
	    }
	    $scope.init();
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/listing.controller.js":[function(require,module,exports){
angular.module('app')
    .controller('listingPanelCtrl', function ($scope, $http, usersService, productsService, $rootScope) {
        $scope.servicesFound = productsService.getProductList();
        $scope.range = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };
        $scope.selectService = function (service) {
            usersService.getOwner(service.owner).then( function () {
                $rootScope.$broadcast('serviceSelected', service);
            });
        }
        $scope.locateService = function(service) {
            $rootScope.$emit('locatedService', {
                location: service.coords,
                idService: service.id
            });
        }
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });
        
    })
    .controller('textMultipleCtrl', function ($scope, $http, usersService, productsService, $rootScope, conversationsService) {
        $scope.servicesFound = productsService.getProductList();
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });

        $scope.sendMessages = function(textMessageMultiple) {
            for (var i = 0; i < $scope.servicesFound.length; i++) {
                if ($('#' + $scope.servicesFound[i].id).is(':checked')) {
                    $scope.sendMessageUnic($scope.servicesFound[i].owner, textMessageMultiple);
                }
            }
        }

        $scope.sendMessageUnic = function(ownerId, textMessage) {
            var conversation = {
                user1: $scope.user._id,
                user2: ownerId,
                mensaje: '' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + ''
            };
            conversationsService.createConversations(conversation).then ( function() {
                if (conversationsService.getConversationUpdated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
    })
    .controller('serviceDetailsListCtrl', function ($scope, $http, usersService, productsService, $rootScope, conversationsService) {
        $scope.selectedService = $rootScope.selectedService;
        $scope.mensajeEnviado = false;
        $scope.owner = usersService.takeOwner();
        $scope.$watch(function(){return usersService.takeOwner()}, function() {
            $scope.owner = usersService.takeOwner();
            if ($scope.owner) {
                $scope.noEsContacto = true;
                if ($scope.user && $scope.user.contactos) {
                    var contactos= $scope.user.contactos.split(",");
                    for (var i = 0; i < contactos.length; i++) {
                        if (contactos[i] == $scope.owner._id) {
                            $scope.noEsContacto = false;
                        }
                    }
                }
            }
        });
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });
        if ($scope.owner) {
            $scope.noEsContacto = true;
            if ($scope.user && $scope.user.contactos) {
                var contactos= $scope.user.contactos.split(",");
                for (var i = 0; i < contactos.length; i++) {
                    if (contactos[i] == $scope.owner._id) {
                        $scope.noEsContacto = false;
                    }
                }
            }
        };
        $scope.addContacts = function () {
            var ownerId = $scope.owner._id;
            usersService.addContact(ownerId);

        }
        $scope.dismissServiceDetails = function () {
            $scope.selectedService = undefined;
            $rootScope.selectedService = undefined;
        }

        $scope.sendMessage = function(textMessage) {
            var conversation = {
                user1: $scope.user._id,
                user2: $scope.owner._id,
                mensaje: '' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + ''
            };
            conversationsService.createConversations(conversation).then ( function() {
                if (conversationsService.getConversationUpdated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
    })
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/map.controller.js":[function(require,module,exports){
angular.module('app')
    .controller('MapCtrl', function($scope, productsService, usersService, $rootScope) {

        $scope.map = {center: {latitude: 51.219053, longitude: 4.404418 }, zoom: 15 };
        $scope.options = {scrollwheel: false};

        //$scope.searchResults = JSON.parse(localStorage.getItem('results'));

        $scope.searchResults = productsService.getProductList();
        $scope.markers = [];

        _.each($scope.searchResults, function (result, index) {
            $scope.marker = {
                id: index,
                coords: {
                    latitude: result.coords.latitude,
                    longitude: result.coords.longitude
                },
                owner: result.owner,
                images: result.images,
                idService: result.id,
                name: result.name,
                icon: {url: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'},
                events: {
                    click: function (marker, eventName, args) {
                        usersService.getOwner(marker.model.owner).then( function () {
                            $rootScope.selectedService = marker.model;
                        });
                    }
                }
            };
            $scope.markers.push($scope.marker);
        });

        $scope.searchOptions = productsService.getSearchOptions();

        if($scope.searchOptions && $scope.searchOptions.coordinates){
            $scope.map.center = $scope.searchOptions.coordinates;
        }

        $scope.dismissServiceDetails = function () {
            $scope.selectedService = null;
        };

        $rootScope.$on('serviceSelected', function (event, service) {
            usersService.getOwner(service.owner);
            $rootScope.selectedService = service;
        });

        $rootScope.$on('locatedService', function(event, props){
            for (var i = 0; i < $scope.markers.length; i++) {
                if (props.idService == $scope.markers[i].idService && props.location.latitude == $scope.markers[i].coords.latitude && props.location.longitude == $scope.markers[i].coords.longitude) {
                    $scope.markers[i].icon = {url: 'http://labs.google.com/ridefinder/images/mm_20_yellow.png'};
                    $scope.map.center = props.location;
                } else {
                    $scope.markers[i].icon = {url: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'};
                }
            }
        });

    });
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/navbar.controller.js":[function(require,module,exports){
angular.module('app')
    .controller('navbarCtrl', function ($scope, $http, usersService, productsService,$location) {  
	    $scope.user = usersService.getUser();
	    $scope.$watch(function(){return usersService.getUser()}, function() {
	        $scope.user = usersService.getUser();
	    });
		$scope.loginUser = function(email, pass){
	      	usersService.loginUser(email,pass).then( function(response){
	          	if (response.err){
	            	$scope.login_error = response.err;
	          	}else{
		            $scope.login_error = null;
		            usersService.setUser(response.data.data);
	          	}
	        },
	        function(status){
	          	console.log(status);
	        });
	    };
	    $scope.logoutUser = function(){
			usersService.setUser(null);
			usersService.setToken("");
			localStorage.setItem('token',null);
            localStorage.setItem('user',null);
	    };
	    $scope.registerUser = function(user_data, pass2){
	      	if (user_data.password == pass2) {
				usersService.registerUser(user_data).then( function(response){
		          	if (response.err){
		          		console.log("error")
		          		console.log(response)
		            	$scope.register_error = response.err;
		          	}else{
			            usersService.setUser(response.data.data);
			            $scope.register_error = null;
			            $scope.user_data = {};
		          	}
		        },
		        function(status){
		          console.log(status);
		        });
	      	} else {
	      		$scope.register_error = "Las contraseñas no coinciden";
	      	}
		};
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/search.controller.js":[function(require,module,exports){

angular.module('app')
    .controller('SearchCtrl', function ($scope, $http, productsService, usersService, uiGmapGoogleMapApi, $location, $timeout) {
        $scope.products = null;
        $scope.distance = null;
        $scope.product_filter = false;
        $scope.busqueda = '';
        $scope.notProduct = false;
        if (usersService.getUser()) {
            $scope.userNoLoged = false;
        } else {
            $scope.userNoLoged = true;
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.userNoLoged = false;
            } else {
                $scope.userNoLoged = true;
            }
        });
        $scope.filterProduct = function () {
            $scope.product_filter = true;
        };
        $scope.hideFilterProduct = function () {
            $scope.product_filter = false;
        };

        $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.userLocation = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.userLocation = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };
        $scope.filterOptions = {};
        $scope.filterOptions.startsNumber = 0;
        $scope.startsSelected = function (value) {
            $scope.filterOptions.startsNumber = value;
        };
        var getProductsSearch = function () {
            var coordinates = {latitude: $scope.position.latitude, longitude: $scope.position.longitude};
            $scope.filterOptions.coordinates = coordinates;
            if (!$scope.filterOptions.distance) {
                $scope.filterOptions.distance = 90;
            }
            productsService.getProducts($scope.filterOptions).then(function(data) {
                $location.path('/discover/map-listing-list');
            });

        };
        $scope.searchProduct = function () {
            if($scope.filterOptions.product) {
               if (!$scope.position) {
                    $scope.location();
                    $timeout(function () {
                        getProductsSearch();
                    }, 2000);
                } else {
                    getProductsSearch();
                } 
            } else {
                $scope.notProduct = true;
            }
            
        };
        $scope.loginUser = function () {
            $location.path('/login');
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('searchTextField');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
            });
        };

        $scope.beforeRenderStartDate = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.filterOptions.endDate) {
                var activeDate = moment($scope.filterOptions.endDate);
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() >= activeDate.valueOf()) $dates[i].selectable = false;
                }
            }
        };

        $scope.beforeRenderEndDate = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.filterOptions.startDate) {
                var activeDate = moment($scope.filterOptions.startDate).subtract(1, $view).add(1, 'minute');
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() <= activeDate.valueOf()) {
                        $dates[i].selectable = false;
                    }
                }
            }
        };

    })
;

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/services.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('registerService', function ($scope, $location, Upload, $timeout, usersService, productsService) {
        $scope.service_data = {};
        if (usersService.getUser()) {
            $scope.service_data.owner = usersService.getUser()._id;
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.service_data.owner = usersService.getUser()._id;
            }
        });
        $scope.service_data.images = [];
        $scope.namesFiles = [];
        $scope.uploadFiles = function (files) {
            if (files[0]){
                var nameFile = "upload/tmp/" + files[0].name;
                $scope.namesFiles.push(files[0].name);
                $scope.service_data.images.push(nameFile);
                $scope.files = files;
                if (files && files.length) {
                    Upload.upload({
                        url: '/upload',
                        data: {
                            files: files
                        }
                    }).then(function (response) {
                        $timeout(function () {
                            $scope.result = response.data;
                        });
                    }, function (response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    }, function (evt) {
                        $scope.progress =
                        Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
            }
        };
        $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.service_data.location = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.service_data.location = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };
        $scope.beforeRenderdayHourFrom = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.service_data.dayHourTo) {
                var activeDate = moment($scope.service_data.dayHourTo);
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() >= activeDate.valueOf()) $dates[i].selectable = false;
                }
            }
        };

        $scope.beforeRenderdayHourTo = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.service_data.dayHourFrom) {
                var activeDate = moment($scope.service_data.dayHourFrom).subtract(1, $view).add(1, 'minute');
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() <= activeDate.valueOf()) {
                        $dates[i].selectable = false;
                    }
                }
            }
        };
        $scope.sendEntrar = function(){
          $location.path('/login');
        };  
        $scope.registerService = function(service_data) {
            if (service_data.name) {
                if (service_data.location) {
                    if (service_data.category) {
                        if (service_data.description) {
                            productsService.createProduct(service_data).then(
                                function(response){
                                  if (response.err){
                                    $scope.serviceRegisterError = response.err;
                                  }else{
                                    $scope.correctRegister = "Servicio registrado correctamente";
                                  }
                                },
                                function(status){
                                  console.log(status);
                                }
                            )
                        } else {
                            $scope.serviceRegisterError = "Introduzcala descripción del servicio";
                        }
                    } else {
                        $scope.serviceRegisterError = "Seleccione la categoría del servicio";
                    }
                } else {
                    $scope.serviceRegisterError = "Introduzca la ubicación del servicio";
                }
            } else {
                $scope.serviceRegisterError = "Introduzca el nombre del servicio";
            }
        }
        
    })
    .controller('viewServiceCtrl', function ($scope, $location, usersService, productsService, $http) {
        $scope.service_data = {};
        $scope.beforeService = false;
        $scope.nextService = false;
        $scope.dataNumber = 0;

        if (usersService.getUser()) {
            if (usersService.getUser()) {
                $scope.owner = usersService.getUser()._id;
            } else {
                $scope.owner = undefined;
            }
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.owner = usersService.getUser()._id;
            } else {
                $scope.owner = undefined;
            }
        });
        $scope.init = function () {
            productsService.getProductByOwner($scope.owner).then(function(data) {
                $scope.data = data;
                $scope.service_data = data[0];
                $scope.getImages();
                if ($scope.service_data) {
                    $scope.getPlace($scope.service_data.location[0],$scope.service_data.location[1]);
                    if ($scope.service_data.active) {
                        $scope.service_data.active = "Si";
                    } else {
                        $scope.service_data.active = "No";
                    }
                    if (data[1]) {
                        $scope.nextService = true;
                    }
                }
            });
        }

        $scope.getImages = function () {
            if ($scope.service_data.images) {
                $scope.namesFiles = $scope.service_data.images.split(',');
                for (var i = 0; i < $scope.namesFiles.length; i++) {
                    $scope.namesFiles[i] = $scope.namesFiles[i].substring(11,$scope.namesFiles[i].length);
                }
            }
        }
        $scope.getPlace = function (lat,long) {
            $http({
                method: "GET",
                url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true',
                dataType: "json"
            })
            .then(function (place) {
                $scope.locationPosition = place.data.results[0].formatted_address;
            });
            
        }
        $scope.changeService = function (action) {
            if (action == "next") {
                $scope.dataNumber = $scope.dataNumber + 1;
            } else {
                $scope.dataNumber = $scope.dataNumber - 1;
            }
            $scope.service_data = $scope.data[$scope.dataNumber];
            $scope.getImages();
            $scope.getPlace($scope.service_data.location[0],$scope.service_data.location[1]);
            if ($scope.service_data.active) {
                $scope.service_data.active = "Si";
            } else {
                $scope.service_data.active = "No";
            }
            $scope.beforeService = false;
            $scope.nextService = false;
            if ($scope.data[$scope.dataNumber + 1]) {
                $scope.nextService = true;
            }
            if ($scope.data[$scope.dataNumber - 1]) {
                $scope.beforeService = true;
            }
        }
        $scope.init();
        $scope.sendEntrar = function(){
          $location.path('/login');
        };
        
    })
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/user.controller.js":[function(require,module,exports){
'use strict';


/* global angular, document, window */

angular.module('app')
  .controller('LoginCtrl', function($scope, $timeout, $stateParams, usersService,$location) {
    $scope.user_data = {};
    $scope.user_data.email = null;
    $scope.user_data.pass = null;
    $scope.user = usersService.getUser();
    $scope.$watch(function(){return usersService.getUser()}, function() {
        $scope.user = usersService.getUser();
    });
    $scope.user_error = null;
    $scope.loginUser = function(user_data){

      usersService.loginUser(user_data.email,user_data.pass).then(
        function(response){
          if (response.err){
            $scope.user_error = response.err;
          }else{
            $scope.user = response.data;
            usersService.setUser(response.data.data);
            $location.path('/');
          }
        },
        function(status){
          console.log(status);
        }
      );

    };
    $scope.sendRegister = function(){
      $location.path('/register');
    };
  })

  .controller('RegisterUserCtrl', function($scope, $timeout, Upload,$stateParams, usersService,$location) {

    $scope.email = null;
    $scope.pass = null;
    $scope.user = null;
    $scope.user_error = null;
    $scope.user_data = {};
    $scope.registerUser = function(user_data) {
      if(user_data.password2 != user_data.password) {
        $scope.user_error = "Las contraseñas no coinciden";
      } else {
        var data = {
          name : user_data.name,
          surname : user_data.surname,
          password : user_data.password,
          email : user_data.email,
          nick : user_data.nick,
          telefono : user_data.telefono,
          web : user_data.web,
          fax : user_data.fax,
          facebook : user_data.facebook,
          linkedin : user_data.linkedin,
          video : user_data.video,
          avatar : $scope.user_data.avatar,
          idpropio : user_data.idpropio,
          dateIdPropio: user_data.dateIdPropio
        };
        usersService.registerUser(data).then(
          function(response){
            if (response.err){
              $scope.user_error = response.err;
            }else{
              $scope.user = response.data;
              $scope.user_error = null;
              $location.path('/login');
            }
          },
          function(status){
            console.log(status);
          }
        );
      }
    };

    $scope.uploadFiles = function (files) {
      $scope.user_data.avatar = "upload/tmp/" + files[0].name;
      $scope.files = files;
      if (files && files.length) {
        Upload.upload({
          url: '/upload',
          data: {
            files: files
          }
        }).then(function (response) {
          $timeout(function () {
            $scope.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    };

    $scope.sendEntrar = function(){
      $location.path('/login');
    };
  })


.controller('UpdateUserCtrl', function($scope, $timeout, Upload,$stateParams, usersService,$location) {

    $scope.user = usersService.getUser();
    $scope.$watch(function(){return usersService.getUser()}, function() {
      $scope.user = usersService.getUser();
      for (var key in $scope.user) {
        $scope.user_data[key] = $scope.user[key];
      }
      $scope.user_data.password = undefined;
    });
    $scope.user_error = null;
    $scope.user_data = {};
    for (var key in $scope.user) {
      $scope.user_data[key] = $scope.user[key];
    }
    $scope.user_data.password = undefined;
    $scope.updateUser = function(user_data) {
      if(user_data.password2 != user_data.password) {
        $scope.user_error = "Las contraseñas no coinciden";
      } else {
        var data = {
          name : user_data.name,
          surname : user_data.surname,
          password : user_data.password,
          email : user_data.email,
          nick : user_data.nick,
          telefono : user_data.telefono,
          web : user_data.web,
          fax : user_data.fax,
          facebook : user_data.facebook,
          linkedin : user_data.linkedin,
          video : user_data.video,
          avatar : $scope.user_data.avatar,
          idpropio : user_data.idpropio,
          dateIdPropio: user_data.dateIdPropio,
          _id: user_data._id
        };
        usersService.updateUser(data).then(
          function(response){
            if (response.err){
              $scope.user_error = response.err;
            }else{
              $scope.user = response.data;
              $scope.user_error = null;
              $location.path('/search');
            }
          },
          function(status){
            console.log(status);
          }
        );
      }
    };

    $scope.uploadFiles = function (files) {
      $scope.user_data.avatar = "upload/tmp/" + files[0].name;
      $scope.files = files;
      if (files && files.length) {
        Upload.upload({
          url: '/upload',
          data: {
            files: files
          }
        }).then(function (response) {
          $timeout(function () {
            $scope.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    };

    $scope.sendEntrar = function(){
      $location.path('/login');
    };
  });
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewAllOffersCtrl.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewAllOffersCtrl', function ($scope, $http, usersService, productsService, $location) {
        $scope.firstTimeEntered = true;
	    $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.locationUser = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.locationUser = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };

        $scope.acept = function() {
    		if ($scope.locationUser) {
                var dataSent = {
                    offers: true, 
                    product: '', 
                    coordinates: {latitude: $scope.locationUser[0],longitude: $scope.locationUser[1]},
                    distance: 90
                };
                productsService.getProducts(dataSent).then(function(data) {
                    $scope.firstTimeEntered = false;
                    $scope.servicesFound = productsService.getProductList();
                });
			    $scope.serviceRegisterError = "";
        	} else {
        		$scope.serviceRegisterError = "Introduzca la ubicación en la que desea encontrarlos";
        	}
        }
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewConversations.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewConversationsCtrl', function ($scope, $http, usersService, productsService, $location, conversationsService) {
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
            $scope.showConversacion = false;
	        conversationsService.getConversations().then( function () {
	        	$scope.conversaciones = conversationsService.getMensajes();
	        });
        });
        $scope.showConversacion = false;
        conversationsService.getConversations().then( function () {
        	$scope.conversaciones = conversationsService.getMensajes();
        });

        $scope.showConversationSelected = function (id) {
        	for (var i = 0; i < $scope.conversaciones.length; i++) {
        		if (id == $scope.conversaciones[i]._id) {
        			$scope.mensaejeOriginal = $scope.conversaciones[i].mensaejeOriginal;
        			$scope.mensaejeId = $scope.conversaciones[i]._id;
        			$scope.user2 = $scope.conversaciones[i].user.$$state.value.data;
        			$scope.showConversacion = true;
        			$scope.mensajes = $scope.conversaciones[i].mensaje;
        		}
        	}
        };

        $scope.sendMessage = function(textMessage) {
        	$scope.mensajes.push({text: textMessage, owner: $scope.user._id});
        	$scope.mensaejeOriginal = $scope.mensaejeOriginal + '2-2,IDCAMBIO,2-2' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + '';
        	$scope.conversationToSend = {
                id: $scope.mensaejeId,
                mensaje: $scope.mensaejeOriginal
            };
            console.log($scope.conversationToSend)
            conversationsService.updateConversations($scope.conversationToSend).then ( function() {
                if (conversationsService.getConversationCreated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewOffersForYouCtrl.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewOffersForYouCtrl', function ($scope, $http, usersService, productsService, $location) {
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
        $scope.getServices = function () {
        	var dataSent = {
        		offers: true, 
        		product: $scope.user.firstTime, 
        		coordinates: {latitude: $scope.user.location[0],longitude: $scope.user.location[1]},
        		distance: 90
        	};
	        productsService.getProducts(dataSent).then(function(data) {
	            $scope.servicesFound = productsService.getProductList();
	        });
	    };
	    $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.locationUser = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.locationUser = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };
	    if (usersService.getUser()) {
            if (usersService.getUser()) {
            	$scope.user = usersService.getUser();
                $scope.owner = $scope.user._id;
                if ($scope.user.firstTime){
                	$scope.firstTimeEntered = false;
                	$scope.getServices();
				} else {
					$scope.firstTimeEntered = true;
				}
            } else {
                $scope.owner = undefined;
                $scope.firstTimeEntered = true;
            }
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.user = usersService.getUser();
                $scope.owner = $scope.user._id;
				if ($scope.user.firstTime){
                	$scope.firstTimeEntered = false;
                	$scope.getServices();
				} else {
					$scope.firstTimeEntered = true;
				}
            } else {
                $scope.owner = undefined;
        		$scope.firstTimeEntered = true;
            }
        });

        $scope.acept = function(firstTime) {
        	if (firstTime) {
        		if ($scope.locationUser) {
					var data = {
						firstTime : firstTime,
						location: $scope.locationUser
				    };
				    usersService.updateUser(data).then(
				        function(response){
				            if (response.err){
				            }else{
				              $scope.user = response.data;
				              $scope.firstTimeEntered = false;
				              $scope.getServices();
				            }
				        },
				        function(status){
				            console.log(status);
				        }
				    );
				    $scope.serviceRegisterError = "";
	        	} else {
	        		$scope.serviceRegisterError = "Introduzca la ubicación en la que desea encontrarlos";
	        	}
        	} else {
        		$scope.serviceRegisterError = "Introduzca las categorias o productos que desea encontrar";
        	}
        }
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/main.js":[function(require,module,exports){
// Angular App
require('./angular/app.js');
require('./angular/config.router.js');
require('./angular/main.js');

// Directives
require('essential/js/angular/main');
require('layout/js/angular/main');
require('sidebar/js/angular/main');

require('media/js/angular/main');

//Controllers
require("./controllers/search.controller");
require("./controllers/navbar.controller");

require("./controllers/user.controller");
require("./controllers/listing.controller");
require("./controllers/map.controller");
require("./controllers/services.controller");
require("./controllers/contacts.controller");
require("./controllers/viewOffersForYouCtrl.controller");
require("./controllers/viewAllOffersCtrl.controller");
require("./controllers/viewConversations.controller");

//Services
require("./services/main.service");
require("./services/geolocationSvc");
require("./translate/translate");

},{"./angular/app.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/app.js","./angular/config.router.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/config.router.js","./angular/main.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/main.js","./controllers/contacts.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/contacts.controller.js","./controllers/listing.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/listing.controller.js","./controllers/map.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/map.controller.js","./controllers/navbar.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/navbar.controller.js","./controllers/search.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/search.controller.js","./controllers/services.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/services.controller.js","./controllers/user.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/user.controller.js","./controllers/viewAllOffersCtrl.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewAllOffersCtrl.controller.js","./controllers/viewConversations.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewConversations.controller.js","./controllers/viewOffersForYouCtrl.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewOffersForYouCtrl.controller.js","./services/geolocationSvc":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/geolocationSvc.js","./services/main.service":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/main.service.js","./translate/translate":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/translate/translate.js","essential/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/main.js","layout/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/main.js","media/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/main.js","sidebar/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/main.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/geolocationSvc.js":[function(require,module,exports){
angular.module('app').factory('geolocationSvc', ['$q', '$window', function ($q, $window) {

    'use strict';

    function getCurrentPosition() {
        var deferred = $q.defer();

        if (!$window.navigator.geolocation) {
            deferred.reject('Geolocation not supported.');
        } else {
            $window.navigator.geolocation.getCurrentPosition(
                function (position) {
                    deferred.resolve(position);
                },
                function (err) {
                    deferred.reject(err);
                });
        }

        return deferred.promise;
    }

    return {
        getCurrentPosition: getCurrentPosition
    };
}]);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/main.service.js":[function(require,module,exports){
angular.module('app')    
    .factory('conversationsService', function ($http, usersService) {
        var conversationCreated = null,
            mensajesTodos = [],
            conversationUpdated = null;
        var myService = {
            /**
             * @method getProducts : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            createConversations: function (conversation) {
                if (usersService.getToken()) {
                    return $http({
                        method: "POST",
                        url: '/api/conversations',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: conversation
                    })
                        .then(function (data) {
                                console.log(data)
                                conversationCreated = true;
                            },
                            function (error) {
                                alert(error.data)
                            });
                }
            },
            updateConversations: function (conversationToSend) {
                if (usersService.getToken()) {
                    return $http({
                        method: "PUT",
                        url: '/api/conversations',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: conversationToSend
                    })
                        .then(function (data) {
                                console.log(data)
                                conversationUpdated = true;
                            },
                            function (error) {
                                alert(error.data)
                            });
                }
            },
            getConversationCreated: function () {
                return conversationCreated;
            },
            getConversationUpdated: function () {
                return conversationUpdated;
            },
            getConversations: function () {
                return $http({
                    method: "GET",
                    url: '/api/conversations',
                    headers: {
                            'x-access-token': usersService.getToken()
                        },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json"
                })
                .then(function (mensajes) {
                    for (var i = 0; i < mensajes.data.length; i ++) {
                        mensajesTodos[i] = {};
                        console.log(mensajes.data[i]._id)
                        mensajesTodos[i]._id = mensajes.data[i]._id;
                        mensajesTodos[i].mensaje = [];
                        mensajesTodos[i].mensaejeOriginal = mensajes.data[i].mensaje;
                        var a = mensajes.data[i].mensaje.split('2-2,IDCAMBIO,2-2');
                        if (a[1]) {
                            for (var j = 0; j < a.length; j++) {
                                var b = a[j].split('-,IDCAMBIO,-');
                                mensajesTodos[i].mensaje[j] = {text: b[0], owner: b[1]};
                            }
                        } else {
                            var b = a[0].split('-,IDCAMBIO,-');
                            console.log(b)
                            mensajesTodos[i].mensaje[0] = {text: b[0], owner: b[1]};
                        }
                        if (usersService.getUser()._id == mensajes.data[i].user1) {
                            mensajesTodos[i].user = usersService.getOwner(mensajes.data[i].user2);
                        } else {
                            mensajesTodos[i].user = usersService.getOwner(mensajes.data[i].user1);
                        }
                    }
                    usersService.getOwner(usersService.getUser()._id).then ( function () {
                        return {err: null, data: mensajesTodos};
                    })
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;
                        return {err: my_err, data: mensajesTodos};
                    }
                })
            },
            getMensajes: function () {
                return mensajesTodos;
            }
        };
        return myService;
    })
    .factory('productsService', function ($http, usersService) {
        var product_list = [],
            best_product_list = [],
            anuncio = null,
            search_options;
        var myService = {
            /**
             * @method getProducts : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            getProducts: function (options) {

                search_options = options;

                if (options.endDate) {
                    options.endDate = moment(options.endDate).format();
                }
                if (options.startDate) {
                    options.startDate = moment(options.startDate).format();
                }

                var parameters = {
                    lat: options.coordinates.latitude,
                    long: options.coordinates.longitude,
                    distance: options.distance,
                    product: options.product,
                    price: options.price,
                    offers: options.offers,
                    startsNumber: options.startsNumber,
                    contactsOnly: options.contactsOnly,
                    dayHourFrom: options.startDate,
                    dayHourTo: options.endDate,
                    discount: options.discount
                };
                if (options.contactsOnly) {
                    parameters.email = usersService.getUser().email;
                }
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: parameters
                    })
                        .then(function (productsMarkers) {
                                product_list = prettyMarkers(productsMarkers.data);
                                return ({
                                    searchOptions: options,
                                    searchResults: product_list
                                });
                            },
                            function (error) {
                                alert(error.data)
                            });
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: parameters
                    })
                        .then(function (productsMarkers) {
                                product_list = prettyMarkers(productsMarkers.data);
                                return ({
                                    searchOptions: options,
                                    searchResults: product_list
                                });
                            },
                            function (error) {
                                alert(error.data)
                            });
                }

                //return promise;
            },

            /**
             * @method getProductInfo : service to obtain the data for a product in $scope
             * @params : id (Object) product id
             * @return : Object
             **/
            getProductInfo: function (product_id) {
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            id: product_id
                        }
                    })
                        .then(function (product) {
                            return product.data;
                        });
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            id: product_id
                        }
                    })
                        .then(function (product) {
                            return product.data;
                        });
                }

            },
            setProductList: function (products) {
                product_list = products;
            },
            getProductList: function () {
                return product_list;
            },
            setBestProductList: function (products) {
                best_product_list = products;
            },
            getBestProductList: function () {
                return best_product_list;
            },
            getSearchOptions: function () {
                return search_options;
            },
            setAnuncio: function (value) {
                anuncio = value;
            },
            getAnuncio: function () {
                return anuncio;
            },
            createProduct: function (data) {
                if (usersService.getToken()) {
                    return $http({
                        method: "POST",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: data
                    })
                        .then(function (product) {
                            return product.data;
                        });
                } else {
                    return $http({
                        method: "POST",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: data
                    })
                        .then(function (product) {
                            return product.data;
                        });
                }
            },
            // updateProduct: function (data) {
            //     console.log("updated")
            // },
            getProductByOwner: function (owner) {
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {owner: owner}
                    })
                    .then(function (products) {
                            return products.data;
                        },
                        function (error) {
                            alert(error.data)
                        });
                }
            }
        };
        return myService;
    })

    .factory('usersService', function ($http) {
        var my_err = null,
            user = null,
            ownerProduct = null,
            my_data;
        var token = "";
        if (window.localStorage.getItem('token') != undefined && window.localStorage.getItem('token') != null ) {
            token = window.localStorage.getItem('token');
            user = JSON.parse(window.localStorage.getItem('user'));
        }

        var myUserService = {
            /**
             * @method getMarkers : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            getToken: function () {
                return token;
            },
            setToken: function (value) {
                token = value;
            },
            loginUser: function (email, pass) {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            email: email,
                            pass: pass
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            email: email,
                            pass: pass
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                }
            },
            /**
             * @method registerUser : register an user
             * @params : user (Object) user data for register
             * @return : Object
             **/
            registerUser: function (user) {
                delete user.password2;
                if (token) {
                    return $http({
                        method: "POST",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            user: user
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            } else {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                } else {
                    return $http({
                        method: "POST",
                        url: '/api/users',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            user: user
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            } else {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                }
            },
            updateUser: function (userdata) {
                if(userdata) {
                    if (userdata.password2) {
                        delete userdata.password2;
                    }
                }
                return $http({
                    method: "PUT",
                    url: '/api/users',
                    headers: {
                        'x-access-token': token
                    },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    params: {
                        user: userdata
                    }
                })
                .then(function (userInfo) {
                    user = {};
                    console.log(user)
                    if (token) {
                        return $http({
                            method: "GET",
                            url: '/api/users',
                            headers: {
                                'x-access-token': token
                            },
                            withCredentials: true,
                            contentType: "application/x-www-form-urlencoded",
                            dataType: "json"
                        })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            user = userInfo.data
                            localStorage.setItem('user', JSON.stringify(userInfo.data));
                            console.log(user)
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                    }
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    } else {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    }
                })
            },
            getUserContacts: function (userdata) {
                return $http({
                    method: "GET",
                    url: '/api/users/contacts',
                    headers: {
                        'x-access-token': token
                    },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json"
                })
                .then(function (userInfo) {
                    return userInfo.data
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    } else {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    }
                })
            },
            /**
             * @method getUsertInfo : service to obtain the data for an user in $scope
             * @params : id (Object) user email
             * @return : Object
             **/
            getUsertInfo: function (user_email) {
                //TODO
            },
            setUser: function (new_user) {
                user = new_user;
            },
            getUser: function () {
                return user;
            },
            getOwner: function (ownerId) {
                return $http({
                    method: "GET",
                    url: '/api/users/photo',
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    params: {
                        id: ownerId
                    }
                })
                .then(function (userInfo) {
                    ownerProduct = userInfo.data
                    my_data = userInfo.data;
                    return {err: null, data: my_data};
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;
                        return {err: my_err, data: my_data};
                    }
                })
            },
            
            takeOwner: function () {
                return ownerProduct;
            },
            getUserUpdated: function() {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json"
                    })
                    .then(function (userInfo) {
                        my_data = userInfo.data;
                        token = userInfo.data.token;
                        localStorage.setItem('token', token);
                        localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                        return {err: null, data: my_data};
                        //return productsMarkers.data;
                    }).catch(function (err) {
                        // recover here if err is 404
                        if (err.status === 404) {
                            my_err = err.data;

                            return {err: my_err, data: my_data};
                        }
                    })
                }
            },
            addContact: function (ownerId) {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users/addContact',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            ownerId: ownerId
                        }
                    })
                    .then(function (userInfo) {
                        user = userInfo.data;
                        localStorage.setItem('user', JSON.stringify(userInfo.data));
                        return {err: null, data: my_data};
                        //return productsMarkers.data;
                    }).catch(function (err) {
                        // recover here if err is 404
                        if (err.status === 404) {
                            my_err = err.data;

                            return {err: my_err, data: my_data};
                        }
                    })
                }
            }
        };
        return myUserService;
    })
;


/**
 * @method prettyMarkers : create a correct mark for the map with the services data
 * @params : marker (array) is an array of objects
 * @return : array
 **/
function prettyMarkers(markers) {

    var marker = {
        id: null,
        name: null,
        towards: null,
        icon: null,
        coords: {},
        options: {}
    };
    var return_markers = [],
        return_best = [];

    angular.forEach(markers, function (mark, key) {
        marker = {
            id: mark._id,
            name: mark.name,
            owner: mark.owner,
            price: mark.price,
            distance: mark.distance,
            images: mark.images,
            productType: mark.productType,
            icon: 'http://google-maps-icons.googlecode.com/files/bookstore.png',
            coords: {
                latitude: mark.location[0],
                longitude: mark.location[1]
            }
        };
        return_markers.push(marker);
    });
    return return_markers;
}


/*
 *@method arrayObjectIndexOf: Helper Function to serach indexOf
 * @params myArray (Array), the array to search
 * @params searchTerm (String), string to search into the array
 * @params property (String), the index value
 *
 */
function arrayObjectIndexOf(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) {
            return i;
        }
    }
    return -1;
}

/*
 *@method removeArrayObject: Helper Function to remove an object from the array
 * @params myArray (Array), the array to search
 * @params searchTerm (String), string with the id to remove
 * @params property (String), the index value
 *
 */
function removeArrayObject(myArray, searchTerm, property) {
    var array_modify = [];
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] !== searchTerm) {
            array_modify.push(myArray[i]);
        }
    }
    return array_modify;
}



},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/translate/translate.js":[function(require,module,exports){
'use strict';
var trans_en = {};
var trans_es = {};
angular.module('app').config(function ($translateProvider) {
    $translateProvider.translations('en', trans_en)
        .translations('es', trans_es)
        .registerAvailableLanguageKeys(['en', 'es'], {
            'en_US': 'en',
            'en_UK': 'en'
        })
        .determinePreferredLanguage()
        .fallbackLanguage('en', 'es');
    $translateProvider.useSanitizeValueStrategy('escaped');
});


trans_en = {
    'searchLabel' : 'Search'
};
trans_es = {
    'searchLabel' : 'Buscar',
    'contactsLoged': 'PARA VER SUS CONTACTOS DEBE ESTAR LOGEADO',
    'entrar' : 'Entrar',
    'userNoContacts' : "No tienes ningun contacto añadido",
    'Gocono' : "Gocono",
    'error!' : 'Error!',
    'registro': 'Registro',
    'nombre' : 'Nombre',
    'apellidos' : 'Apellidos',
    'nick' : 'Nick',
    'email' : 'Email',
    'contraseña' : 'Contraseña',
    'repetirContraseña' : 'Repetir contraseña',
    'aceptar' : 'Aceptar',
    'editarPerfil': 'Editar perfil',
    'salir' : 'Salir',
    'precio': 'Precio',
    'Día/Hora' : 'Día/Hora',
    'lanzarComunicacion' : 'Lanzar comunicacion',
    'descripcion' : 'Descripcion',
    'añadirContactos' : 'Añadir a contactos',
    'registrarNuevoServicio' : 'Registrar nuevo servicio',
    'paraRegistrarUnServicioDebeIniciarSesión' : 'Para registrar un servicio debe iniciar sesión',
    'localizacion' : 'Localización',
    'categoria' : 'Categoria',
    'activo' : 'Activo',
    'si' : 'Si',
    'no' : 'No',
    'tipoProducto' : 'Tipo de producto',
    'descuento' : 'Descuento',
    'duracionDescuento' : 'Duración descuento',
    'videoExplicativo' : 'Video explicativo',
    'elegirDía/horaServicioDesde:' : 'Elegir día/hora servicio Desde:',
    'elegirDía/horaServicioHasta:' : 'Elegir día/hora servicio Hasta:',
    'ofertas' : 'Ofertas',
    'registrarServicio' : 'Registrar servicio',
    'verTodasOfertas' : 'Ver todas las ofertas',
    'siguienteServicio' : 'Siguiente servicio',
    'anteriorServicio' : 'Anterior servicio',
    'paraSecciónLogeado' : 'Para ver esta sección debe estar logeado',
    'serviciosPreferidos' : 'Servicios preferidos',
    'verOfertasTi' : 'Ver ofertas para tí',
    'registrarse' : 'Registrarse',
    'telefono' : 'Teléfono',
    'linkedin' : 'Linkedin',
    'facebook' : 'Facebook',
    'web' : 'Web',
    'video' : 'Vídeo',
    'fax' : 'Fax',
    'idPropio' : 'Id. Propio',
    'añadirFechaIdPropio' : 'Añadir fecha al Id. Propio',
    'eligeFotoPerfil' : 'Elige una foto de perfil',
    'seleccionarFoto' : 'Seleccionar Foto',
    'SiRegistradoInicieSesion' : 'Si ya esta registrado, inicie sesión',
    'iniciarSesionMensaje' : 'Inicia Sesión en gocono para publicar y contratar servicios',
    'mensajeRegistrarse' : 'Si aun no esta registrado, hágalo',
    'cambiarContraseña' : 'Cambiar contraseña',
    'guardarCambios' : 'Guardar cambios',
    'mensajeVerPerfilIniciarSesion' : 'Para ver el perfil, debe iniciar sesión',
    'introduzcaServicioBuscar' : 'Error Introduzca el servicio que desea buscar.',
    'filtrosAdicionales' : 'Filtros adicionales',
    'precioMaximo' : 'Precio máximo',
    'distanciaMaximaLocalización' : 'Distancia máxima de localización en Km',
    'tengaOfertas' : 'Tenga ofertas el profesional',
    'valoracionMinima' : 'Valoración mínima',
    'buscarContactos' : 'Buscar solo entre mis contactos',
    'descuentoMinimo' : 'Descuento mínimo',
    'accesoProfesional/Empresa' : 'Acceso profesional/Empresa',
    'verServicio' : 'Ver servicio',
    'verConversaciones' : 'Ver conversaciones',
    'verContactos' : 'Ver contactos',
    'servicios' : 'Servicios',
    'motor' : 'Motor',
    'bicicletas' : 'Bicicletas',
    'mascotas' : 'Mascotas',
    'cursos' : 'Cursos',
    'hogar' : 'Hogar',
    'masajes' : 'Masajes',
    'reparacion' : 'Reparación',
    'especiales' : 'Especiales',
    'otros' : 'Otros',
    'eligeFotosServicio' : 'Elige las fotos del servicio',
    'fotosServicio' : 'Fotos del servicio',
    'verConversaciones': 'Conversaciones',
    'enviar' : 'Enviar'


};


/*check if the struct of each translation have all the objects*/
var a, keytrans_en, fails, fail, correct, keytrans_es;
fails = 0;
fail = [];
correct = 0;
a = 0;
for (keytrans_en in trans_en) {
    correct = 0;
    for (keytrans_es in trans_es) {
        if (keytrans_es === keytrans_en) {
            correct = 1;
        }
    }
    if (correct === 0) {
        fails = fails + 1;
        fail[a] = keytrans_en;
        a = a + 1;
    } else {
        correct = 0;
    }
}
if (fails > 0) {
    console.log("Translate errors");
    console.log(fails);
    console.log(fail);
}
},{}]},{},["./src/js/themes/angular/app.js"]);
