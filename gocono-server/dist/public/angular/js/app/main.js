(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"./src/js/themes/angular/main.js":[function(require,module,exports){
// Angular App
require('./angular/app.js');
require('./angular/config.router.js');
require('./angular/main.js');

// Directives
require('essential/js/angular/main');
require('layout/js/angular/main');
require('sidebar/js/angular/main');

require('media/js/angular/main');

//Controllers
require("./controllers/search.controller");
require("./controllers/navbar.controller");

require("./controllers/user.controller");
require("./controllers/listing.controller");
require("./controllers/map.controller");
require("./controllers/services.controller");
require("./controllers/contacts.controller");
require("./controllers/viewOffersForYouCtrl.controller");
require("./controllers/viewAllOffersCtrl.controller");
require("./controllers/viewConversations.controller");

//Services
require("./services/main.service");
require("./services/geolocationSvc");
require("./translate/translate");

},{"./angular/app.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/app.js","./angular/config.router.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/config.router.js","./angular/main.js":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/main.js","./controllers/contacts.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/contacts.controller.js","./controllers/listing.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/listing.controller.js","./controllers/map.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/map.controller.js","./controllers/navbar.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/navbar.controller.js","./controllers/search.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/search.controller.js","./controllers/services.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/services.controller.js","./controllers/user.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/user.controller.js","./controllers/viewAllOffersCtrl.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewAllOffersCtrl.controller.js","./controllers/viewConversations.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewConversations.controller.js","./controllers/viewOffersForYouCtrl.controller":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewOffersForYouCtrl.controller.js","./services/geolocationSvc":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/geolocationSvc.js","./services/main.service":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/main.service.js","./translate/translate":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/translate/translate.js","essential/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/main.js","layout/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/main.js","media/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/main.js","sidebar/js/angular/main":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/main.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_carousel.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('carousel', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkCarousel();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_check-all.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'check-all') {
                        el.tkCheckAll();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_collapse.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'collapse') {
                        el.tkCollapse();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_cover.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('cover', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function () {
                        el.tkCover();
                    }, 200);
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_datepicker.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('datepicker', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkDatePicker();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_daterangepicker.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('daterangepickerReport', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkDaterangepickerReport();
                }
            };
        } ])
        .directive('daterangepickerReservation', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkDaterangepickerReservation();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_expandable.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('expandable', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkExpandable();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_minicolors.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('minicolors', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkMiniColors();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_modal.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'modal') {
                        el.tkModal();
                    }
                    if (attrs.toggle == 'tk-modal-demo') {
                        el.tkModalDemo();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_nestable.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('nestable', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkNestable();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_panel-collapse.js":[function(require,module,exports){
(function () {
    "use strict";

    var randomId = function () {
        /** @return String */
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };

    angular.module('app')
        .directive('toggle', [ '$compile', function ($compile) {
            return {
                restrict: 'A',
                priority: 100,
                compile: function (el, attrs) {

                    if (attrs.toggle !== 'panel-collapse') return;

                    var body = angular.element('.panel-body', el),
                        id = body.attr('id') || randomId(),
                        collapse = angular.element('<div/>');

                    collapse
                        .attr('id', id)
                        .addClass('collapse' + (el.data('open') ? ' in' : ''))
                        .append(body.clone());

                    body.remove();

                    el.append(collapse);

                    $('.panel-collapse-trigger', el)
                        .attr('data-toggle', 'collapse')
                        .attr('data-target', '#' + id)
                        .collapse({trigger: false})
                        .removeAttr('style');

                    return function (scope, el, attrs) {
                    };

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_select2.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'select2' || attrs.toggle == 'select2-tags') {
                        el.tkSelect2();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_selectpicker.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('selectpicker', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkSelectPicker();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_slider.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('slider', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.slider == 'default') {
                        el.tkSlider();
                    }

                    if (attrs.slider == 'formatter') {
                        el.tkSliderFormatter();
                    }

                }
            };
        } ]);

    angular.module('app')
        .directive('onSlide', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    el.tkSliderUpdate();

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_summernote.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('summernote', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkSummernote();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tables.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'data-table') {
                        el.tkDataTable();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tabs.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'tab') {
                        el.click(function(e){
                            e.preventDefault();
                        });
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_touchspin.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'touch-spin') {
                        el.tkTouchSpin();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tree.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (attrs.toggle == 'tree') {
                        el.tkFancyTree();
                    }

                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_wizard.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'wizard') {
                        el.tkWizard();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/main.js":[function(require,module,exports){
require('./_panel-collapse');
require('./_carousel');
require('./_check-all');
require('./_collapse');
require('./_cover');
require('./_datepicker');
require('./_daterangepicker');
require('./_expandable');
require('./_minicolors');
require('./_modal');
require('./_nestable');
require('./_select2');
require('./_selectpicker');
require('./_slider');
require('./_summernote');
require('./_touchspin');
require('./_tables');
require('./_tabs');
require('./_tree');
require('./_wizard');
},{"./_carousel":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_carousel.js","./_check-all":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_check-all.js","./_collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_collapse.js","./_cover":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_cover.js","./_datepicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_datepicker.js","./_daterangepicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_daterangepicker.js","./_expandable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_expandable.js","./_minicolors":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_minicolors.js","./_modal":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_modal.js","./_nestable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_nestable.js","./_panel-collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_panel-collapse.js","./_select2":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_select2.js","./_selectpicker":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_selectpicker.js","./_slider":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_slider.js","./_summernote":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_summernote.js","./_tables":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tables.js","./_tabs":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tabs.js","./_touchspin":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_touchspin.js","./_tree":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_tree.js","./_wizard":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/essential/js/angular/_wizard.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_gridalicious.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ '$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'gridalicious') {
                        $timeout(function(){
                            el.tkGridalicious();
                        }, 100);
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_isotope.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ '$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'isotope') {
                        $timeout(function(){
                            el.tkIsotope();
                        }, 100);
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_parallax.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('parallax', [ function () {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    el.tkParallax();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_scrollable.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('scrollable', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el) {
                    el.tkScrollable();
                }
            };
        } ])
        .directive('scrollableH', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el) {
                    el.tkScrollable({ horizontal: true });
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_sidebar-pc.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggle', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggle == 'sidebar-size-pc-demo') {
                        el.tkSidebarSizePcDemo();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/main.js":[function(require,module,exports){
require('./_scrollable');
require('./_isotope');
require('./_parallax');
require('./_gridalicious');
require('./_sidebar-pc');
},{"./_gridalicious":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_gridalicious.js","./_isotope":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_isotope.js","./_parallax":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_parallax.js","./_scrollable":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_scrollable.js","./_sidebar-pc":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/layout/js/angular/_sidebar-pc.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_owl.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('owlBasic', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkOwlDefault();
                    }, 200);
                }
            };
        } ])
        .directive('owlMixed', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkOwlMixed();
                    }, 200);
                }
            };
        } ])
        .directive('owlPreview', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkOwlPreview();
                    }, 200);
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_slick.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('slickBasic', [ '$timeout', function ($timeout) {
            return {
                restrict: 'C',
                link: function (scope, el) {
                    $timeout(function(){
                        el.tkSlickDefault();
                    }, 200);
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/main.js":[function(require,module,exports){
require('./_owl');
require('./_slick');
},{"./_owl":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_owl.js","./_slick":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/media/js/angular/_slick.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-collapse.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('type', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (! el.is('.sidebar')) return;
                    if (attrs.type !== 'collapse') return;

                    el.tkSidebarCollapse();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-dropdown.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('type', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {

                    if (! el.is('.sidebar')) return;
                    if (attrs.type !== 'dropdown') return;

                    el.tkSidebarDropdown();
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-toggle-bar.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .directive('toggleBar', [ function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (attrs.toggleBar) {
                        el.tkSidebarToggleBar();
                    }
                }
            };
        } ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/main.js":[function(require,module,exports){
require('./_sidebar-dropdown');
require('./_sidebar-collapse');
require('./_sidebar-toggle-bar');
},{"./_sidebar-collapse":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-collapse.js","./_sidebar-dropdown":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-dropdown.js","./_sidebar-toggle-bar":"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/lib/sidebar/js/angular/_sidebar-toggle-bar.js"}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/app.js":[function(require,module,exports){
(function(){
    'use strict';

    angular.module('app', [
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'uiGmapgoogle-maps',
        'ngFileUpload',
        'ui.bootstrap.datetimepicker',
        'pascalprecht.translate'
    ]);

    var app = angular.module('app')
        .config(
        [ '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
            function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
                app.controller = $controllerProvider.register;
                app.directive = $compileProvider.directive;
                app.filter = $filterProvider.register;
                app.factory = $provide.factory;
                app.service = $provide.service;
                app.constant = $provide.constant;
                app.value = $provide.value;
            }
        ])
        .config(
        ['uiGmapGoogleMapApiProvider', function(GoogleMapApiProviders) {
            GoogleMapApiProviders.configure({
                v: '2.3.3',
                libraries: 'weather,geometry,visualization',
                key: 'AIzaSyCz2DCKjoWMnlrf0V23RGidagkYrwTce58'
            });
            }
        ]);

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/config.router.js":[function(require,module,exports){
(function(){
    'use strict';

    angular.module('app')
        .run([ '$rootScope', '$state', '$stateParams',
            function ($rootScope, $state, $stateParams) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ])
        .config(
        [ '$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {

                $urlRouterProvider
                    .otherwise('/search');

                $stateProvider
                    //edited view
                    .state('discover', {
                        abstract: true,
                        url: '/discover',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('/search', {
                        url: '/search',
                        templateUrl: 'containers/search.view.html',
                        controller: 'SearchCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'containers/userLogin.view.html',
                        controller: 'LoginCtrl'
                    })
                    .state('register', {
                        url: '/register',
                        templateUrl: 'containers/userRegister.view.html',
                        controller: 'RegisterUserCtrl'
                    })
                    .state('discover.listing', {
                        url: '/listing',
                        templateUrl: 'containers/listing.view.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l-sum-13';
                        }]
                    })
                    .state('discover.map-listing-list', {
                        url: '/map-listing-list',
                        templateUrl: 'containers/search-results.view.html',
                        controller: 'MapCtrl'
                    })
                    .state('registerService', {
                        url: '/registerService',
                        templateUrl: 'containers/register-service.view.html',
                        controller: 'registerService'
                    })
                    .state('userProfile', {
                        url: '/userProfile',
                        templateUrl: 'containers/user-profile.view.html',
                        controller: 'UpdateUserCtrl'
                    })
                    .state('viewService', {
                        url: '/viewService',
                        templateUrl: 'containers/view-services-registered.view.html',
                        controller: 'viewServiceCtrl'
                    })
                    .state('viewContacts', {
                        url: '/viewContacts',
                        templateUrl: 'containers/view-contacts.view.html',
                        controller: 'viewContactsCtrl'
                    })
                    .state('viewOffersForYou', {
                        url: '/viewOffersForYou',
                        templateUrl: 'containers/view-offers-for-you.view.html',
                        controller: 'viewOffersForYouCtrl'
                    })
                    .state('viewAllOffers', {
                        url: '/viewAllOffers',
                        templateUrl: 'containers/view-all-offers.view.html',
                        controller: 'viewAllOffersCtrl'
                    })
                    .state('viewConversations', {
                        url: '/viewConversations',
                        templateUrl: 'containers/view-conversations.view.html',
                        controller: 'viewConversationsCtrl'
                    })
                    



                    //No edited yet
                    .state('discover.map-full', {
                        url: '/map-full',
                        templateUrl: 'discover/map-full.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs';
                        }]
                    })
                    .state('discover.map-listing-grid', {
                        url: '/map-listing-grid',
                        templateUrl: 'discover/map-listing-grid.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-48pc-lg sidebar-r-40pc';
                        }]
                    })
                    .state('discover.listing-grid', {
                        url: '/listing-grid',
                        templateUrl: 'discover/listing-grid.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l-sum-13';
                        }]
                    })
                    .state('discover.listing-map', {
                        url: '/listing-map',
                        templateUrl: 'discover/listing-map.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l-sum-13';
                        }]
                    });

                $stateProvider
                    .state('property', {
                        abstract: true,
                        url: '/property',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('property.map', {
                        url: '/map',
                        templateUrl: 'property/map.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-48pc-lg sidebar-r-40pc';
                        }]
                    })
                    .state('property.property', {
                        url: '/property',
                        templateUrl: 'property/property.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-25pc-lg sidebar-r-30pc';
                        }]
                    })
                    .state('property.edit', {
                        url: '/edit',
                        templateUrl: 'property/edit.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs sidebar-r-48pc-lg sidebar-r-40pc';
                        }]
                    });

                $stateProvider
                    .state('map-features', {
                        abstract: true,
                        url: '/map-features',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('map-features.themes', {
                        url: '/themes',
                        templateUrl: 'map-features/themes.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1 sidebar-r1-xs';
                        }]
                    })
                    .state('map-features.filters', {
                        url: '/filters',
                        templateUrl: 'map-features/filters.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l1';
                        }]
                    })
                    .state('map-features.markers', {
                        url: '/markers',
                        templateUrl: 'map-features/markers.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'st-layout ls-top-navbar ls-bottom-footer show-sidebar sidebar-l2';
                        }]
                    });

                $stateProvider
                    .state('front', {
                        abstract: true,
                        url: '/front',
                        template: '<div ui-view class="ui-view-main" />'
                    })
                    .state('front.home-map', {
                        url: '/home-map',
                        templateUrl: 'front/home-map.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.home-slider', {
                        url: '/home-slider',
                        templateUrl: 'front/home-slider.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.listing', {
                        url: '/listing',
                        templateUrl: 'front/listing.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.listing-grid', {
                        url: '/listing-grid',
                        templateUrl: 'front/listing-grid.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    })
                    .state('front.property', {
                        url: '/property',
                        templateUrl: 'front/property.html',
                        controller: ['$scope', function($scope){
                            $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                        }]
                    });
            }
        ]
    );

})();
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/angular/main.js":[function(require,module,exports){
(function () {
    "use strict";

    angular.module('app')
        .controller('AppCtrl', [ '$scope', '$state',
            function ($scope, $state) {

                $scope.app = {
                    settings: {
                        htmlClass: ''
                    }
                };

                $scope.$state = $state;

            } ]);

})();

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/contacts.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewContactsCtrl', function ($scope, $http, usersService, productsService, $location) {
		$scope.user = usersService.getUser();
	    $scope.$watch(function(){return usersService.getUser()}, function() {
	        $scope.user = usersService.getUser();
	        if ($scope.user) {
		    	$scope.init();
	        }
	    });
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
	    $scope.init = function () {
		    usersService.getUserContacts().then( function(response){
		    	if (response.err){
	              	$scope.user_error = response.err;
	            } else {
	          		$scope.services = response;
	          		$scope.servicesContacts = [];
	          		var entered = false;
	          		var noEnteredService = true;
			        for (var i = 0; i < $scope.services.length; i ++) {
			        	for (var item in $scope.services[i].contacts) {
			        		if ($scope.servicesContacts[0]) {
			        			noEnteredService = true;
					        	for (var j = 0; j < $scope.servicesContacts.length; j ++) {
					        		if ($scope.servicesContacts[j].name == $scope.services[i].contacts[item]) {
					        			noEnteredService = false;
					        			entered = false;
					        			for (var t = 0; t < $scope.servicesContacts[j].contacts.length; t++) {
					        				if ($scope.servicesContacts[j].contacts[t] == $scope.services[i].name) {
					        					entered = true;
					        				}
					        			}
					        			if (!entered) {
					        				$scope.servicesContacts[j].contacts.push($scope.services[i].name);
					        			}					        			
					        		}
					        	}
					        	if (noEnteredService) {
					        			$scope.servicesContacts.push({"name" :$scope.services[i].contacts[item], "contacts": [$scope.services[i].name]});
					        		}
					        } else {
					        	$scope.servicesContacts[0] = {"name" :$scope.services[i].contacts[item], "contacts": [$scope.services[i].name]};
					        }
				    	}
			        }
	          	}
	        });
	    }
	    $scope.init();
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/listing.controller.js":[function(require,module,exports){
angular.module('app')
    .controller('listingPanelCtrl', function ($scope, $http, usersService, productsService, $rootScope) {
        $scope.servicesFound = productsService.getProductList();
        $scope.range = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };
        $scope.selectService = function (service) {
            usersService.getOwner(service.owner).then( function () {
                $rootScope.$broadcast('serviceSelected', service);
            });
        }
        $scope.locateService = function(service) {
            $rootScope.$emit('locatedService', {
                location: service.coords,
                idService: service.id
            });
        }
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });
        
    })
    .controller('textMultipleCtrl', function ($scope, $http, usersService, productsService, $rootScope, conversationsService) {
        $scope.servicesFound = productsService.getProductList();
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });

        $scope.sendMessages = function(textMessageMultiple) {
            for (var i = 0; i < $scope.servicesFound.length; i++) {
                if ($('#' + $scope.servicesFound[i].id).is(':checked')) {
                    $scope.sendMessageUnic($scope.servicesFound[i].owner, textMessageMultiple);
                }
            }
        }

        $scope.sendMessageUnic = function(ownerId, textMessage) {
            var conversation = {
                user1: $scope.user._id,
                user2: ownerId,
                mensaje: '' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + ''
            };
            conversationsService.createConversations(conversation).then ( function() {
                if (conversationsService.getConversationUpdated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
    })
    .controller('serviceDetailsListCtrl', function ($scope, $http, usersService, productsService, $rootScope, conversationsService) {
        $scope.selectedService = $rootScope.selectedService;
        $scope.mensajeEnviado = false;
        $scope.owner = usersService.takeOwner();
        $scope.$watch(function(){return usersService.takeOwner()}, function() {
            $scope.owner = usersService.takeOwner();
            if ($scope.owner) {
                $scope.noEsContacto = true;
                if ($scope.user && $scope.user.contactos) {
                    var contactos= $scope.user.contactos.split(",");
                    for (var i = 0; i < contactos.length; i++) {
                        if (contactos[i] == $scope.owner._id) {
                            $scope.noEsContacto = false;
                        }
                    }
                }
            }
        });
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });
        if ($scope.owner) {
            $scope.noEsContacto = true;
            if ($scope.user && $scope.user.contactos) {
                var contactos= $scope.user.contactos.split(",");
                for (var i = 0; i < contactos.length; i++) {
                    if (contactos[i] == $scope.owner._id) {
                        $scope.noEsContacto = false;
                    }
                }
            }
        };
        $scope.addContacts = function () {
            var ownerId = $scope.owner._id;
            usersService.addContact(ownerId);

        }
        $scope.dismissServiceDetails = function () {
            $scope.selectedService = undefined;
            $rootScope.selectedService = undefined;
        }

        $scope.sendMessage = function(textMessage) {
            var conversation = {
                user1: $scope.user._id,
                user2: $scope.owner._id,
                mensaje: '' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + ''
            };
            conversationsService.createConversations(conversation).then ( function() {
                if (conversationsService.getConversationUpdated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
    })
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/map.controller.js":[function(require,module,exports){
angular.module('app')
    .controller('MapCtrl', function($scope, productsService, usersService, $rootScope) {

        $scope.map = {center: {latitude: 51.219053, longitude: 4.404418 }, zoom: 15 };
        $scope.options = {scrollwheel: false};

        //$scope.searchResults = JSON.parse(localStorage.getItem('results'));

        $scope.searchResults = productsService.getProductList();
        $scope.markers = [];

        _.each($scope.searchResults, function (result, index) {
            $scope.marker = {
                id: index,
                coords: {
                    latitude: result.coords.latitude,
                    longitude: result.coords.longitude
                },
                owner: result.owner,
                images: result.images,
                idService: result.id,
                name: result.name,
                icon: {url: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'},
                events: {
                    click: function (marker, eventName, args) {
                        usersService.getOwner(marker.model.owner).then( function () {
                            $rootScope.selectedService = marker.model;
                        });
                    }
                }
            };
            $scope.markers.push($scope.marker);
        });

        $scope.searchOptions = productsService.getSearchOptions();

        if($scope.searchOptions && $scope.searchOptions.coordinates){
            $scope.map.center = $scope.searchOptions.coordinates;
        }

        $scope.dismissServiceDetails = function () {
            $scope.selectedService = null;
        };

        $rootScope.$on('serviceSelected', function (event, service) {
            usersService.getOwner(service.owner);
            $rootScope.selectedService = service;
        });

        $rootScope.$on('locatedService', function(event, props){
            for (var i = 0; i < $scope.markers.length; i++) {
                if (props.idService == $scope.markers[i].idService && props.location.latitude == $scope.markers[i].coords.latitude && props.location.longitude == $scope.markers[i].coords.longitude) {
                    $scope.markers[i].icon = {url: 'http://labs.google.com/ridefinder/images/mm_20_yellow.png'};
                    $scope.map.center = props.location;
                } else {
                    $scope.markers[i].icon = {url: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'};
                }
            }
        });

    });
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/navbar.controller.js":[function(require,module,exports){
angular.module('app')
    .controller('navbarCtrl', function ($scope, $http, usersService, productsService,$location) {  
	    $scope.user = usersService.getUser();
	    $scope.$watch(function(){return usersService.getUser()}, function() {
	        $scope.user = usersService.getUser();
	    });
		$scope.loginUser = function(email, pass){
	      	usersService.loginUser(email,pass).then( function(response){
	          	if (response.err){
	            	$scope.login_error = response.err;
	          	}else{
		            $scope.login_error = null;
		            usersService.setUser(response.data.data);
	          	}
	        },
	        function(status){
	          	console.log(status);
	        });
	    };
	    $scope.logoutUser = function(){
			usersService.setUser(null);
			usersService.setToken("");
			localStorage.setItem('token',null);
            localStorage.setItem('user',null);
	    };
	    $scope.registerUser = function(user_data, pass2){
	      	if (user_data.password == pass2) {
				usersService.registerUser(user_data).then( function(response){
		          	if (response.err){
		          		console.log("error")
		          		console.log(response)
		            	$scope.register_error = response.err;
		          	}else{
			            usersService.setUser(response.data.data);
			            $scope.register_error = null;
			            $scope.user_data = {};
		          	}
		        },
		        function(status){
		          console.log(status);
		        });
	      	} else {
	      		$scope.register_error = "Las contraseñas no coinciden";
	      	}
		};
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/search.controller.js":[function(require,module,exports){

angular.module('app')
    .controller('SearchCtrl', function ($scope, $http, productsService, usersService, uiGmapGoogleMapApi, $location, $timeout) {
        $scope.products = null;
        $scope.distance = null;
        $scope.product_filter = false;
        $scope.busqueda = '';
        $scope.notProduct = false;
        if (usersService.getUser()) {
            $scope.userNoLoged = false;
        } else {
            $scope.userNoLoged = true;
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.userNoLoged = false;
            } else {
                $scope.userNoLoged = true;
            }
        });
        $scope.filterProduct = function () {
            $scope.product_filter = true;
        };
        $scope.hideFilterProduct = function () {
            $scope.product_filter = false;
        };

        $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.userLocation = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.userLocation = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };
        $scope.filterOptions = {};
        $scope.filterOptions.startsNumber = 0;
        $scope.startsSelected = function (value) {
            $scope.filterOptions.startsNumber = value;
        };
        var getProductsSearch = function () {
            var coordinates = {latitude: $scope.position.latitude, longitude: $scope.position.longitude};
            $scope.filterOptions.coordinates = coordinates;
            if (!$scope.filterOptions.distance) {
                $scope.filterOptions.distance = 90;
            }
            productsService.getProducts($scope.filterOptions).then(function(data) {
                $location.path('/discover/map-listing-list');
            });

        };
        $scope.searchProduct = function () {
            if($scope.filterOptions.product) {
               if (!$scope.position) {
                    $scope.location();
                    $timeout(function () {
                        getProductsSearch();
                    }, 2000);
                } else {
                    getProductsSearch();
                } 
            } else {
                $scope.notProduct = true;
            }
            
        };
        $scope.loginUser = function () {
            $location.path('/login');
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('searchTextField');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
            });
        };

        $scope.beforeRenderStartDate = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.filterOptions.endDate) {
                var activeDate = moment($scope.filterOptions.endDate);
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() >= activeDate.valueOf()) $dates[i].selectable = false;
                }
            }
        };

        $scope.beforeRenderEndDate = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.filterOptions.startDate) {
                var activeDate = moment($scope.filterOptions.startDate).subtract(1, $view).add(1, 'minute');
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() <= activeDate.valueOf()) {
                        $dates[i].selectable = false;
                    }
                }
            }
        };

    })
;

},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/services.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('registerService', function ($scope, $location, Upload, $timeout, usersService, productsService) {
        $scope.service_data = {};
        if (usersService.getUser()) {
            $scope.service_data.owner = usersService.getUser()._id;
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.service_data.owner = usersService.getUser()._id;
            }
        });
        $scope.service_data.images = [];
        $scope.namesFiles = [];
        $scope.uploadFiles = function (files) {
            if (files[0]){
                var nameFile = "upload/tmp/" + files[0].name;
                $scope.namesFiles.push(files[0].name);
                $scope.service_data.images.push(nameFile);
                $scope.files = files;
                if (files && files.length) {
                    Upload.upload({
                        url: '/upload',
                        data: {
                            files: files
                        }
                    }).then(function (response) {
                        $timeout(function () {
                            $scope.result = response.data;
                        });
                    }, function (response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    }, function (evt) {
                        $scope.progress =
                        Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
            }
        };
        $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.service_data.location = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.service_data.location = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };
        $scope.beforeRenderdayHourFrom = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.service_data.dayHourTo) {
                var activeDate = moment($scope.service_data.dayHourTo);
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() >= activeDate.valueOf()) $dates[i].selectable = false;
                }
            }
        };

        $scope.beforeRenderdayHourTo = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.service_data.dayHourFrom) {
                var activeDate = moment($scope.service_data.dayHourFrom).subtract(1, $view).add(1, 'minute');
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() <= activeDate.valueOf()) {
                        $dates[i].selectable = false;
                    }
                }
            }
        };
        $scope.sendEntrar = function(){
          $location.path('/login');
        };  
        $scope.registerService = function(service_data) {
            if (service_data.name) {
                if (service_data.location) {
                    if (service_data.category) {
                        if (service_data.description) {
                            productsService.createProduct(service_data).then(
                                function(response){
                                  if (response.err){
                                    $scope.serviceRegisterError = response.err;
                                  }else{
                                    $scope.correctRegister = "Servicio registrado correctamente";
                                  }
                                },
                                function(status){
                                  console.log(status);
                                }
                            )
                        } else {
                            $scope.serviceRegisterError = "Introduzcala descripción del servicio";
                        }
                    } else {
                        $scope.serviceRegisterError = "Seleccione la categoría del servicio";
                    }
                } else {
                    $scope.serviceRegisterError = "Introduzca la ubicación del servicio";
                }
            } else {
                $scope.serviceRegisterError = "Introduzca el nombre del servicio";
            }
        }
        
    })
    .controller('viewServiceCtrl', function ($scope, $location, usersService, productsService, $http) {
        $scope.service_data = {};
        $scope.beforeService = false;
        $scope.nextService = false;
        $scope.dataNumber = 0;

        if (usersService.getUser()) {
            if (usersService.getUser()) {
                $scope.owner = usersService.getUser()._id;
            } else {
                $scope.owner = undefined;
            }
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.owner = usersService.getUser()._id;
            } else {
                $scope.owner = undefined;
            }
        });
        $scope.init = function () {
            productsService.getProductByOwner($scope.owner).then(function(data) {
                $scope.data = data;
                $scope.service_data = data[0];
                $scope.getImages();
                if ($scope.service_data) {
                    $scope.getPlace($scope.service_data.location[0],$scope.service_data.location[1]);
                    if ($scope.service_data.active) {
                        $scope.service_data.active = "Si";
                    } else {
                        $scope.service_data.active = "No";
                    }
                    if (data[1]) {
                        $scope.nextService = true;
                    }
                }
            });
        }

        $scope.getImages = function () {
            if ($scope.service_data.images) {
                $scope.namesFiles = $scope.service_data.images.split(',');
                for (var i = 0; i < $scope.namesFiles.length; i++) {
                    $scope.namesFiles[i] = $scope.namesFiles[i].substring(11,$scope.namesFiles[i].length);
                }
            }
        }
        $scope.getPlace = function (lat,long) {
            $http({
                method: "GET",
                url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true',
                dataType: "json"
            })
            .then(function (place) {
                $scope.locationPosition = place.data.results[0].formatted_address;
            });
            
        }
        $scope.changeService = function (action) {
            if (action == "next") {
                $scope.dataNumber = $scope.dataNumber + 1;
            } else {
                $scope.dataNumber = $scope.dataNumber - 1;
            }
            $scope.service_data = $scope.data[$scope.dataNumber];
            $scope.getImages();
            $scope.getPlace($scope.service_data.location[0],$scope.service_data.location[1]);
            if ($scope.service_data.active) {
                $scope.service_data.active = "Si";
            } else {
                $scope.service_data.active = "No";
            }
            $scope.beforeService = false;
            $scope.nextService = false;
            if ($scope.data[$scope.dataNumber + 1]) {
                $scope.nextService = true;
            }
            if ($scope.data[$scope.dataNumber - 1]) {
                $scope.beforeService = true;
            }
        }
        $scope.init();
        $scope.sendEntrar = function(){
          $location.path('/login');
        };
        
    })
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/user.controller.js":[function(require,module,exports){
'use strict';


/* global angular, document, window */

angular.module('app')
  .controller('LoginCtrl', function($scope, $timeout, $stateParams, usersService,$location) {
    $scope.user_data = {};
    $scope.user_data.email = null;
    $scope.user_data.pass = null;
    $scope.user = usersService.getUser();
    $scope.$watch(function(){return usersService.getUser()}, function() {
        $scope.user = usersService.getUser();
    });
    $scope.user_error = null;
    $scope.loginUser = function(user_data){

      usersService.loginUser(user_data.email,user_data.pass).then(
        function(response){
          if (response.err){
            $scope.user_error = response.err;
          }else{
            $scope.user = response.data;
            usersService.setUser(response.data.data);
            $location.path('/');
          }
        },
        function(status){
          console.log(status);
        }
      );

    };
    $scope.sendRegister = function(){
      $location.path('/register');
    };
  })

  .controller('RegisterUserCtrl', function($scope, $timeout, Upload,$stateParams, usersService,$location) {

    $scope.email = null;
    $scope.pass = null;
    $scope.user = null;
    $scope.user_error = null;
    $scope.user_data = {};
    $scope.registerUser = function(user_data) {
      if(user_data.password2 != user_data.password) {
        $scope.user_error = "Las contraseñas no coinciden";
      } else {
        var data = {
          name : user_data.name,
          surname : user_data.surname,
          password : user_data.password,
          email : user_data.email,
          nick : user_data.nick,
          telefono : user_data.telefono,
          web : user_data.web,
          fax : user_data.fax,
          facebook : user_data.facebook,
          linkedin : user_data.linkedin,
          video : user_data.video,
          avatar : $scope.user_data.avatar,
          idpropio : user_data.idpropio,
          dateIdPropio: user_data.dateIdPropio
        };
        usersService.registerUser(data).then(
          function(response){
            if (response.err){
              $scope.user_error = response.err;
            }else{
              $scope.user = response.data;
              $scope.user_error = null;
              $location.path('/login');
            }
          },
          function(status){
            console.log(status);
          }
        );
      }
    };

    $scope.uploadFiles = function (files) {
      $scope.user_data.avatar = "upload/tmp/" + files[0].name;
      $scope.files = files;
      if (files && files.length) {
        Upload.upload({
          url: '/upload',
          data: {
            files: files
          }
        }).then(function (response) {
          $timeout(function () {
            $scope.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    };

    $scope.sendEntrar = function(){
      $location.path('/login');
    };
  })


.controller('UpdateUserCtrl', function($scope, $timeout, Upload,$stateParams, usersService,$location) {

    $scope.user = usersService.getUser();
    $scope.$watch(function(){return usersService.getUser()}, function() {
      $scope.user = usersService.getUser();
      for (var key in $scope.user) {
        $scope.user_data[key] = $scope.user[key];
      }
      $scope.user_data.password = undefined;
    });
    $scope.user_error = null;
    $scope.user_data = {};
    for (var key in $scope.user) {
      $scope.user_data[key] = $scope.user[key];
    }
    $scope.user_data.password = undefined;
    $scope.updateUser = function(user_data) {
      if(user_data.password2 != user_data.password) {
        $scope.user_error = "Las contraseñas no coinciden";
      } else {
        var data = {
          name : user_data.name,
          surname : user_data.surname,
          password : user_data.password,
          email : user_data.email,
          nick : user_data.nick,
          telefono : user_data.telefono,
          web : user_data.web,
          fax : user_data.fax,
          facebook : user_data.facebook,
          linkedin : user_data.linkedin,
          video : user_data.video,
          avatar : $scope.user_data.avatar,
          idpropio : user_data.idpropio,
          dateIdPropio: user_data.dateIdPropio,
          _id: user_data._id
        };
        usersService.updateUser(data).then(
          function(response){
            if (response.err){
              $scope.user_error = response.err;
            }else{
              $scope.user = response.data;
              $scope.user_error = null;
              $location.path('/search');
            }
          },
          function(status){
            console.log(status);
          }
        );
      }
    };

    $scope.uploadFiles = function (files) {
      $scope.user_data.avatar = "upload/tmp/" + files[0].name;
      $scope.files = files;
      if (files && files.length) {
        Upload.upload({
          url: '/upload',
          data: {
            files: files
          }
        }).then(function (response) {
          $timeout(function () {
            $scope.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    };

    $scope.sendEntrar = function(){
      $location.path('/login');
    };
  });
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewAllOffersCtrl.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewAllOffersCtrl', function ($scope, $http, usersService, productsService, $location) {
        $scope.firstTimeEntered = true;
	    $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.locationUser = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.locationUser = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };

        $scope.acept = function() {
    		if ($scope.locationUser) {
                var dataSent = {
                    offers: true, 
                    product: '', 
                    coordinates: {latitude: $scope.locationUser[0],longitude: $scope.locationUser[1]},
                    distance: 90
                };
                productsService.getProducts(dataSent).then(function(data) {
                    $scope.firstTimeEntered = false;
                    $scope.servicesFound = productsService.getProductList();
                });
			    $scope.serviceRegisterError = "";
        	} else {
        		$scope.serviceRegisterError = "Introduzca la ubicación en la que desea encontrarlos";
        	}
        }
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewConversations.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewConversationsCtrl', function ($scope, $http, usersService, productsService, $location, conversationsService) {
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
            $scope.showConversacion = false;
	        conversationsService.getConversations().then( function () {
	        	$scope.conversaciones = conversationsService.getMensajes();
	        });
        });
        $scope.showConversacion = false;
        conversationsService.getConversations().then( function () {
        	$scope.conversaciones = conversationsService.getMensajes();
        });

        $scope.showConversationSelected = function (id) {
        	for (var i = 0; i < $scope.conversaciones.length; i++) {
        		if (id == $scope.conversaciones[i]._id) {
        			$scope.mensaejeOriginal = $scope.conversaciones[i].mensaejeOriginal;
        			$scope.mensaejeId = $scope.conversaciones[i]._id;
        			$scope.user2 = $scope.conversaciones[i].user.$$state.value.data;
        			$scope.showConversacion = true;
        			$scope.mensajes = $scope.conversaciones[i].mensaje;
        		}
        	}
        };

        $scope.sendMessage = function(textMessage) {
        	$scope.mensajes.push({text: textMessage, owner: $scope.user._id});
        	$scope.mensaejeOriginal = $scope.mensaejeOriginal + '2-2,IDCAMBIO,2-2' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + '';
        	$scope.conversationToSend = {
                id: $scope.mensaejeId,
                mensaje: $scope.mensaejeOriginal
            };
            console.log($scope.conversationToSend)
            conversationsService.updateConversations($scope.conversationToSend).then ( function() {
                if (conversationsService.getConversationCreated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/controllers/viewOffersForYouCtrl.controller.js":[function(require,module,exports){
angular.module('app')
	.controller('viewOffersForYouCtrl', function ($scope, $http, usersService, productsService, $location) {
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
        $scope.getServices = function () {
        	var dataSent = {
        		offers: true, 
        		product: $scope.user.firstTime, 
        		coordinates: {latitude: $scope.user.location[0],longitude: $scope.user.location[1]},
        		distance: 90
        	};
	        productsService.getProducts(dataSent).then(function(data) {
	            $scope.servicesFound = productsService.getProductList();
	        });
	    };
	    $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.locationUser = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.locationUser = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };
	    if (usersService.getUser()) {
            if (usersService.getUser()) {
            	$scope.user = usersService.getUser();
                $scope.owner = $scope.user._id;
                if ($scope.user.firstTime){
                	$scope.firstTimeEntered = false;
                	$scope.getServices();
				} else {
					$scope.firstTimeEntered = true;
				}
            } else {
                $scope.owner = undefined;
                $scope.firstTimeEntered = true;
            }
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.user = usersService.getUser();
                $scope.owner = $scope.user._id;
				if ($scope.user.firstTime){
                	$scope.firstTimeEntered = false;
                	$scope.getServices();
				} else {
					$scope.firstTimeEntered = true;
				}
            } else {
                $scope.owner = undefined;
        		$scope.firstTimeEntered = true;
            }
        });

        $scope.acept = function(firstTime) {
        	if (firstTime) {
        		if ($scope.locationUser) {
					var data = {
						firstTime : firstTime,
						location: $scope.locationUser
				    };
				    usersService.updateUser(data).then(
				        function(response){
				            if (response.err){
				            }else{
				              $scope.user = response.data;
				              $scope.firstTimeEntered = false;
				              $scope.getServices();
				            }
				        },
				        function(status){
				            console.log(status);
				        }
				    );
				    $scope.serviceRegisterError = "";
	        	} else {
	        		$scope.serviceRegisterError = "Introduzca la ubicación en la que desea encontrarlos";
	        	}
        	} else {
        		$scope.serviceRegisterError = "Introduzca las categorias o productos que desea encontrar";
        	}
        }
	})
;
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/geolocationSvc.js":[function(require,module,exports){
angular.module('app').factory('geolocationSvc', ['$q', '$window', function ($q, $window) {

    'use strict';

    function getCurrentPosition() {
        var deferred = $q.defer();

        if (!$window.navigator.geolocation) {
            deferred.reject('Geolocation not supported.');
        } else {
            $window.navigator.geolocation.getCurrentPosition(
                function (position) {
                    deferred.resolve(position);
                },
                function (err) {
                    deferred.reject(err);
                });
        }

        return deferred.promise;
    }

    return {
        getCurrentPosition: getCurrentPosition
    };
}]);
},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/services/main.service.js":[function(require,module,exports){
angular.module('app')    
    .factory('conversationsService', function ($http, usersService) {
        var conversationCreated = null,
            mensajesTodos = [],
            conversationUpdated = null;
        var myService = {
            /**
             * @method getProducts : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            createConversations: function (conversation) {
                if (usersService.getToken()) {
                    return $http({
                        method: "POST",
                        url: '/api/conversations',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: conversation
                    })
                        .then(function (data) {
                                console.log(data)
                                conversationCreated = true;
                            },
                            function (error) {
                                alert(error.data)
                            });
                }
            },
            updateConversations: function (conversationToSend) {
                if (usersService.getToken()) {
                    return $http({
                        method: "PUT",
                        url: '/api/conversations',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: conversationToSend
                    })
                        .then(function (data) {
                                console.log(data)
                                conversationUpdated = true;
                            },
                            function (error) {
                                alert(error.data)
                            });
                }
            },
            getConversationCreated: function () {
                return conversationCreated;
            },
            getConversationUpdated: function () {
                return conversationUpdated;
            },
            getConversations: function () {
                return $http({
                    method: "GET",
                    url: '/api/conversations',
                    headers: {
                            'x-access-token': usersService.getToken()
                        },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json"
                })
                .then(function (mensajes) {
                    for (var i = 0; i < mensajes.data.length; i ++) {
                        mensajesTodos[i] = {};
                        console.log(mensajes.data[i]._id)
                        mensajesTodos[i]._id = mensajes.data[i]._id;
                        mensajesTodos[i].mensaje = [];
                        mensajesTodos[i].mensaejeOriginal = mensajes.data[i].mensaje;
                        var a = mensajes.data[i].mensaje.split('2-2,IDCAMBIO,2-2');
                        if (a[1]) {
                            for (var j = 0; j < a.length; j++) {
                                var b = a[j].split('-,IDCAMBIO,-');
                                mensajesTodos[i].mensaje[j] = {text: b[0], owner: b[1]};
                            }
                        } else {
                            var b = a[0].split('-,IDCAMBIO,-');
                            console.log(b)
                            mensajesTodos[i].mensaje[0] = {text: b[0], owner: b[1]};
                        }
                        if (usersService.getUser()._id == mensajes.data[i].user1) {
                            mensajesTodos[i].user = usersService.getOwner(mensajes.data[i].user2);
                        } else {
                            mensajesTodos[i].user = usersService.getOwner(mensajes.data[i].user1);
                        }
                    }
                    usersService.getOwner(usersService.getUser()._id).then ( function () {
                        return {err: null, data: mensajesTodos};
                    })
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;
                        return {err: my_err, data: mensajesTodos};
                    }
                })
            },
            getMensajes: function () {
                return mensajesTodos;
            }
        };
        return myService;
    })
    .factory('productsService', function ($http, usersService) {
        var product_list = [],
            best_product_list = [],
            anuncio = null,
            search_options;
        var myService = {
            /**
             * @method getProducts : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            getProducts: function (options) {

                search_options = options;

                if (options.endDate) {
                    options.endDate = moment(options.endDate).format();
                }
                if (options.startDate) {
                    options.startDate = moment(options.startDate).format();
                }

                var parameters = {
                    lat: options.coordinates.latitude,
                    long: options.coordinates.longitude,
                    distance: options.distance,
                    product: options.product,
                    price: options.price,
                    offers: options.offers,
                    startsNumber: options.startsNumber,
                    contactsOnly: options.contactsOnly,
                    dayHourFrom: options.startDate,
                    dayHourTo: options.endDate,
                    discount: options.discount
                };
                if (options.contactsOnly) {
                    parameters.email = usersService.getUser().email;
                }
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: parameters
                    })
                        .then(function (productsMarkers) {
                                product_list = prettyMarkers(productsMarkers.data);
                                return ({
                                    searchOptions: options,
                                    searchResults: product_list
                                });
                            },
                            function (error) {
                                alert(error.data)
                            });
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: parameters
                    })
                        .then(function (productsMarkers) {
                                product_list = prettyMarkers(productsMarkers.data);
                                return ({
                                    searchOptions: options,
                                    searchResults: product_list
                                });
                            },
                            function (error) {
                                alert(error.data)
                            });
                }

                //return promise;
            },

            /**
             * @method getProductInfo : service to obtain the data for a product in $scope
             * @params : id (Object) product id
             * @return : Object
             **/
            getProductInfo: function (product_id) {
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            id: product_id
                        }
                    })
                        .then(function (product) {
                            return product.data;
                        });
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            id: product_id
                        }
                    })
                        .then(function (product) {
                            return product.data;
                        });
                }

            },
            setProductList: function (products) {
                product_list = products;
            },
            getProductList: function () {
                return product_list;
            },
            setBestProductList: function (products) {
                best_product_list = products;
            },
            getBestProductList: function () {
                return best_product_list;
            },
            getSearchOptions: function () {
                return search_options;
            },
            setAnuncio: function (value) {
                anuncio = value;
            },
            getAnuncio: function () {
                return anuncio;
            },
            createProduct: function (data) {
                if (usersService.getToken()) {
                    return $http({
                        method: "POST",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: data
                    })
                        .then(function (product) {
                            return product.data;
                        });
                } else {
                    return $http({
                        method: "POST",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: data
                    })
                        .then(function (product) {
                            return product.data;
                        });
                }
            },
            // updateProduct: function (data) {
            //     console.log("updated")
            // },
            getProductByOwner: function (owner) {
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {owner: owner}
                    })
                    .then(function (products) {
                            return products.data;
                        },
                        function (error) {
                            alert(error.data)
                        });
                }
            }
        };
        return myService;
    })

    .factory('usersService', function ($http) {
        var my_err = null,
            user = null,
            ownerProduct = null,
            my_data;
        var token = "";
        if (window.localStorage.getItem('token') != undefined && window.localStorage.getItem('token') != null ) {
            token = window.localStorage.getItem('token');
            user = JSON.parse(window.localStorage.getItem('user'));
        }

        var myUserService = {
            /**
             * @method getMarkers : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            getToken: function () {
                return token;
            },
            setToken: function (value) {
                token = value;
            },
            loginUser: function (email, pass) {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            email: email,
                            pass: pass
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            email: email,
                            pass: pass
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                }
            },
            /**
             * @method registerUser : register an user
             * @params : user (Object) user data for register
             * @return : Object
             **/
            registerUser: function (user) {
                delete user.password2;
                if (token) {
                    return $http({
                        method: "POST",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            user: user
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            } else {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                } else {
                    return $http({
                        method: "POST",
                        url: '/api/users',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            user: user
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            } else {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                }
            },
            updateUser: function (userdata) {
                if(userdata) {
                    if (userdata.password2) {
                        delete userdata.password2;
                    }
                }
                return $http({
                    method: "PUT",
                    url: '/api/users',
                    headers: {
                        'x-access-token': token
                    },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    params: {
                        user: userdata
                    }
                })
                .then(function (userInfo) {
                    user = {};
                    console.log(user)
                    if (token) {
                        return $http({
                            method: "GET",
                            url: '/api/users',
                            headers: {
                                'x-access-token': token
                            },
                            withCredentials: true,
                            contentType: "application/x-www-form-urlencoded",
                            dataType: "json"
                        })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            user = userInfo.data
                            localStorage.setItem('user', JSON.stringify(userInfo.data));
                            console.log(user)
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                    }
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    } else {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    }
                })
            },
            getUserContacts: function (userdata) {
                return $http({
                    method: "GET",
                    url: '/api/users/contacts',
                    headers: {
                        'x-access-token': token
                    },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json"
                })
                .then(function (userInfo) {
                    return userInfo.data
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    } else {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    }
                })
            },
            /**
             * @method getUsertInfo : service to obtain the data for an user in $scope
             * @params : id (Object) user email
             * @return : Object
             **/
            getUsertInfo: function (user_email) {
                //TODO
            },
            setUser: function (new_user) {
                user = new_user;
            },
            getUser: function () {
                return user;
            },
            getOwner: function (ownerId) {
                return $http({
                    method: "GET",
                    url: '/api/users/photo',
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    params: {
                        id: ownerId
                    }
                })
                .then(function (userInfo) {
                    ownerProduct = userInfo.data
                    my_data = userInfo.data;
                    return {err: null, data: my_data};
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;
                        return {err: my_err, data: my_data};
                    }
                })
            },
            
            takeOwner: function () {
                return ownerProduct;
            },
            getUserUpdated: function() {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json"
                    })
                    .then(function (userInfo) {
                        my_data = userInfo.data;
                        token = userInfo.data.token;
                        localStorage.setItem('token', token);
                        localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                        return {err: null, data: my_data};
                        //return productsMarkers.data;
                    }).catch(function (err) {
                        // recover here if err is 404
                        if (err.status === 404) {
                            my_err = err.data;

                            return {err: my_err, data: my_data};
                        }
                    })
                }
            },
            addContact: function (ownerId) {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users/addContact',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            ownerId: ownerId
                        }
                    })
                    .then(function (userInfo) {
                        user = userInfo.data;
                        localStorage.setItem('user', JSON.stringify(userInfo.data));
                        return {err: null, data: my_data};
                        //return productsMarkers.data;
                    }).catch(function (err) {
                        // recover here if err is 404
                        if (err.status === 404) {
                            my_err = err.data;

                            return {err: my_err, data: my_data};
                        }
                    })
                }
            }
        };
        return myUserService;
    })
;


/**
 * @method prettyMarkers : create a correct mark for the map with the services data
 * @params : marker (array) is an array of objects
 * @return : array
 **/
function prettyMarkers(markers) {

    var marker = {
        id: null,
        name: null,
        towards: null,
        icon: null,
        coords: {},
        options: {}
    };
    var return_markers = [],
        return_best = [];

    angular.forEach(markers, function (mark, key) {
        marker = {
            id: mark._id,
            name: mark.name,
            owner: mark.owner,
            price: mark.price,
            distance: mark.distance,
            images: mark.images,
            productType: mark.productType,
            icon: 'http://google-maps-icons.googlecode.com/files/bookstore.png',
            coords: {
                latitude: mark.location[0],
                longitude: mark.location[1]
            }
        };
        return_markers.push(marker);
    });
    return return_markers;
}


/*
 *@method arrayObjectIndexOf: Helper Function to serach indexOf
 * @params myArray (Array), the array to search
 * @params searchTerm (String), string to search into the array
 * @params property (String), the index value
 *
 */
function arrayObjectIndexOf(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) {
            return i;
        }
    }
    return -1;
}

/*
 *@method removeArrayObject: Helper Function to remove an object from the array
 * @params myArray (Array), the array to search
 * @params searchTerm (String), string with the id to remove
 * @params property (String), the index value
 *
 */
function removeArrayObject(myArray, searchTerm, property) {
    var array_modify = [];
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] !== searchTerm) {
            array_modify.push(myArray[i]);
        }
    }
    return array_modify;
}



},{}],"/home/polepo/Escritorio/gocono-dev/gocono-server/dist/public/src/js/themes/angular/translate/translate.js":[function(require,module,exports){
'use strict';
var trans_en = {};
var trans_es = {};
angular.module('app').config(function ($translateProvider) {
    $translateProvider.translations('en', trans_en)
        .translations('es', trans_es)
        .registerAvailableLanguageKeys(['en', 'es'], {
            'en_US': 'en',
            'en_UK': 'en'
        })
        .determinePreferredLanguage()
        .fallbackLanguage('en', 'es');
    $translateProvider.useSanitizeValueStrategy('escaped');
});


trans_en = {
    'searchLabel' : 'Search'
};
trans_es = {
    'searchLabel' : 'Buscar',
    'contactsLoged': 'PARA VER SUS CONTACTOS DEBE ESTAR LOGEADO',
    'entrar' : 'Entrar',
    'userNoContacts' : "No tienes ningun contacto añadido",
    'Gocono' : "Gocono",
    'error!' : 'Error!',
    'registro': 'Registro',
    'nombre' : 'Nombre',
    'apellidos' : 'Apellidos',
    'nick' : 'Nick',
    'email' : 'Email',
    'contraseña' : 'Contraseña',
    'repetirContraseña' : 'Repetir contraseña',
    'aceptar' : 'Aceptar',
    'editarPerfil': 'Editar perfil',
    'salir' : 'Salir',
    'precio': 'Precio',
    'Día/Hora' : 'Día/Hora',
    'lanzarComunicacion' : 'Lanzar comunicacion',
    'descripcion' : 'Descripcion',
    'añadirContactos' : 'Añadir a contactos',
    'registrarNuevoServicio' : 'Registrar nuevo servicio',
    'paraRegistrarUnServicioDebeIniciarSesión' : 'Para registrar un servicio debe iniciar sesión',
    'localizacion' : 'Localización',
    'categoria' : 'Categoria',
    'activo' : 'Activo',
    'si' : 'Si',
    'no' : 'No',
    'tipoProducto' : 'Tipo de producto',
    'descuento' : 'Descuento',
    'duracionDescuento' : 'Duración descuento',
    'videoExplicativo' : 'Video explicativo',
    'elegirDía/horaServicioDesde:' : 'Elegir día/hora servicio Desde:',
    'elegirDía/horaServicioHasta:' : 'Elegir día/hora servicio Hasta:',
    'ofertas' : 'Ofertas',
    'registrarServicio' : 'Registrar servicio',
    'verTodasOfertas' : 'Ver todas las ofertas',
    'siguienteServicio' : 'Siguiente servicio',
    'anteriorServicio' : 'Anterior servicio',
    'paraSecciónLogeado' : 'Para ver esta sección debe estar logeado',
    'serviciosPreferidos' : 'Servicios preferidos',
    'verOfertasTi' : 'Ver ofertas para tí',
    'registrarse' : 'Registrarse',
    'telefono' : 'Teléfono',
    'linkedin' : 'Linkedin',
    'facebook' : 'Facebook',
    'web' : 'Web',
    'video' : 'Vídeo',
    'fax' : 'Fax',
    'idPropio' : 'Id. Propio',
    'añadirFechaIdPropio' : 'Añadir fecha al Id. Propio',
    'eligeFotoPerfil' : 'Elige una foto de perfil',
    'seleccionarFoto' : 'Seleccionar Foto',
    'SiRegistradoInicieSesion' : 'Si ya esta registrado, inicie sesión',
    'iniciarSesionMensaje' : 'Inicia Sesión en gocono para publicar y contratar servicios',
    'mensajeRegistrarse' : 'Si aun no esta registrado, hágalo',
    'cambiarContraseña' : 'Cambiar contraseña',
    'guardarCambios' : 'Guardar cambios',
    'mensajeVerPerfilIniciarSesion' : 'Para ver el perfil, debe iniciar sesión',
    'introduzcaServicioBuscar' : 'Error Introduzca el servicio que desea buscar.',
    'filtrosAdicionales' : 'Filtros adicionales',
    'precioMaximo' : 'Precio máximo',
    'distanciaMaximaLocalización' : 'Distancia máxima de localización en Km',
    'tengaOfertas' : 'Tenga ofertas el profesional',
    'valoracionMinima' : 'Valoración mínima',
    'buscarContactos' : 'Buscar solo entre mis contactos',
    'descuentoMinimo' : 'Descuento mínimo',
    'accesoProfesional/Empresa' : 'Acceso profesional/Empresa',
    'verServicio' : 'Ver servicio',
    'verConversaciones' : 'Ver conversaciones',
    'verContactos' : 'Ver contactos',
    'servicios' : 'Servicios',
    'motor' : 'Motor',
    'bicicletas' : 'Bicicletas',
    'mascotas' : 'Mascotas',
    'cursos' : 'Cursos',
    'hogar' : 'Hogar',
    'masajes' : 'Masajes',
    'reparacion' : 'Reparación',
    'especiales' : 'Especiales',
    'otros' : 'Otros',
    'eligeFotosServicio' : 'Elige las fotos del servicio',
    'fotosServicio' : 'Fotos del servicio',
    'verConversaciones': 'Conversaciones',
    'enviar' : 'Enviar'


};


/*check if the struct of each translation have all the objects*/
var a, keytrans_en, fails, fail, correct, keytrans_es;
fails = 0;
fail = [];
correct = 0;
a = 0;
for (keytrans_en in trans_en) {
    correct = 0;
    for (keytrans_es in trans_es) {
        if (keytrans_es === keytrans_en) {
            correct = 1;
        }
    }
    if (correct === 0) {
        fails = fails + 1;
        fail[a] = keytrans_en;
        a = a + 1;
    } else {
        correct = 0;
    }
}
if (fails > 0) {
    console.log("Translate errors");
    console.log(fails);
    console.log(fail);
}
},{}]},{},["./src/js/themes/angular/main.js"]);
