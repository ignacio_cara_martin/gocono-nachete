/*!
 * Author: mosaicpro.biz
 * Licence: Commercial (http://themeforest.net/licenses)
 * Copyright 2014
 */

var requireDir = require('require-dir');
requireDir('./lib/gulp/tasks', {recurse: true});