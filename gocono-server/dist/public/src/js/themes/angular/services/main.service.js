angular.module('app')    
    .factory('conversationsService', function ($http, usersService) {
        var conversationCreated = null,
            mensajesTodos = [],
            conversationUpdated = null;
        var myService = {
            /**
             * @method getProducts : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            createConversations: function (conversation) {
                if (usersService.getToken()) {
                    return $http({
                        method: "POST",
                        url: '/api/conversations',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: conversation
                    })
                        .then(function (data) {
                                console.log(data)
                                conversationCreated = true;
                            },
                            function (error) {
                                alert(error.data)
                            });
                }
            },
            updateConversations: function (conversationToSend) {
                if (usersService.getToken()) {
                    return $http({
                        method: "PUT",
                        url: '/api/conversations',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: conversationToSend
                    })
                        .then(function (data) {
                                console.log(data)
                                conversationUpdated = true;
                            },
                            function (error) {
                                alert(error.data)
                            });
                }
            },
            getConversationCreated: function () {
                return conversationCreated;
            },
            getConversationUpdated: function () {
                return conversationUpdated;
            },
            getConversations: function () {
                return $http({
                    method: "GET",
                    url: '/api/conversations',
                    headers: {
                            'x-access-token': usersService.getToken()
                        },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json"
                })
                .then(function (mensajes) {
                    for (var i = 0; i < mensajes.data.length; i ++) {
                        mensajesTodos[i] = {};
                        console.log(mensajes.data[i]._id)
                        mensajesTodos[i]._id = mensajes.data[i]._id;
                        mensajesTodos[i].mensaje = [];
                        mensajesTodos[i].mensaejeOriginal = mensajes.data[i].mensaje;
                        var a = mensajes.data[i].mensaje.split('2-2,IDCAMBIO,2-2');
                        if (a[1]) {
                            for (var j = 0; j < a.length; j++) {
                                var b = a[j].split('-,IDCAMBIO,-');
                                mensajesTodos[i].mensaje[j] = {text: b[0], owner: b[1]};
                            }
                        } else {
                            var b = a[0].split('-,IDCAMBIO,-');
                            console.log(b)
                            mensajesTodos[i].mensaje[0] = {text: b[0], owner: b[1]};
                        }
                        if (usersService.getUser()._id == mensajes.data[i].user1) {
                            mensajesTodos[i].user = usersService.getOwner(mensajes.data[i].user2);
                        } else {
                            mensajesTodos[i].user = usersService.getOwner(mensajes.data[i].user1);
                        }
                    }
                    usersService.getOwner(usersService.getUser()._id).then ( function () {
                        return {err: null, data: mensajesTodos};
                    })
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;
                        return {err: my_err, data: mensajesTodos};
                    }
                })
            },
            getMensajes: function () {
                return mensajesTodos;
            }
        };
        return myService;
    })
    .factory('productsService', function ($http, usersService) {
        var product_list = [],
            best_product_list = [],
            anuncio = null,
            search_options;
        var myService = {
            /**
             * @method getProducts : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            getProducts: function (options) {

                search_options = options;

                if (options.endDate) {
                    options.endDate = moment(options.endDate).format();
                }
                if (options.startDate) {
                    options.startDate = moment(options.startDate).format();
                }

                var parameters = {
                    lat: options.coordinates.latitude,
                    long: options.coordinates.longitude,
                    distance: options.distance,
                    product: options.product,
                    price: options.price,
                    offers: options.offers,
                    startsNumber: options.startsNumber,
                    contactsOnly: options.contactsOnly,
                    dayHourFrom: options.startDate,
                    dayHourTo: options.endDate,
                    discount: options.discount
                };
                if (options.contactsOnly) {
                    parameters.email = usersService.getUser().email;
                }
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: parameters
                    })
                        .then(function (productsMarkers) {
                                product_list = prettyMarkers(productsMarkers.data);
                                return ({
                                    searchOptions: options,
                                    searchResults: product_list
                                });
                            },
                            function (error) {
                                alert(error.data)
                            });
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: parameters
                    })
                        .then(function (productsMarkers) {
                                product_list = prettyMarkers(productsMarkers.data);
                                return ({
                                    searchOptions: options,
                                    searchResults: product_list
                                });
                            },
                            function (error) {
                                alert(error.data)
                            });
                }

                //return promise;
            },

            /**
             * @method getProductInfo : service to obtain the data for a product in $scope
             * @params : id (Object) product id
             * @return : Object
             **/
            getProductInfo: function (product_id) {
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            id: product_id
                        }
                    })
                        .then(function (product) {
                            return product.data;
                        });
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            id: product_id
                        }
                    })
                        .then(function (product) {
                            return product.data;
                        });
                }

            },
            setProductList: function (products) {
                product_list = products;
            },
            getProductList: function () {
                return product_list;
            },
            setBestProductList: function (products) {
                best_product_list = products;
            },
            getBestProductList: function () {
                return best_product_list;
            },
            getSearchOptions: function () {
                return search_options;
            },
            setAnuncio: function (value) {
                anuncio = value;
            },
            getAnuncio: function () {
                return anuncio;
            },
            createProduct: function (data) {
                if (usersService.getToken()) {
                    return $http({
                        method: "POST",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: data
                    })
                        .then(function (product) {
                            return product.data;
                        });
                } else {
                    return $http({
                        method: "POST",
                        url: '/api/products',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: data
                    })
                        .then(function (product) {
                            return product.data;
                        });
                }
            },
            // updateProduct: function (data) {
            //     console.log("updated")
            // },
            getProductByOwner: function (owner) {
                if (usersService.getToken()) {
                    return $http({
                        method: "GET",
                        url: '/api/products',
                        headers: {
                            'x-access-token': usersService.getToken()
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {owner: owner}
                    })
                    .then(function (products) {
                            return products.data;
                        },
                        function (error) {
                            alert(error.data)
                        });
                }
            }
        };
        return myService;
    })

    .factory('usersService', function ($http) {
        var my_err = null,
            user = null,
            ownerProduct = null,
            my_data;
        var token = "";
        if (window.localStorage.getItem('token') != undefined && window.localStorage.getItem('token') != null ) {
            token = window.localStorage.getItem('token');
            user = JSON.parse(window.localStorage.getItem('user'));
        }

        var myUserService = {
            /**
             * @method getMarkers : service to obtain the data of the markers from an coordenate
             * @params : coordenates (Object) used to create the url for the service
             * @return : array
             **/
            getToken: function () {
                return token;
            },
            setToken: function (value) {
                token = value;
            },
            loginUser: function (email, pass) {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            email: email,
                            pass: pass
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                } else {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            email: email,
                            pass: pass
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                }
            },
            /**
             * @method registerUser : register an user
             * @params : user (Object) user data for register
             * @return : Object
             **/
            registerUser: function (user) {
                delete user.password2;
                if (token) {
                    return $http({
                        method: "POST",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            user: user
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            } else {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                } else {
                    return $http({
                        method: "POST",
                        url: '/api/users',
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            user: user
                        }
                    })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            token = userInfo.data.token;
                            localStorage.setItem('token', token);
                            localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                            return {err: null, data: my_data};
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            } else {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                }
            },
            updateUser: function (userdata) {
                if(userdata) {
                    if (userdata.password2) {
                        delete userdata.password2;
                    }
                }
                return $http({
                    method: "PUT",
                    url: '/api/users',
                    headers: {
                        'x-access-token': token
                    },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    params: {
                        user: userdata
                    }
                })
                .then(function (userInfo) {
                    user = {};
                    console.log(user)
                    if (token) {
                        return $http({
                            method: "GET",
                            url: '/api/users',
                            headers: {
                                'x-access-token': token
                            },
                            withCredentials: true,
                            contentType: "application/x-www-form-urlencoded",
                            dataType: "json"
                        })
                        .then(function (userInfo) {
                            my_data = userInfo.data;
                            user = userInfo.data
                            localStorage.setItem('user', JSON.stringify(userInfo.data));
                            console.log(user)
                            return {err: null, data: my_data};
                            //return productsMarkers.data;
                        }).catch(function (err) {
                            // recover here if err is 404
                            if (err.status === 404) {
                                my_err = err.data;

                                return {err: my_err, data: my_data};
                            }
                        })
                    }
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    } else {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    }
                })
            },
            getUserContacts: function (userdata) {
                return $http({
                    method: "GET",
                    url: '/api/users/contacts',
                    headers: {
                        'x-access-token': token
                    },
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json"
                })
                .then(function (userInfo) {
                    return userInfo.data
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    } else {
                        my_err = err.data;

                        return {err: my_err, data: my_data};
                    }
                })
            },
            /**
             * @method getUsertInfo : service to obtain the data for an user in $scope
             * @params : id (Object) user email
             * @return : Object
             **/
            getUsertInfo: function (user_email) {
                //TODO
            },
            setUser: function (new_user) {
                user = new_user;
            },
            getUser: function () {
                return user;
            },
            getOwner: function (ownerId) {
                return $http({
                    method: "GET",
                    url: '/api/users/photo',
                    withCredentials: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    params: {
                        id: ownerId
                    }
                })
                .then(function (userInfo) {
                    ownerProduct = userInfo.data
                    my_data = userInfo.data;
                    return {err: null, data: my_data};
                }).catch(function (err) {
                    // recover here if err is 404
                    if (err.status === 404) {
                        my_err = err.data;
                        return {err: my_err, data: my_data};
                    }
                })
            },
            
            takeOwner: function () {
                return ownerProduct;
            },
            getUserUpdated: function() {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json"
                    })
                    .then(function (userInfo) {
                        my_data = userInfo.data;
                        token = userInfo.data.token;
                        localStorage.setItem('token', token);
                        localStorage.setItem('user', JSON.stringify(userInfo.data.data));
                        return {err: null, data: my_data};
                        //return productsMarkers.data;
                    }).catch(function (err) {
                        // recover here if err is 404
                        if (err.status === 404) {
                            my_err = err.data;

                            return {err: my_err, data: my_data};
                        }
                    })
                }
            },
            addContact: function (ownerId) {
                if (token) {
                    return $http({
                        method: "GET",
                        url: '/api/users/addContact',
                        headers: {
                            'x-access-token': token
                        },
                        withCredentials: true,
                        contentType: "application/x-www-form-urlencoded",
                        dataType: "json",
                        params: {
                            ownerId: ownerId
                        }
                    })
                    .then(function (userInfo) {
                        user = userInfo.data;
                        localStorage.setItem('user', JSON.stringify(userInfo.data));
                        return {err: null, data: my_data};
                        //return productsMarkers.data;
                    }).catch(function (err) {
                        // recover here if err is 404
                        if (err.status === 404) {
                            my_err = err.data;

                            return {err: my_err, data: my_data};
                        }
                    })
                }
            }
        };
        return myUserService;
    })
;


/**
 * @method prettyMarkers : create a correct mark for the map with the services data
 * @params : marker (array) is an array of objects
 * @return : array
 **/
function prettyMarkers(markers) {

    var marker = {
        id: null,
        name: null,
        towards: null,
        icon: null,
        coords: {},
        options: {}
    };
    var return_markers = [],
        return_best = [];

    angular.forEach(markers, function (mark, key) {
        marker = {
            id: mark._id,
            name: mark.name,
            owner: mark.owner,
            price: mark.price,
            distance: mark.distance,
            images: mark.images,
            productType: mark.productType,
            icon: 'http://google-maps-icons.googlecode.com/files/bookstore.png',
            coords: {
                latitude: mark.location[0],
                longitude: mark.location[1]
            }
        };
        return_markers.push(marker);
    });
    return return_markers;
}


/*
 *@method arrayObjectIndexOf: Helper Function to serach indexOf
 * @params myArray (Array), the array to search
 * @params searchTerm (String), string to search into the array
 * @params property (String), the index value
 *
 */
function arrayObjectIndexOf(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) {
            return i;
        }
    }
    return -1;
}

/*
 *@method removeArrayObject: Helper Function to remove an object from the array
 * @params myArray (Array), the array to search
 * @params searchTerm (String), string with the id to remove
 * @params property (String), the index value
 *
 */
function removeArrayObject(myArray, searchTerm, property) {
    var array_modify = [];
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] !== searchTerm) {
            array_modify.push(myArray[i]);
        }
    }
    return array_modify;
}


