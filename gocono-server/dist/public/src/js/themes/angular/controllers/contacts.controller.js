angular.module('app')
	.controller('viewContactsCtrl', function ($scope, $http, usersService, productsService, $location) {
		$scope.user = usersService.getUser();
	    $scope.$watch(function(){return usersService.getUser()}, function() {
	        $scope.user = usersService.getUser();
	        if ($scope.user) {
		    	$scope.init();
	        }
	    });
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
	    $scope.init = function () {
		    usersService.getUserContacts().then( function(response){
		    	if (response.err){
	              	$scope.user_error = response.err;
	            } else {
	          		$scope.services = response;
	          		$scope.servicesContacts = [];
	          		var entered = false;
	          		var noEnteredService = true;
			        for (var i = 0; i < $scope.services.length; i ++) {
			        	for (var item in $scope.services[i].contacts) {
			        		if ($scope.servicesContacts[0]) {
			        			noEnteredService = true;
					        	for (var j = 0; j < $scope.servicesContacts.length; j ++) {
					        		if ($scope.servicesContacts[j].name == $scope.services[i].contacts[item]) {
					        			noEnteredService = false;
					        			entered = false;
					        			for (var t = 0; t < $scope.servicesContacts[j].contacts.length; t++) {
					        				if ($scope.servicesContacts[j].contacts[t] == $scope.services[i].name) {
					        					entered = true;
					        				}
					        			}
					        			if (!entered) {
					        				$scope.servicesContacts[j].contacts.push($scope.services[i].name);
					        			}					        			
					        		}
					        	}
					        	if (noEnteredService) {
					        			$scope.servicesContacts.push({"name" :$scope.services[i].contacts[item], "contacts": [$scope.services[i].name]});
					        		}
					        } else {
					        	$scope.servicesContacts[0] = {"name" :$scope.services[i].contacts[item], "contacts": [$scope.services[i].name]};
					        }
				    	}
			        }
	          	}
	        });
	    }
	    $scope.init();
	})
;