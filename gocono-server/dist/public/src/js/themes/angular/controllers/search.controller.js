
angular.module('app')
    .controller('SearchCtrl', function ($scope, $http, productsService, usersService, uiGmapGoogleMapApi, $location, $timeout) {
        $scope.products = null;
        $scope.distance = null;
        $scope.product_filter = false;
        $scope.busqueda = '';
        $scope.notProduct = false;
        if (usersService.getUser()) {
            $scope.userNoLoged = false;
        } else {
            $scope.userNoLoged = true;
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.userNoLoged = false;
            } else {
                $scope.userNoLoged = true;
            }
        });
        $scope.filterProduct = function () {
            $scope.product_filter = true;
        };
        $scope.hideFilterProduct = function () {
            $scope.product_filter = false;
        };

        $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.userLocation = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.userLocation = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.range = function(min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };
        $scope.filterOptions = {};
        $scope.filterOptions.startsNumber = 0;
        $scope.startsSelected = function (value) {
            $scope.filterOptions.startsNumber = value;
        };
        var getProductsSearch = function () {
            var coordinates = {latitude: $scope.position.latitude, longitude: $scope.position.longitude};
            $scope.filterOptions.coordinates = coordinates;
            if (!$scope.filterOptions.distance) {
                $scope.filterOptions.distance = 90;
            }
            productsService.getProducts($scope.filterOptions).then(function(data) {
                $location.path('/discover/map-listing-list');
            });

        };
        $scope.searchProduct = function () {
            if($scope.filterOptions.product) {
               if (!$scope.position) {
                    $scope.location();
                    $timeout(function () {
                        getProductsSearch();
                    }, 2000);
                } else {
                    getProductsSearch();
                } 
            } else {
                $scope.notProduct = true;
            }
            
        };
        $scope.loginUser = function () {
            $location.path('/login');
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('searchTextField');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
            });
        };

        $scope.beforeRenderStartDate = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.filterOptions.endDate) {
                var activeDate = moment($scope.filterOptions.endDate);
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() >= activeDate.valueOf()) $dates[i].selectable = false;
                }
            }
        };

        $scope.beforeRenderEndDate = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.filterOptions.startDate) {
                var activeDate = moment($scope.filterOptions.startDate).subtract(1, $view).add(1, 'minute');
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() <= activeDate.valueOf()) {
                        $dates[i].selectable = false;
                    }
                }
            }
        };

    })
;
