angular.module('app')
	.controller('viewAllOffersCtrl', function ($scope, $http, usersService, productsService, $location) {
        $scope.firstTimeEntered = true;
	    $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.locationUser = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.locationUser = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };

        $scope.acept = function() {
    		if ($scope.locationUser) {
                var dataSent = {
                    offers: true, 
                    product: '', 
                    coordinates: {latitude: $scope.locationUser[0],longitude: $scope.locationUser[1]},
                    distance: 90
                };
                productsService.getProducts(dataSent).then(function(data) {
                    $scope.firstTimeEntered = false;
                    $scope.servicesFound = productsService.getProductList();
                });
			    $scope.serviceRegisterError = "";
        	} else {
        		$scope.serviceRegisterError = "Introduzca la ubicación en la que desea encontrarlos";
        	}
        }
	})
;