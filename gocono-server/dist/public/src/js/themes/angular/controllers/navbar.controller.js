angular.module('app')
    .controller('navbarCtrl', function ($scope, $http, usersService, productsService,$location) {  
	    $scope.user = usersService.getUser();
	    $scope.$watch(function(){return usersService.getUser()}, function() {
	        $scope.user = usersService.getUser();
	    });
		$scope.loginUser = function(email, pass){
	      	usersService.loginUser(email,pass).then( function(response){
	          	if (response.err){
	            	$scope.login_error = response.err;
	          	}else{
		            $scope.login_error = null;
		            usersService.setUser(response.data.data);
	          	}
	        },
	        function(status){
	          	console.log(status);
	        });
	    };
	    $scope.logoutUser = function(){
			usersService.setUser(null);
			usersService.setToken("");
			localStorage.setItem('token',null);
            localStorage.setItem('user',null);
	    };
	    $scope.registerUser = function(user_data, pass2){
	      	if (user_data.password == pass2) {
				usersService.registerUser(user_data).then( function(response){
		          	if (response.err){
		          		console.log("error")
		          		console.log(response)
		            	$scope.register_error = response.err;
		          	}else{
			            usersService.setUser(response.data.data);
			            $scope.register_error = null;
			            $scope.user_data = {};
		          	}
		        },
		        function(status){
		          console.log(status);
		        });
	      	} else {
	      		$scope.register_error = "Las contraseñas no coinciden";
	      	}
		};
	})
;