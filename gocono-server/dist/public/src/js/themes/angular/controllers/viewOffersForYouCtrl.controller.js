angular.module('app')
	.controller('viewOffersForYouCtrl', function ($scope, $http, usersService, productsService, $location) {
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
        $scope.getServices = function () {
        	var dataSent = {
        		offers: true, 
        		product: $scope.user.firstTime, 
        		coordinates: {latitude: $scope.user.location[0],longitude: $scope.user.location[1]},
        		distance: 90
        	};
	        productsService.getProducts(dataSent).then(function(data) {
	            $scope.servicesFound = productsService.getProductList();
	        });
	    };
	    $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.locationUser = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.locationUser = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };
	    if (usersService.getUser()) {
            if (usersService.getUser()) {
            	$scope.user = usersService.getUser();
                $scope.owner = $scope.user._id;
                if ($scope.user.firstTime){
                	$scope.firstTimeEntered = false;
                	$scope.getServices();
				} else {
					$scope.firstTimeEntered = true;
				}
            } else {
                $scope.owner = undefined;
                $scope.firstTimeEntered = true;
            }
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.user = usersService.getUser();
                $scope.owner = $scope.user._id;
				if ($scope.user.firstTime){
                	$scope.firstTimeEntered = false;
                	$scope.getServices();
				} else {
					$scope.firstTimeEntered = true;
				}
            } else {
                $scope.owner = undefined;
        		$scope.firstTimeEntered = true;
            }
        });

        $scope.acept = function(firstTime) {
        	if (firstTime) {
        		if ($scope.locationUser) {
					var data = {
						firstTime : firstTime,
						location: $scope.locationUser
				    };
				    usersService.updateUser(data).then(
				        function(response){
				            if (response.err){
				            }else{
				              $scope.user = response.data;
				              $scope.firstTimeEntered = false;
				              $scope.getServices();
				            }
				        },
				        function(status){
				            console.log(status);
				        }
				    );
				    $scope.serviceRegisterError = "";
	        	} else {
	        		$scope.serviceRegisterError = "Introduzca la ubicación en la que desea encontrarlos";
	        	}
        	} else {
        		$scope.serviceRegisterError = "Introduzca las categorias o productos que desea encontrar";
        	}
        }
	})
;