angular.module('app')
    .controller('listingPanelCtrl', function ($scope, $http, usersService, productsService, $rootScope) {
        $scope.servicesFound = productsService.getProductList();
        $scope.range = function (min, max, step) {
            step = step || 1;
            var input = [];
            for (var i = min; i <= max; i += step) {
                input.push(i);
            }
            return input;
        };
        $scope.selectService = function (service) {
            usersService.getOwner(service.owner).then( function () {
                $rootScope.$broadcast('serviceSelected', service);
            });
        }
        $scope.locateService = function(service) {
            $rootScope.$emit('locatedService', {
                location: service.coords,
                idService: service.id
            });
        }
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });
        
    })
    .controller('textMultipleCtrl', function ($scope, $http, usersService, productsService, $rootScope, conversationsService) {
        $scope.servicesFound = productsService.getProductList();
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });

        $scope.sendMessages = function(textMessageMultiple) {
            for (var i = 0; i < $scope.servicesFound.length; i++) {
                if ($('#' + $scope.servicesFound[i].id).is(':checked')) {
                    $scope.sendMessageUnic($scope.servicesFound[i].owner, textMessageMultiple);
                }
            }
        }

        $scope.sendMessageUnic = function(ownerId, textMessage) {
            var conversation = {
                user1: $scope.user._id,
                user2: ownerId,
                mensaje: '' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + ''
            };
            conversationsService.createConversations(conversation).then ( function() {
                if (conversationsService.getConversationUpdated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
    })
    .controller('serviceDetailsListCtrl', function ($scope, $http, usersService, productsService, $rootScope, conversationsService) {
        $scope.selectedService = $rootScope.selectedService;
        $scope.mensajeEnviado = false;
        $scope.owner = usersService.takeOwner();
        $scope.$watch(function(){return usersService.takeOwner()}, function() {
            $scope.owner = usersService.takeOwner();
            if ($scope.owner) {
                $scope.noEsContacto = true;
                if ($scope.user && $scope.user.contactos) {
                    var contactos= $scope.user.contactos.split(",");
                    for (var i = 0; i < contactos.length; i++) {
                        if (contactos[i] == $scope.owner._id) {
                            $scope.noEsContacto = false;
                        }
                    }
                }
            }
        });
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
        });
        if ($scope.owner) {
            $scope.noEsContacto = true;
            if ($scope.user && $scope.user.contactos) {
                var contactos= $scope.user.contactos.split(",");
                for (var i = 0; i < contactos.length; i++) {
                    if (contactos[i] == $scope.owner._id) {
                        $scope.noEsContacto = false;
                    }
                }
            }
        };
        $scope.addContacts = function () {
            var ownerId = $scope.owner._id;
            usersService.addContact(ownerId);

        }
        $scope.dismissServiceDetails = function () {
            $scope.selectedService = undefined;
            $rootScope.selectedService = undefined;
        }

        $scope.sendMessage = function(textMessage) {
            var conversation = {
                user1: $scope.user._id,
                user2: $scope.owner._id,
                mensaje: '' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + ''
            };
            conversationsService.createConversations(conversation).then ( function() {
                if (conversationsService.getConversationUpdated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
    })
;