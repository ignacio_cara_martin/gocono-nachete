angular.module('app')
	.controller('viewConversationsCtrl', function ($scope, $http, usersService, productsService, $location, conversationsService) {
		$scope.sendEntrar = function(){
	      	$location.path('/login');
	    };
        $scope.user = usersService.getUser();
        $scope.$watch(function(){return usersService.getUser()}, function() {
            $scope.user = usersService.getUser();
            $scope.showConversacion = false;
	        conversationsService.getConversations().then( function () {
	        	$scope.conversaciones = conversationsService.getMensajes();
	        });
        });
        $scope.showConversacion = false;
        conversationsService.getConversations().then( function () {
        	$scope.conversaciones = conversationsService.getMensajes();
        });

        $scope.showConversationSelected = function (id) {
        	for (var i = 0; i < $scope.conversaciones.length; i++) {
        		if (id == $scope.conversaciones[i]._id) {
        			$scope.mensaejeOriginal = $scope.conversaciones[i].mensaejeOriginal;
        			$scope.mensaejeId = $scope.conversaciones[i]._id;
        			$scope.user2 = $scope.conversaciones[i].user.$$state.value.data;
        			$scope.showConversacion = true;
        			$scope.mensajes = $scope.conversaciones[i].mensaje;
        		}
        	}
        };

        $scope.sendMessage = function(textMessage) {
        	$scope.mensajes.push({text: textMessage, owner: $scope.user._id});
        	$scope.mensaejeOriginal = $scope.mensaejeOriginal + '2-2,IDCAMBIO,2-2' + textMessage + '-,IDCAMBIO,-' + $scope.user._id + '';
        	$scope.conversationToSend = {
                id: $scope.mensaejeId,
                mensaje: $scope.mensaejeOriginal
            };
            console.log($scope.conversationToSend)
            conversationsService.updateConversations($scope.conversationToSend).then ( function() {
                if (conversationsService.getConversationCreated()) {
                    $scope.mensajeEnviado = true;
                }
            })
        }
	})
;