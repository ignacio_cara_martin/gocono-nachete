'use strict';


/* global angular, document, window */

angular.module('app')
  .controller('LoginCtrl', function($scope, $timeout, $stateParams, usersService,$location) {
    $scope.user_data = {};
    $scope.user_data.email = null;
    $scope.user_data.pass = null;
    $scope.user = usersService.getUser();
    $scope.$watch(function(){return usersService.getUser()}, function() {
        $scope.user = usersService.getUser();
    });
    $scope.user_error = null;
    $scope.loginUser = function(user_data){

      usersService.loginUser(user_data.email,user_data.pass).then(
        function(response){
          if (response.err){
            $scope.user_error = response.err;
          }else{
            $scope.user = response.data;
            usersService.setUser(response.data.data);
            $location.path('/');
          }
        },
        function(status){
          console.log(status);
        }
      );

    };
    $scope.sendRegister = function(){
      $location.path('/register');
    };
  })

  .controller('RegisterUserCtrl', function($scope, $timeout, Upload,$stateParams, usersService,$location) {

    $scope.email = null;
    $scope.pass = null;
    $scope.user = null;
    $scope.user_error = null;
    $scope.user_data = {};
    $scope.registerUser = function(user_data) {
      if(user_data.password2 != user_data.password) {
        $scope.user_error = "Las contraseñas no coinciden";
      } else {
        var data = {
          name : user_data.name,
          surname : user_data.surname,
          password : user_data.password,
          email : user_data.email,
          nick : user_data.nick,
          telefono : user_data.telefono,
          web : user_data.web,
          fax : user_data.fax,
          facebook : user_data.facebook,
          linkedin : user_data.linkedin,
          video : user_data.video,
          avatar : $scope.user_data.avatar,
          idpropio : user_data.idpropio,
          dateIdPropio: user_data.dateIdPropio
        };
        usersService.registerUser(data).then(
          function(response){
            if (response.err){
              $scope.user_error = response.err;
            }else{
              $scope.user = response.data;
              $scope.user_error = null;
              $location.path('/login');
            }
          },
          function(status){
            console.log(status);
          }
        );
      }
    };

    $scope.uploadFiles = function (files) {
      $scope.user_data.avatar = "upload/tmp/" + files[0].name;
      $scope.files = files;
      if (files && files.length) {
        Upload.upload({
          url: '/upload',
          data: {
            files: files
          }
        }).then(function (response) {
          $timeout(function () {
            $scope.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    };

    $scope.sendEntrar = function(){
      $location.path('/login');
    };
  })


.controller('UpdateUserCtrl', function($scope, $timeout, Upload,$stateParams, usersService,$location) {

    $scope.user = usersService.getUser();
    $scope.$watch(function(){return usersService.getUser()}, function() {
      $scope.user = usersService.getUser();
      for (var key in $scope.user) {
        $scope.user_data[key] = $scope.user[key];
      }
      $scope.user_data.password = undefined;
    });
    $scope.user_error = null;
    $scope.user_data = {};
    for (var key in $scope.user) {
      $scope.user_data[key] = $scope.user[key];
    }
    $scope.user_data.password = undefined;
    $scope.updateUser = function(user_data) {
      if(user_data.password2 != user_data.password) {
        $scope.user_error = "Las contraseñas no coinciden";
      } else {
        var data = {
          name : user_data.name,
          surname : user_data.surname,
          password : user_data.password,
          email : user_data.email,
          nick : user_data.nick,
          telefono : user_data.telefono,
          web : user_data.web,
          fax : user_data.fax,
          facebook : user_data.facebook,
          linkedin : user_data.linkedin,
          video : user_data.video,
          avatar : $scope.user_data.avatar,
          idpropio : user_data.idpropio,
          dateIdPropio: user_data.dateIdPropio,
          _id: user_data._id
        };
        usersService.updateUser(data).then(
          function(response){
            if (response.err){
              $scope.user_error = response.err;
            }else{
              $scope.user = response.data;
              $scope.user_error = null;
              $location.path('/search');
            }
          },
          function(status){
            console.log(status);
          }
        );
      }
    };

    $scope.uploadFiles = function (files) {
      $scope.user_data.avatar = "upload/tmp/" + files[0].name;
      $scope.files = files;
      if (files && files.length) {
        Upload.upload({
          url: '/upload',
          data: {
            files: files
          }
        }).then(function (response) {
          $timeout(function () {
            $scope.result = response.data;
          });
        }, function (response) {
          if (response.status > 0) {
            $scope.errorMsg = response.status + ': ' + response.data;
          }
        }, function (evt) {
          $scope.progress =
            Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
      }
    };

    $scope.sendEntrar = function(){
      $location.path('/login');
    };
  });