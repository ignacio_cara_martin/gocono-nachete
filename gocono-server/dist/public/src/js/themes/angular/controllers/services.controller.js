angular.module('app')
	.controller('registerService', function ($scope, $location, Upload, $timeout, usersService, productsService) {
        $scope.service_data = {};
        if (usersService.getUser()) {
            $scope.service_data.owner = usersService.getUser()._id;
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.service_data.owner = usersService.getUser()._id;
            }
        });
        $scope.service_data.images = [];
        $scope.namesFiles = [];
        $scope.uploadFiles = function (files) {
            if (files[0]){
                var nameFile = "upload/tmp/" + files[0].name;
                $scope.namesFiles.push(files[0].name);
                $scope.service_data.images.push(nameFile);
                $scope.files = files;
                if (files && files.length) {
                    Upload.upload({
                        url: '/upload',
                        data: {
                            files: files
                        }
                    }).then(function (response) {
                        $timeout(function () {
                            $scope.result = response.data;
                        });
                    }, function (response) {
                        if (response.status > 0) {
                            $scope.errorMsg = response.status + ': ' + response.data;
                        }
                    }, function (evt) {
                        $scope.progress =
                        Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
            }
        };
        $scope.location = function () {
            window.navigator.geolocation.getCurrentPosition(function (position) {
                $scope.$apply(function () {
                    $scope.position = position.coords;
                    $scope.service_data.location = [position.coords.latitude, position.coords.longitude];
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'latLng': {
                            lat: $scope.position.latitude,
                            lng: $scope.position.longitude
                        }
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $scope.$apply(function () {
                                    $scope.locationPosition = results[0].formatted_address;
                                });
                            } else {
                                $scope.$apply(function () {
                                    $scope.locationPosition = "No se ha podido conseguir la dirección";
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert(error);
            });
        };
        $scope.initialize = function(event) {
            var input = document.getElementById('localizacion');
            var searchBox = new google.maps.places.SearchBox(input);
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                $scope.position = {};
                $scope.position.latitude = places[0].geometry.location.lat();
                $scope.position.longitude = places[0].geometry.location.lng();
                $scope.service_data.location = [places[0].geometry.location.lat(), places[0].geometry.location.lng()];
            });
        };
        $scope.beforeRenderdayHourFrom = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.service_data.dayHourTo) {
                var activeDate = moment($scope.service_data.dayHourTo);
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() >= activeDate.valueOf()) $dates[i].selectable = false;
                }
            }
        };

        $scope.beforeRenderdayHourTo = function($view, $dates, $leftDate, $upDate, $rightDate) {
            if ($scope.service_data.dayHourFrom) {
                var activeDate = moment($scope.service_data.dayHourFrom).subtract(1, $view).add(1, 'minute');
                for (var i = 0; i < $dates.length; i++) {
                    if ($dates[i].localDateValue() <= activeDate.valueOf()) {
                        $dates[i].selectable = false;
                    }
                }
            }
        };
        $scope.sendEntrar = function(){
          $location.path('/login');
        };  
        $scope.registerService = function(service_data) {
            if (service_data.name) {
                if (service_data.location) {
                    if (service_data.category) {
                        if (service_data.description) {
                            productsService.createProduct(service_data).then(
                                function(response){
                                  if (response.err){
                                    $scope.serviceRegisterError = response.err;
                                  }else{
                                    $scope.correctRegister = "Servicio registrado correctamente";
                                  }
                                },
                                function(status){
                                  console.log(status);
                                }
                            )
                        } else {
                            $scope.serviceRegisterError = "Introduzcala descripción del servicio";
                        }
                    } else {
                        $scope.serviceRegisterError = "Seleccione la categoría del servicio";
                    }
                } else {
                    $scope.serviceRegisterError = "Introduzca la ubicación del servicio";
                }
            } else {
                $scope.serviceRegisterError = "Introduzca el nombre del servicio";
            }
        }
        
    })
    .controller('viewServiceCtrl', function ($scope, $location, usersService, productsService, $http) {
        $scope.service_data = {};
        $scope.beforeService = false;
        $scope.nextService = false;
        $scope.dataNumber = 0;

        if (usersService.getUser()) {
            if (usersService.getUser()) {
                $scope.owner = usersService.getUser()._id;
            } else {
                $scope.owner = undefined;
            }
        }
        $scope.$watch(function(){return usersService.getUser()}, function() {
            if (usersService.getUser()) {
                $scope.owner = usersService.getUser()._id;
            } else {
                $scope.owner = undefined;
            }
        });
        $scope.init = function () {
            productsService.getProductByOwner($scope.owner).then(function(data) {
                $scope.data = data;
                $scope.service_data = data[0];
                $scope.getImages();
                if ($scope.service_data) {
                    $scope.getPlace($scope.service_data.location[0],$scope.service_data.location[1]);
                    if ($scope.service_data.active) {
                        $scope.service_data.active = "Si";
                    } else {
                        $scope.service_data.active = "No";
                    }
                    if (data[1]) {
                        $scope.nextService = true;
                    }
                }
            });
        }

        $scope.getImages = function () {
            if ($scope.service_data.images) {
                $scope.namesFiles = $scope.service_data.images.split(',');
                for (var i = 0; i < $scope.namesFiles.length; i++) {
                    $scope.namesFiles[i] = $scope.namesFiles[i].substring(11,$scope.namesFiles[i].length);
                }
            }
        }
        $scope.getPlace = function (lat,long) {
            $http({
                method: "GET",
                url: 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + long + '&sensor=true',
                dataType: "json"
            })
            .then(function (place) {
                $scope.locationPosition = place.data.results[0].formatted_address;
            });
            
        }
        $scope.changeService = function (action) {
            if (action == "next") {
                $scope.dataNumber = $scope.dataNumber + 1;
            } else {
                $scope.dataNumber = $scope.dataNumber - 1;
            }
            $scope.service_data = $scope.data[$scope.dataNumber];
            $scope.getImages();
            $scope.getPlace($scope.service_data.location[0],$scope.service_data.location[1]);
            if ($scope.service_data.active) {
                $scope.service_data.active = "Si";
            } else {
                $scope.service_data.active = "No";
            }
            $scope.beforeService = false;
            $scope.nextService = false;
            if ($scope.data[$scope.dataNumber + 1]) {
                $scope.nextService = true;
            }
            if ($scope.data[$scope.dataNumber - 1]) {
                $scope.beforeService = true;
            }
        }
        $scope.init();
        $scope.sendEntrar = function(){
          $location.path('/login');
        };
        
    })
;