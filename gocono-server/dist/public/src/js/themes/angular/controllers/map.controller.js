angular.module('app')
    .controller('MapCtrl', function($scope, productsService, usersService, $rootScope) {

        $scope.map = {center: {latitude: 51.219053, longitude: 4.404418 }, zoom: 15 };
        $scope.options = {scrollwheel: false};

        //$scope.searchResults = JSON.parse(localStorage.getItem('results'));

        $scope.searchResults = productsService.getProductList();
        $scope.markers = [];

        _.each($scope.searchResults, function (result, index) {
            $scope.marker = {
                id: index,
                coords: {
                    latitude: result.coords.latitude,
                    longitude: result.coords.longitude
                },
                owner: result.owner,
                images: result.images,
                idService: result.id,
                name: result.name,
                icon: {url: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'},
                events: {
                    click: function (marker, eventName, args) {
                        usersService.getOwner(marker.model.owner).then( function () {
                            $rootScope.selectedService = marker.model;
                        });
                    }
                }
            };
            $scope.markers.push($scope.marker);
        });

        $scope.searchOptions = productsService.getSearchOptions();

        if($scope.searchOptions && $scope.searchOptions.coordinates){
            $scope.map.center = $scope.searchOptions.coordinates;
        }

        $scope.dismissServiceDetails = function () {
            $scope.selectedService = null;
        };

        $rootScope.$on('serviceSelected', function (event, service) {
            usersService.getOwner(service.owner);
            $rootScope.selectedService = service;
        });

        $rootScope.$on('locatedService', function(event, props){
            for (var i = 0; i < $scope.markers.length; i++) {
                if (props.idService == $scope.markers[i].idService && props.location.latitude == $scope.markers[i].coords.latitude && props.location.longitude == $scope.markers[i].coords.longitude) {
                    $scope.markers[i].icon = {url: 'http://labs.google.com/ridefinder/images/mm_20_yellow.png'};
                    $scope.map.center = props.location;
                } else {
                    $scope.markers[i].icon = {url: 'http://labs.google.com/ridefinder/images/mm_20_blue.png'};
                }
            }
        });

    });