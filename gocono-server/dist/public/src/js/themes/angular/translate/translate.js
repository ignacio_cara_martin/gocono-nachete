'use strict';
var trans_en = {};
var trans_es = {};
angular.module('app').config(function ($translateProvider) {
    $translateProvider.translations('en', trans_en)
        .translations('es', trans_es)
        .registerAvailableLanguageKeys(['en', 'es'], {
            'en_US': 'en',
            'en_UK': 'en'
        })
        .determinePreferredLanguage()
        .fallbackLanguage('en', 'es');
    $translateProvider.useSanitizeValueStrategy('escaped');
});


trans_en = {
    'searchLabel' : 'Search'
};
trans_es = {
    'searchLabel' : 'Buscar',
    'contactsLoged': 'PARA VER SUS CONTACTOS DEBE ESTAR LOGEADO',
    'entrar' : 'Entrar',
    'userNoContacts' : "No tienes ningun contacto añadido",
    'Gocono' : "Gocono",
    'error!' : 'Error!',
    'registro': 'Registro',
    'nombre' : 'Nombre',
    'apellidos' : 'Apellidos',
    'nick' : 'Nick',
    'email' : 'Email',
    'contraseña' : 'Contraseña',
    'repetirContraseña' : 'Repetir contraseña',
    'aceptar' : 'Aceptar',
    'editarPerfil': 'Editar perfil',
    'salir' : 'Salir',
    'precio': 'Precio',
    'Día/Hora' : 'Día/Hora',
    'lanzarComunicacion' : 'Lanzar comunicacion',
    'descripcion' : 'Descripcion',
    'añadirContactos' : 'Añadir a contactos',
    'registrarNuevoServicio' : 'Registrar nuevo servicio',
    'paraRegistrarUnServicioDebeIniciarSesión' : 'Para registrar un servicio debe iniciar sesión',
    'localizacion' : 'Localización',
    'categoria' : 'Categoria',
    'activo' : 'Activo',
    'si' : 'Si',
    'no' : 'No',
    'tipoProducto' : 'Tipo de producto',
    'descuento' : 'Descuento',
    'duracionDescuento' : 'Duración descuento',
    'videoExplicativo' : 'Video explicativo',
    'elegirDía/horaServicioDesde:' : 'Elegir día/hora servicio Desde:',
    'elegirDía/horaServicioHasta:' : 'Elegir día/hora servicio Hasta:',
    'ofertas' : 'Ofertas',
    'registrarServicio' : 'Registrar servicio',
    'verTodasOfertas' : 'Ver todas las ofertas',
    'siguienteServicio' : 'Siguiente servicio',
    'anteriorServicio' : 'Anterior servicio',
    'paraSecciónLogeado' : 'Para ver esta sección debe estar logeado',
    'serviciosPreferidos' : 'Servicios preferidos',
    'verOfertasTi' : 'Ver ofertas para tí',
    'registrarse' : 'Registrarse',
    'telefono' : 'Teléfono',
    'linkedin' : 'Linkedin',
    'facebook' : 'Facebook',
    'web' : 'Web',
    'video' : 'Vídeo',
    'fax' : 'Fax',
    'idPropio' : 'Id. Propio',
    'añadirFechaIdPropio' : 'Añadir fecha al Id. Propio',
    'eligeFotoPerfil' : 'Elige una foto de perfil',
    'seleccionarFoto' : 'Seleccionar Foto',
    'SiRegistradoInicieSesion' : 'Si ya esta registrado, inicie sesión',
    'iniciarSesionMensaje' : 'Inicia Sesión en gocono para publicar y contratar servicios',
    'mensajeRegistrarse' : 'Si aun no esta registrado, hágalo',
    'cambiarContraseña' : 'Cambiar contraseña',
    'guardarCambios' : 'Guardar cambios',
    'mensajeVerPerfilIniciarSesion' : 'Para ver el perfil, debe iniciar sesión',
    'introduzcaServicioBuscar' : 'Error Introduzca el servicio que desea buscar.',
    'filtrosAdicionales' : 'Filtros adicionales',
    'precioMaximo' : 'Precio máximo',
    'distanciaMaximaLocalización' : 'Distancia máxima de localización en Km',
    'tengaOfertas' : 'Tenga ofertas el profesional',
    'valoracionMinima' : 'Valoración mínima',
    'buscarContactos' : 'Buscar solo entre mis contactos',
    'descuentoMinimo' : 'Descuento mínimo',
    'accesoProfesional/Empresa' : 'Acceso profesional/Empresa',
    'verServicio' : 'Ver servicio',
    'verConversaciones' : 'Ver conversaciones',
    'verContactos' : 'Ver contactos',
    'servicios' : 'Servicios',
    'motor' : 'Motor',
    'bicicletas' : 'Bicicletas',
    'mascotas' : 'Mascotas',
    'cursos' : 'Cursos',
    'hogar' : 'Hogar',
    'masajes' : 'Masajes',
    'reparacion' : 'Reparación',
    'especiales' : 'Especiales',
    'otros' : 'Otros',
    'eligeFotosServicio' : 'Elige las fotos del servicio',
    'fotosServicio' : 'Fotos del servicio',
    'verConversaciones': 'Conversaciones',
    'enviar' : 'Enviar'


};


/*check if the struct of each translation have all the objects*/
var a, keytrans_en, fails, fail, correct, keytrans_es;
fails = 0;
fail = [];
correct = 0;
a = 0;
for (keytrans_en in trans_en) {
    correct = 0;
    for (keytrans_es in trans_es) {
        if (keytrans_es === keytrans_en) {
            correct = 1;
        }
    }
    if (correct === 0) {
        fails = fails + 1;
        fail[a] = keytrans_en;
        a = a + 1;
    } else {
        correct = 0;
    }
}
if (fails > 0) {
    console.log("Translate errors");
    console.log(fails);
    console.log(fail);
}