// Angular App
require('./angular/app.js');
require('./angular/config.router.js');
require('./angular/main.js');

// Directives
require('essential/js/angular/main');
require('layout/js/angular/main');
require('sidebar/js/angular/main');

require('media/js/angular/main');

//Controllers
require("./controllers/search.controller");
require("./controllers/navbar.controller");

require("./controllers/user.controller");
require("./controllers/listing.controller");
require("./controllers/map.controller");
require("./controllers/services.controller");
require("./controllers/contacts.controller");
require("./controllers/viewOffersForYouCtrl.controller");
require("./controllers/viewAllOffersCtrl.controller");
require("./controllers/viewConversations.controller");

//Services
require("./services/main.service");
require("./services/geolocationSvc");
require("./translate/translate");
