// Essentials
require('essential/js/main');

// Layout
require('layout/js/main');

// Sidebar
require('sidebar/js/main');

// Owl Carousel
require('media/js/carousel/main');

// CORE
require('./main');